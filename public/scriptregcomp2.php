﻿<?php
// ******************************************************************************************
  if (!isset($_SESSION))
  session_start();
	// ini_set('display_errors', 1);
	// ini_set('display_startup_errors', 1);
	// error_reporting(E_ALL);
  include("includes/config.php");
  // echo '<pre>';
  // var_dump($_SESSION);
  // echo '</pre>';die;

  date_default_timezone_set('Asia/Kolkata');
  $date = date("d-m-Y");
  $isttimestamp = date('Y-m-d H:i:s');
  $ses_id= preg_replace('/[^A-Za-z0-9\-]/', '', $_SESSION['sess_id']);

  define ("filesplace","files/".$ses_id."/");
  /// generating QR Code
  //set it to writable location, a place for temp generated PNG files
  $PNG_TEMP_DIR = dirname(__FILE__).DIRECTORY_SEPARATOR.'temp'.DIRECTORY_SEPARATOR.$ses_id.DIRECTORY_SEPARATOR;

  //html PNG location prefix
  $PNG_WEB_DIR = 'temp/'.$ses_id.'/';

  include "qrcode/qrlib.php";
  require_once('tcpdf/tcpdf.php');
  require_once('fpdi/fpdi.php');
  require_once('tcpdf/examples/tcpdf_include.php');

  //var_dump($_SESSION['orderid']);die;

  //ofcourse we need rights to create temp dir
  if (!file_exists($PNG_TEMP_DIR))
      mkdir($PNG_TEMP_DIR);

  //processing form input
  //remember to sanitize user input in real-life solution !!!
  $errorCorrectionLevel = 'H';
  $matrixPointSize = 4;

  $order_id_array = $_SESSION['orderid'];
  $stype_arr = explode('#', $_SESSION['stype']);
  $title_arr = explode('#', $_SESSION['title']);
  $file_path = array();
  $file_name = array();

  foreach ($order_id_array as $key => $qrid) {

  // user data
  $filename = $PNG_TEMP_DIR.$qrid.'.png';

//Co Authors name and other details
  if(isset($_SESSION['coa']))
  {
    $coa_arr = $_SESSION['coa'];
    $name_arr = array_column($coa_arr, 'name');
    $name_csv = implode(',',$name_arr);

    $membership_arr = array_column($coa_arr, 'membership');
    $membership_csv = implode(',',$membership_arr);

    $email_arr = array_column($coa_arr, 'email');
    $email_csv = implode(',',$email_arr);
  }
  else {
    $coa_arr = 0;
  }

  if($coa_arr){
    QRcode::png('Name: '.$_SESSION['name'].'Co Authors'.$name_csv.' Title of Script:'.$title_arr[$key].'Type: '.$stype_arr[$key].' timestamp:'.date('Y-m-d H:i:s'), $filename, $errorCorrectionLevel, $matrixPointSize, 2);
  }
  else {
    QRcode::png('Name: '.$_SESSION['name'].' Title of Script:'.$title_arr[$key].'Type: '.$stype_arr[$key].' timestamp:'.date('Y-m-d H:i:s'), $filename, $errorCorrectionLevel, $matrixPointSize, 2);
  }
	////// Qr Code Generated
  $fs="files/".$ses_id;
  $name = trim($order_id_array[$key]);

  // require_once('tcpdf/tcpdf.php');

  // require_once('fpdi/fpdi.php');
  // require_once('tcpdf/examples/tcpdf_include.php');
  $pdf = new FPDI();

  //Set the source PDF file
  $pagecount = $pdf->setSourceFile(filesplace."$name.pdf");
  // taking to payment gateway

  //add pages of pdf

  $i = 1;
  while ($i <= $pagecount)
  {
    $pdf->AddPage();
    $pdf-> SetAutoPageBreak('on',0.5);
    //Import the first page of the file
    $tpl = $pdf->importPage($i,$boxName = '/MediaBox');
    //Use this page as template
    $pdf->useTemplate($tpl);

    #Print Hello World at the bottom of the page
    $pdf->Image('images/pdflogo2.png',8, 2,15);
    // Arial bold 15
    $pdf->SetFont('Helvetica', '', 10);
    $pdf->SetFillColor(111,111,111);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(25, 10);
    $pdf->MultiCell(0, 6, $title_arr[$key] , 0 , 'L' , 1);
    $pdf->SetXY(160, 10);
    $pdf->MultiCell(0, 6,'Page:'.$i.'/'.$pagecount, 0 ,'R' ,1);
    // Position at 1.5 cm from bottom
    $pdf->SetY(-20);
    // Arial italic 8
    $pdf->SetFont('Helvetica','I',8);
    // Page number
    $pdf->SetFont('Helvetica', '', 10);
    // $pdf->SetFillColor(244,147,136);
    // $pdf->SetTextColor(12,2,1);
    $pdf->SetFillColor(111,111,111);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(10,-20);
    $pdf->MultiCell(0, 6, 'Registered @ www.swaindia.org on '.date('Y-m-d') , 0 , 'L' , 1);
    $pdf->SetXY(100,-20);
    if($coa_arr){
      $pdf->MultiCell(0, 6,' © '.$_SESSION['name'].' , '.$name_csv, 0 ,'R' ,1);
    }
    else {
      $pdf->MultiCell(0, 6,' © '.$_SESSION['name'] , 0 ,'R' ,1);
    }
    //Go to 1.5 cm from bottom
    $i= $i +1;
  }

    // print Cert
    $pdf->AddPage();
    $pdf-> SetAutoPageBreak('on',0);
    #Print Hello World at the bottom of the page

    // Move to the right
    $pdf->Cell(80);
    // Title
    $pdf->SetY( +2 );
    $pdf->SetX( +35 );
    $pdf->SetFillColor(111,111,111);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetFont('Helvetica', '', 10);
    $pdf->SetXY(25, 10);
    $pdf->MultiCell(0, 6,  $title_arr[$key] , 0 , 'L' , 1);
    $pdf->SetXY(160, 10);
    $pdf->MultiCell(0, 6,'Page:'.$i.'/'.$pagecount, 0 ,'R' ,1);
    $pdf->SetFillColor(255,255,255);
    $pdf->MultiCell(0, 6,' ', 0 ,'R' ,1);
    $pdf->MultiCell(0, 6,' ', 0 ,'R' ,1);
    $pdf->MultiCell(0, 6,' ', 0 ,'R' ,1);
    $pdf->MultiCell(0, 6,' ', 0 ,'R' ,1);

    $pdf->SetFont('Helvetica','B',14);
    $pdf->SetTextColor(255, 0, 0);
    $pdf->MultiCell(0, 6, 'Certificate of Registration' , 0 , 'C' , 1);
    $pdf->MultiCell(0, 6,' ', 0 ,'R' ,1);
    $pdf->SetFont('Helvetica','B',10);
    $pdf->SetTextColor(0, 0, 0, 100);
    $pdf->MultiCell(0, 6, 'This is to certify that I have registered this '.$stype_arr[$key], 0 , 'C' , 1);
    $pdf->MultiCell(0, 6, 'titled  '. $title_arr[$key], 0 , 'C' , 1);
      if($coa_arr){
        $pdf->MultiCell(0, 6, 'Written by '. $_SESSION['name'].' with '.$name_csv , 0 , 'C' , 1);
        $pdf->MultiCell(0, 6, 'Whose SWA Membership No. are '. $_SESSION['prn'] .', '.$membership_csv.' respectively ' , 0 , 'C' , 1);
    }
    else {
      $pdf->MultiCell(0, 6, 'Written by '. $_SESSION['name'] , 0 , 'C' , 1);
      $pdf->MultiCell(0, 6, 'Whose SWA Membership No. is '. $_SESSION['prn']  , 0 , 'C' , 1);
    }
    $pdf->MultiCell(0, 6, 'On '.date('d/m/Y'), 0 , 'C' , 1);
    // $pdf->MultiCell(0, 6, 'On '.$date , 0 , 'C' , 1);
    $pdf->MultiCell(0, 6, '& as a proof thereof is placed below my digital signature and'  , 0 , 'C' , 1);
    $pdf->MultiCell(0, 6, ' seal of the Association with relevant details in the QR code.'  , 0 , 'C' , 1);
    $pdf->MultiCell(0, 6, '(CC Avenue) Reference No.:'. $_SESSION['trk_id'].' and Order Id:'.$name, 0 , 'C' , 1);
    $pdf->MultiCell(0, 6,' ', 0 ,'R' ,1);
    $pdf->MultiCell(0, 6,' ', 0 ,'R' ,1);

    //$pdf->Image('images/pdflogo.png',2, 2,15);
    // Arial bold 15

    $pdf->SetFont('Helvetica','B',10);
    $pdf->SetTextColor(0, 0, 0, 100);
    $pdf->MultiCell(0, 6, 'ZAMAN HABIB ', 0 , 'R' , 1);
    $pdf->MultiCell(0, 6, '(General Secretary SWA)', 0 , 'R' , 1);
    $pdf->MultiCell(0, 6,' ', 0 ,'R' ,1);
    $pdf->MultiCell(0, 6,' ', 0 ,'R' ,1);
    $pdf->MultiCell(0, 6,' ', 0 ,'R' ,1);
    $pdf->MultiCell(0, 6,' ', 0 ,'R' ,1);
    $pdf->MultiCell(0, 6,' ', 0 ,'R' ,1);
    $pdf->MultiCell(0, 6,' ', 0 ,'R' ,1);
    $pdf->MultiCell(0, 6,' ', 0 ,'R' ,1);

    $pdf->SetFont('Helvetica','B',9);
    if($coa_arr){
        $pdf->MultiCell(0, 6, 'Note - This certificate is subject to the declaration by the writers that This work is our original creation. We  hereby declare that we have neither read it anywhere nor watched it in any Film/TV show. In case it is found otherwise we understand that our registration of this work will automatically stand cancelled and we will be solely responsible for the consequences whatsoever.', 0 , 'L' , 1);
  }
  else {
    $pdf->MultiCell(0, 6, 'Note - This certificate is subject to the declaration by the writer that This work is my original creation. I hereby declare that I have neither read it anywhere nor watched it in any Film/TV show. In case it is found otherwise I understand that my registration of this work will automatically stand cancelled and I will be solely responsible for the consequences whatsoever.', 0 , 'L' , 1);
  }
    $pdf->SetFont('Helvetica','B',8);
    $pdf->SetTextColor(255, 0, 0);
    $pdf->MultiCell(0, 6, 'Tampering with document cancels the digital signature & thus the registration.', 0 , 'C' , 1);

    // Position at 1.5 cm from bottom
    $pdf->SetY(-20);
    // Arial italic 8
    $pdf->SetFont('Helvetica','I',8);
    // Page number
    $date=new DateTime(); //this returns the current date time
    $result = $date->format('d-m-Y');
    $pdf->SetFillColor(111,111,111);
    $pdf->SetTextColor(255,255,255);
    //$footer= 'Registred at The Flim Writers Association  on '.date('Y-m-d H:i:s').'  Page:0/'.$pagecount;
    $pdf->SetFont('Helvetica', '', 10);
    $pdf->SetXY(10,-20);
    $pdf->MultiCell(0, 6, 'Registred @ www.swaindia.org on '.date('Y-m-d') , 0 , 'L' , 1);
    $pdf->SetXY(100,-20);
    // $pdf->MultiCell(0, 6,' © '.$_SESSION['name'] , 0 ,'R' ,1);
    if($coa_arr){
      $pdf->MultiCell(0, 6,' © '.$_SESSION['name'].' , '.$name_csv, 0 ,'R' ,1);
      $all_auth = $_SESSION['name'].' with '.$name_csv;
    }
    else {
      $pdf->MultiCell(0, 6,' © '.$_SESSION['name'] , 0 ,'R' ,1);
      $all_auth = $_SESSION['name'];
    }

    $pdf->Image($filename,75, 195, 50, 50);

    // set certificate file
    $certificate = 'file://tcpdf/examples/data/cert/www_filmwritersassociation_org.crt';
    $pkey= 'file://tcpdf/examples/data/cert/www_filmwritersassociation_org.key';

    // set additional information
    $info = array(
    'Name' => 'ZAMAN HABIB',
    'Location' => 'SWA, Mumbai',
    'Reason' => 'Registration',
    'ContactInfo' => 'http://www.swaindia.org',
    );

    // set document signature

    $pdf->setSignature($certificate, $pkey, '123456', '', 2, $info);

    //Go to 1.5 cm from bottom
    // create content for signature (image and/or text)
    $pdf->Image('tcpdf/examples/images/fwa_signature2.png', 160,121, 30, 42, 'PNG');

    // define active area for signature appearance
    $pdf->setSignatureAppearance(160, 110, 30,42);

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    // *** set an empty signature appearance ***
    $pdf->addEmptySignatureAppearance(160, 80, 30, 42);

    // $newname = $name."_mod";
    $newname = $name."_".$_SESSION['name'];
    $pdf->Output(filesplace."/$newname.pdf", "F");
    array_push($file_path, filesplace."$newname.pdf");
    array_push($file_name, $newname.".pdf");
//////sending mail
//sending activation codeto user
  }
  // echo '<pre>';
  // var_dump($name_arr);
  // echo '</pre>';
  // die;
$stype_csv = str_replace('#',',',$_SESSION['stype']);
$title_csv = str_replace('#',',',$_SESSION['title']);
$str = '';

foreach ($stype_arr as $key => $types) {
$str = $str.$types.' titled '.$title_arr[$key];
}

$body = "
Dear ".$_SESSION['name']."<br/><br/>
Thank you for registering ".$str." </strong> by ".$all_auth." with SWA! Your registered file/s is/are attached with this email.
<br><br>
The registered soft copies carry the proof of registration viz. certificate, stamp QR code and the digital signature. For secrecy and security purposes, SWA website DOES NOT save your files/work. Therefore, only you are responsible for the safekeeping of your registered files. Any attempt to tamper with the document will invalidate the digital signature and cancel the registration.<br><br>
Team SWA ​
<br />
<img src='cid:logo_sign_swa'>
";

date_default_timezone_set('Asia/Kolkata');
require_once('class.phpmailer.php');
include("class.smtp.php"); // optional, gets called from within class.phpmailer.php if not already loaded
$mail = new PHPMailer();
// $body  = ob_get_clean();
$mail->IsSMTP(); // telling the class to use SMTP
$mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
                                         // 1 = errors and messages
// $mail->SMTPSecure = "ssl";                 // sets the prefix to the servier                                          // 2 = messages only
$mail->SMTPAuth   = true;                  // enable SMTP authentication
$mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
$mail->Host       = "smtp.gmail.com";      // sets GMAIL as the SMTP server
$mail->Port       = 465;                   // set the SMTP port for the GMAIL server

// $mail->Username   = "registration+fwa.co.in";  // GMAIL username
// $mail->Password   = "SoDGVOayzHJ8";            // GMAIL password
$mail->Username   = "swaindiaosr@gmail.com";  // GMAIL username
$mail->Password   = "SoDGVOayzHJ8";            // GMAIL password
//$mail->g_smtp_host = 'smtp.gmail.com:465';
//$mail->g_smtp_connection_mode = 'ssl';

$mail->SetFrom('registration@swaindia.org', 'SWA Registration System');
$mail->AddReplyTo('registration@swaindia.org', 'SWA Registration System');
$mail->Subject = "SWA Registration of ".$title_csv;
$mail->AltBody = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
$mail->MsgHTML($body);
$mail->SetLanguage("en", 'includes/phpMailer/language/');
$mail->AddEmbeddedImage('images/swa_sign_logo.jpg', 'logo_sign_swa');
foreach ($file_path as $attch) {
  $mail->AddAttachment($attch);      // attachment
}

$mail->AddAddress($_SESSION['User_Email'],$_SESSION['name']);
if($coa_arr){
  foreach ($email_arr as $key => $email) {
    $mail->AddAddress($email,$name_arr[$key]);
  }
}

// $mail->AddAddress('nirmeet@firsteconomy.com');
$mail->Send();
// dump into db
include("includes/config.php");
if(!isset($_SESSION['done']))
{
  foreach ($file_path as $k => $attch) {
  $sql= "INSERT INTO `scr_reg` (`reg_no`, `ord_id`, `date`, `stype`, `title`, `file_name`, `sess_id`, `ack1`, `email_ack`,`co_authors`) VALUES ('".$_SESSION['prn']."', '".$order_id_array[$k]."', '".date('Y-m-d H:i:s')."', '".$stype_arr[$k]."', '".$title_arr[$k]."', '".$file_name[$k]."', '".$ses_id."', 1, '0','".$name_csv."')";
    $result2 = mysqli_query($db,$sql);
  }
  $_SESSION['done'] = 1;
}
// var_dump($sql);die;
?>
<!doctype html>
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="icon" type="image/x-icon" href=""/>
      <title>Film Writers Association | Instruction</title>
      <link href="css/custom.css" rel="stylesheet" type="text/css">
      <link href="css/color.css" rel="stylesheet" type="text/css">
      <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
      <link href="css/responsive.css" rel="stylesheet" type="text/css">
      <link href="css/jquery.bxslider.css" rel="stylesheet" type="text/css">
      <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
      <link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen"/>
      <link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,300' rel='stylesheet' type='text/css'>
      <link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
   </head>
   <body class="inner-page">
      <div id="wrapper" class="inside-menu">
         <?php include_once('header.php'); ?>
         <div id="cp-content-wrap" class="page404 cp-login-page">
            <div class="container">
               <div class="row">
                  <div class="col-md-8 col-md-offset-2 main-reg-div">
                     <div class="cp-reg-box sub-reg-div">
                        <h4>Script Download</h4>
                        <?php foreach ($file_path as $ky => $attch) { ?>
                        <p class="text-center">
                            <center>
                              <a href ='<?php echo $attch; ?>' target=_blank>
                                  <h2>Click here to download your file:  <?php echo $title_arr[$ky]; ?></h2>
                              </a>
                            </center>
                        </p>
                        <?php } ?>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <?php include_once('footer.php'); ?>
      </div>
      <script src="js/jquery-1.11.3.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="js/jquery.bxslider.min.js"></script>
      <script src="js/owl.carousel.min.js"></script>
      <script src="js/jquery.prettyPhoto.js"></script>
      <script src="js/custom.js"></script>
   </body>
</html>
