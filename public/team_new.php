<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/x-icon" href=""/>
  <title>Film Writers Association | People at SWA</title>
  <link href="css/custom.css" rel="stylesheet" type="text/css">
  <link href="css/color.css" rel="stylesheet" type="text/css">
  <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
  <link href="css/responsive.css" rel="stylesheet" type="text/css">
  <link href="css/jquery.bxslider.css" rel="stylesheet" type="text/css">
  <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen"/>
  <link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,300' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
    #filters {
      margin:1%;
      padding:0;
      list-style:none;
    }
    #filters li {
      float:left;
    }
    #filters li span {
      margin-left: 4px;
      border: 1px solid #000;
      display: block;
      padding: 5px 20px;
      text-decoration: none;
      color: #666;
      cursor: pointer;
    }
    #filters li span.active {
      background: #000;
      color:#fff;
    }
    #portfoliolist .portfolio {
      -webkit-box-sizing: border-box;
      -moz-box-sizing: border-box;
      -o-box-sizing: border-box;
      width:10%;
      margin:1%;
      display:none;
      float:left;
      overflow:hidden;
    }
    .portfolio-wrapper {
      overflow:hidden;
      position: relative !important;
      background: #666;
      cursor:pointer;
    }
    .portfolio img {
      max-width:100%;
      position: relative;
      top:0;
      -webkit-transition: all 600ms cubic-bezier(0.645, 0.045, 0.355, 1);
      transition:         all 600ms cubic-bezier(0.645, 0.045, 0.355, 1);   
    }
    .portfolio .label {
      position: absolute;
      width: 100%;
      height:40px;
      bottom:-40px;
      -webkit-transition: all 300ms cubic-bezier(0.645, 0.045, 0.355, 1);
      transition:         all 300ms cubic-bezier(0.645, 0.045, 0.355, 1);
    }
    .portfolio .label-bg {
      background: #000;
      width: 100%;
      height:100%;
      position: absolute;
      top:0;
      left:0;
    }
    .portfolio .label-text {
      color:#fff;
      position: relative;
      z-index:500;
      padding:5px 8px;
    }
    .portfolio .text-category {
      display:block;
      font-size:9px;
      margin-top:5px;
    }
    .portfolio:hover .label {
      bottom:0;
    }
    .portfolio:hover img {
      top:-30px;
    }  
    @media only screen and (min-width: 768px) and (max-width: 959px) {
      .container {
        width: 768px; 
      }
    }
    /*  #Mobile (Portrait) - Note: Design for a width of 320px */
    @media only screen and (max-width: 767px) {
      .container { 
        width: 95%; 
      }
      #portfoliolist .portfolio {
        width:48%;
        margin:1%;
      }   
    }
    /* #Mobile (Landscape) - Note: Design for a width of 480px */
    @media only screen and (min-width: 480px) and (max-width: 767px) {
      .container {
        width: 70%;
      }
    }
    .text-title{
      color:#fff;
    }
    .text-title:hover {
      color: #fff;
      text-decoration: none;
    }



    .tab {
      padding: 30px 0;
    }
    .tabs {
      display: table;
      position: relative;
      overflow: hidden;
      margin: 0;
      width: 100%;
    }
    .tabs li {
      float: left;
      line-height: 38px;
      overflow: hidden;
      padding: 0;
      position: relative;
    }
    .tabs a {
      background-color: #eff0f2;
      border-bottom: 1px solid #fff;
      color: #888;
      font-weight: 500;
      display: block;
      letter-spacing: 0;
      outline: none;
      padding: 0 20px;
      text-decoration: none;
      -webkit-transition: all 0.2s ease-in-out;
      -moz-transition: all 0.2s ease-in-out;
      transition: all 0.2s ease-in-out;
      border-bottom: 2px solid #000;
    }

    .tabs_item {
      display: none;
    }
    .tabs_item h4 {
      font-weight: bold;
      color: #87d3b7;
      font-size: 20px;
    }
   /* .tabs_item img {
      width: 200px;
      float: left;
      margin-right: 30px;
    }*/
    .tabs_item:first-child {
      display: block;
    }

    .current a {
      color: #fff;
      background: #000;
    }
  </style>
</head>
<body class="inner-page">
  <div id="wrapper">
    <?php include_once('header.php'); ?>
    <div id="cp-content-wrap">
      <!-- Banner -->
      <div class="banner_inner">
        <img src="images/bg.jpg">
      </div>
      <!-- End of Banner -->
      <div class="cp_our-story-section tab">
        <div class="container">
          <div class="row">  
            <div class="col-md-12">
              <ul class="tabs">
                <li><a href="javascript:void(0)">Executive Committee</a></li>
                <li><a href="javascript:void(0)">Sub Committee</a></li>
                <li><a href="javascript:void(0)">Staff</a></li>
              </ul> <!-- / tabs -->
            </div>
          </div>
        </div>  
        <div class="tab_content">
          <div class="tabs_item"> 
            <div class="container">
              <div class="row">
                <div class="col-md-12">
                  <div class="cp-acticle-box">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                      <!--content start here-->
                      <ul id="filters" class="clearfix">
                        <!-- <li><span class="filter" data-filter=".exe">Core Members</span></li> -->
                        <li><span class="filter" data-filter=".dsc">DSC</span></li>
                        <li><span class="filter" data-filter=".wel">Welfare</span></li>
                        <li><span class="filter" data-filter=".mvc"> Membership-Verification</span></li>
                        <li><span class="filter" data-filter=".legal">Legal</span></li>
                        <li><span class="filter" data-filter=".cc">Constitution (Bye-laws)</span></li>
                        <li><span class="filter" data-filter=".ev">Event</span></li>
                        <li><span class="filter" data-filter=".dscbible">DSC Bible</span></li>
                        <li><span class="filter" data-filter=".media">Media</span></li>
                        <li><span class="filter" data-filter=".mbc">Film – MBC</span></li>
                        <li><span class="filter" data-filter=".tvmbc"> TV – MBC</span></li>
                        <li><span class="filter" data-filter=".lmbc"> Lyricist MBCC</span></li>
                        <li><span class="filter" data-filter=".web"> Website Editorial Board</span></li>
                      </ul>
                      <div id="portfoliolist"> 
                      <!--exe start-->
              <!-- <div class="portfolio exe" data-cat="exe" data-toggle="modal" data-target="#preeti" >
                <div class="portfolio-wrapper">
                  <img src="img/exe/PreetiMamgain.jpg" alt="" />
                  <div class="label">
                    <div class="label-text">
                      <a class="text-title">Preeti Mamgain</a>
                      <span class="text-category">PRESIDENT</span>
                    </div>
                    <div class="label-bg"></div>
                  </div>
                </div>
              </div>
              <div class="portfolio exe" data-cat="exe">
                <div class="portfolio-wrapper">
                  <img src="img/exe/ZamanHabib.jpg" alt="" />
                  <div class="label">
                    <div class="label-text">
                      <a class="text-title">Zaman Habib</a>
                      <span class="text-category">GENERAL SECRETARY</span>
                    </div>
                    <div class="label-bg"></div>
                  </div>
                </div>
              </div>
              <div class="portfolio exe" data-cat="exe">
                <div class="portfolio-wrapper">
                  <img src="img/exe/DhananjayKumar.jpg" alt="" />
                  <div class="label">
                    <div class="label-text">
                      <a class="text-title">Dhananjay Kumar</a>
                      <span class="text-category">TREASURER</span>
                    </div>
                    <div class="label-bg"></div>
                  </div>
                </div>
              </div>
              <div class="portfolio exe" data-cat="exe">
                <div class="portfolio-wrapper">
                  <img src="img/exe/DanishJaved.jpg" alt="" />
                  <div class="label">
                    <div class="label-text">
                      <a class="text-title">Danish Javed</a>
                      <span class="text-category">VICE PRESIDENT</span>
                    </div>
                    <div class="label-bg"></div>
                  </div>
                </div>
              </div>
              <div class="portfolio exe" data-cat="exe">
                <div class="portfolio-wrapper">
                  <img src="img/exe/VinodRangnath.jpg" alt="" />
                  <div class="label">
                    <div class="label-text">
                      <a class="text-title">Vinod Rangnath</a>
                      <span class="text-category">VICE PRESIDENT</span>
                    </div>
                    <div class="label-bg"></div>
                  </div>
                </div>
              </div>
              <div class="portfolio exe" data-cat="exe">
                <div class="portfolio-wrapper">
                  <img src="img/exe/ManishaKorde.jpg" alt="" />
                  <div class="label">
                    <div class="label-text">
                      <a class="text-title">Manisha Korde</a>
                      <span class="text-category">JOINT SECRETARY</span>
                    </div>
                    <div class="label-bg"></div>
                  </div>
                </div>
              </div>
              <div class="portfolio exe" data-cat="exe">
                <div class="portfolio-wrapper">
                  <img src="img/exe/AbhijeetDeshpande.jpg" alt="" />
                  <div class="label">
                    <div class="label-text">
                      <a class="text-title">Abhijeet Deshpande</a>
                      <span class="text-category">JOINT SECRETARY</span>
                    </div>
                    <div class="label-bg"></div>
                  </div>
                </div>
              </div>
              <div class="portfolio exe" data-cat="exe">
                <div class="portfolio-wrapper">
                  <img src="img/exe/JaleesSherwani.jpg" alt="" />
                  <div class="label">
                    <div class="label-text">
                      <a class="text-title">Jalees Sherwani</a>
                    </div>
                    <div class="label-bg"></div>
                  </div>
                </div>
              </div>
              <div class="portfolio exe" data-cat="exe">
                <div class="portfolio-wrapper">
                  <img src="img/exe/RobinBhatt.jpg" alt="" />
                  <div class="label">
                    <div class="label-text">
                      <a class="text-title">Robin Bhatt</a>
                    </div>
                    <div class="label-bg"></div>
                  </div>
                </div>
              </div>
              <div class="portfolio exe" data-cat="exe">
                <div class="portfolio-wrapper">
                  <img src="img/exe/AnjumRajabali.jpg" alt="" />
                  <div class="label">
                    <div class="label-text">
                      <a class="text-title">Anjum Rajabali</a>
                    </div>
                    <div class="label-bg"></div>
                  </div>
                </div>
              </div>
              <div class="portfolio exe" data-cat="exe">
                <div class="portfolio-wrapper">
                  <img src="img/exe/RajeshDubey.jpg" alt="" />
                  <div class="label">
                    <div class="label-text">
                      <a class="text-title">Rajesh Dubey</a>
                    </div>
                    <div class="label-bg"></div>
                  </div>
                </div>
              </div>
              <div class="portfolio exe" data-cat="exe">
                <div class="portfolio-wrapper">
                  <img src="img/exe/SwanandKirkire.jpg" alt="" />
                  <div class="label">
                    <div class="label-text">
                      <a class="text-title">Swanand Kirkirey</a>
                    </div>
                    <div class="label-bg"></div>
                  </div>
                </div>
              </div>
              <div class="portfolio exe" data-cat="exe">
                <div class="portfolio-wrapper">
                  <img src="img/exe/SANJAYCHHEL.jpg" alt="" />
                  <div class="label">
                    <div class="label-text">
                      <a class="text-title">SANJAY CHHEL</a>
                    </div>
                    <div class="label-bg"></div>
                  </div>
                </div>
              </div>
              <div class="portfolio exe" data-cat="exe">
                <div class="portfolio-wrapper">
                  <img src="img/exe/ShridharRaghvan.jpg" alt="" />
                  <div class="label">
                    <div class="label-text">
                      <a class="text-title">Shridhar Raghvan</a>
                    </div>
                    <div class="label-bg"></div>
                  </div>
                </div>
              </div>
              <div class="portfolio exe" data-cat="exe">
                <div class="portfolio-wrapper">
                  <img src="img/exe/SanjayChouhan.jpg" alt="" />
                  <div class="label">
                    <div class="label-text">
                      <a class="text-title">Sanjay Chouhan</a>
                    </div>
                    <div class="label-bg"></div>
                  </div>
                </div>
              </div>
              <div class="portfolio exe" data-cat="exe">
                <div class="portfolio-wrapper">
                  <img src="img/exe/MeghnaGulzar.jpg" alt="" />
                  <div class="label">
                    <div class="label-text">
                      <a class="text-title">Meghna Gulzary</a>
                    </div>
                    <div class="label-bg"></div>
                  </div>
                </div>
              </div>
              <div class="portfolio exe" data-cat="exe">
                <div class="portfolio-wrapper">
                  <img src="img/exe/SatyamTripathi.jpg" alt="" />
                  <div class="label">
                    <div class="label-text">
                      <a class="text-title">Satyam Tripathi</a>
                    </div>
                    <div class="label-bg"></div>
                  </div>
                </div>
              </div>
              <div class="portfolio exe" data-cat="exe">
                <div class="portfolio-wrapper">
                  <img src="img/exe/SanjaySharma.jpg" alt="" />
                  <div class="label">
                    <div class="label-text">
                      <a class="text-title">Sanjay Sharma</a>
                    </div>
                    <div class="label-bg"></div>
                  </div>
                </div>
              </div>
              <div class="portfolio exe" data-cat="exe">
                <div class="portfolio-wrapper">
                  <img src="img/exe/Jyoti-Kapoor.jpg" alt="" />
                  <div class="label">
                    <div class="label-text">
                      <a class="text-title">Jyoti Kapoor</a>
                    </div>
                    <div class="label-bg"></div>
                  </div>
                </div>
              </div>
              <div class="portfolio exe" data-cat="exe">
                <div class="portfolio-wrapper">
                  <img src="img/exe/SachinDarekar.jpg" alt="" />
                  <div class="label">
                    <div class="label-text">
                      <a class="text-title">Sachin Darekar</a>
                    </div>
                    <div class="label-bg"></div>
                  </div>
                </div>
              </div>
              <div class="portfolio exe" data-cat="exe">
                <div class="portfolio-wrapper">
                  <img src="img/exe/saumyajoshi.jpg" alt="" />
                  <div class="label">
                    <div class="label-text">
                      <a class="text-title">saumya joshi</a>
                    </div>
                    <div class="label-bg"></div>
                  </div>
                </div>
              </div>
              <div class="portfolio exe" data-cat="exe">
                <div class="portfolio-wrapper">
                  <img src="img/exe/SunilSalgia.jpg" alt="" />
                  <div class="label">
                    <div class="label-text">
                      <a class="text-title">Sunil Salgia</a>
                    </div>
                    <div class="label-bg"></div>
                  </div>
                </div>
              </div>
              <div class="portfolio exe" data-cat="exe">
                <div class="portfolio-wrapper">
                  <img src="img/exe/ManojHansraj.jpg" alt="" />
                  <div class="label">
                    <div class="label-text">
                      <a class="text-title">Manoj Hansraj</a>
                    </div>
                    <div class="label-bg"></div>
                  </div>
                </div>
              </div>
              <div class="portfolio exe" data-cat="exe">
                <div class="portfolio-wrapper">
                  <img src="img/exe/AtikaChouhan.jpg" alt="" />
                  <div class="label">
                    <div class="label-text">
                      <a class="text-title">Atika Chouhan</a>
                    </div>
                    <div class="label-bg"></div>
                  </div>
                </div>
              </div>
              <div class="portfolio exe" data-cat="exe">
                <div class="portfolio-wrapper">
                  <img src="img/exe/RadhikaBorkar.jpg" alt="" />
                  <div class="label">
                    <div class="label-text">
                      <a class="text-title">Radhika Borkar</a>
                    </div>
                    <div class="label-bg"></div>
                  </div>
                </div>
              </div>
              <div class="portfolio exe" data-cat="exe">
                <div class="portfolio-wrapper">
                  <img src="img/exe/MitaliMahajan.jpg" alt="" />
                  <div class="label">
                    <div class="label-text">
                      <a class="text-title">Mitali Mahajan</a>
                    </div>
                    <div class="label-bg"></div>
                  </div>
                </div>
              </div>
              <div class="portfolio exe" data-cat="exe">
                <div class="portfolio-wrapper">
                  <img src="img/exe/ShahabAllahabadi.jpg" alt="" />
                  <div class="label">
                    <div class="label-text">
                      <a class="text-title">Shahab Allahabadi</a>
                    </div>
                    <div class="label-bg"></div>
                  </div>
                </div>
              </div>
              <div class="portfolio exe" data-cat="exe">
                <div class="portfolio-wrapper">
                  <img src="img/exe/Debashish.jpg" alt="" />
                  <div class="label">
                    <div class="label-text">
                      <a class="text-title">Debashish</a>
                    </div>
                    <div class="label-bg"></div>
                  </div>
                </div>
              </div>
              <div class="portfolio exe" data-cat="exe">
                <div class="portfolio-wrapper">
                  <img src="img/exe/AashishPawaskar.jpg" alt="" />
                  <div class="label">
                    <div class="label-text">
                      <a class="text-title">Aashish Pawaskary</a>
                    </div>
                    <div class="label-bg"></div>
                  </div>
                </div>
              </div>
              <div class="portfolio exe" data-cat="exe">
                <div class="portfolio-wrapper">
                  <img src="img/exe/KalyaniPandit.jpg" alt="" />
                  <div class="label">
                    <div class="label-text">
                      <a class="text-title">Kalyani Pandit</a>
                    </div>
                    <div class="label-bg"></div>
                  </div>
                </div>
              </div>
              <div class="portfolio exe" data-cat="exe">
                <div class="portfolio-wrapper">
                  <img src="img/exe/ParakhChouhan.jpg" alt="" />
                  <div class="label">
                    <div class="label-text">
                      <a class="text-title">Parakh Chouhan</a>
                    </div>
                    <div class="label-bg"></div>
                  </div>
                </div>
              </div> -->
              <!--exe end here-->     
                        <!--dsc start here-->
                        <div class="portfolio dsc" data-cat="dsc">
                          <div class="portfolio-wrapper">
                            <img src="img/dsc/VinodRangnath.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Vinod Rangnath</a>
                                <span class="text-category">CHAIRMAN</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio dsc" data-cat="dsc">
                          <div class="portfolio-wrapper">
                            <img src="img/dsc/Jyoti-Kapoor.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Jyoti-Kapoor</a>
                                <span class="text-category">CONVENER</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio dsc" data-cat="dsc">
                          <div class="portfolio-wrapper">
                            <img src="img/dsc/RajeshDubey.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Rajesh Dubey</a>
                                <span class="text-category">MEMBER</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio dsc" data-cat="dsc">
                          <div class="portfolio-wrapper">
                            <img src="img/dsc/DanishJaved.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Danish Javed</a>
                                <span class="text-category">MEMBER</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio dsc" data-cat="dsc">
                          <div class="portfolio-wrapper">
                            <img src="img/dsc/DhananjayKumar.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Dhananjay Kumar</a>
                                <span class="text-category">MEMBER</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio dsc" data-cat="dsc">
                          <div class="portfolio-wrapper">
                            <img src="img/dsc/AashishPawaskar.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Aashish Pawaskar</a>
                                <span class="text-category">MEMBER</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio dsc" data-cat="dsc">
                          <div class="portfolio-wrapper">
                            <img src="img/dsc/AtikaChouhan.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Atika Chouhan</a>
                                <span class="text-category">MEMBER</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio dsc" data-cat="dsc">
                          <div class="portfolio-wrapper">
                            <img src="img/dsc/saumyajoshi.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">saumya joshi</a>
                                <span class="text-category">MEMBER</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio dsc" data-cat="dsc">
                          <div class="portfolio-wrapper">
                            <img src="img/dsc/SunilSalgia.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Sunil Salgia</a>
                                <span class="text-category">MEMBER</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio dsc" data-cat="dsc">
                          <div class="portfolio-wrapper">
                            <img src="img/dsc/ParakhChouhan.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Parakh Chouhan</a>
                                <span class="text-category">MEMBER</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio dsc" data-cat="dsc">
                          <div class="portfolio-wrapper">
                            <img src="img/dsc/RadhikaBorkar.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Radhika Borkar</a>
                                <span class="text-category">MEMBER</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio dsc" data-cat="dsc">
                          <div class="portfolio-wrapper">
                            <img src="img/dsc/PreetiMamgain.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Preeti Mamgain</a>
                                <span class="text-category">EX-OFFICIO</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio dsc" data-cat="dsc">
                          <div class="portfolio-wrapper">
                            <img src="img/dsc/ZamanHabib.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Zaman Habib</a>
                                <span class="text-category">EX-OFFICIO</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <!--dsc end here-->
                        <!--welfare start here-->
                        <div class="portfolio wel" data-cat="wel">
                          <div class="portfolio-wrapper">
                            <img src="img/wel/RobinBhatt.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Robin Bhatt</a>
                                <span class="text-category">CHAIRMAN</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio wel" data-cat="wel">
                          <div class="portfolio-wrapper">
                            <img src="img/wel/JaleesSherwani.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Jalees Sherwani</a>
                                <span class="text-category">MEMBER</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio wel" data-cat="wel">
                          <div class="portfolio-wrapper">
                            <img src="img/wel/DhananjayKumar.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Dhananjay Kumar</a>
                                <span class="text-category">MEMBER</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio wel" data-cat="wel">
                          <div class="portfolio-wrapper">
                            <img src="img/wel/SanjayChouhan.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Sanjay Chouhan</a>
                                <span class="text-category">MEMBER</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio wel" data-cat="wel">
                          <div class="portfolio-wrapper">
                            <img src="img/wel/SatyamTripathi.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Satyam Tripathi</a>
                                <span class="text-category">MEMBER</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio wel" data-cat="wel">
                          <div class="portfolio-wrapper">
                            <img src="img/wel/SachinDarekar.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Sachin Darekar</a>
                                <span class="text-category">MEMBER</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio wel" data-cat="wel">
                          <div class="portfolio-wrapper">
                            <img src="img/wel/ShahabAllahabadi.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Shahab Allahabadi</a>
                                <span class="text-category">MEMBER</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio wel" data-cat="wel">
                          <div class="portfolio-wrapper">
                            <img src="img/wel/ManojHansraj.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Manoj Hansraj</a>
                                <span class="text-category">MEMBER</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio wel" data-cat="wel">
                          <div class="portfolio-wrapper">
                            <img src="img/wel/PreetiMamgain.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Preeti Mamgain</a>
                                <span class="text-category">EX-OFFICIO</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio wel" data-cat="wel">
                          <div class="portfolio-wrapper">
                            <img src="img/wel/ZamanHabib.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Zaman Habib</a>
                                <span class="text-category">EX-OFFICIO</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <!--welfare end here-->
                        <!--mvc start here-->
                        <div class="portfolio mvc" data-cat="mvc">
                          <div class="portfolio-wrapper">
                            <img src="img/mvc/JaleesSherwani.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">JaleesSherwani</a>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio mvc" data-cat="mvc">
                          <div class="portfolio-wrapper">
                            <img src="img/mvc/ManojHansraj.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Manoj Hansraj</a>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio mvc" data-cat="mvc">
                          <div class="portfolio-wrapper">
                            <img src="img/mvc/ShahabAllahabadi.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Shahab Allahabadi</a>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <!--mvc end here-->
                        <!--legal start here-->
                        <div class="portfolio legal" data-cat="legal">
                          <div class="portfolio-wrapper">
                            <img src="img/legal/AnjumRajabali.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Anjum Rajabali</a>
                                <span class="text-category">CHARIMAN</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio legal" data-cat="legal">
                          <div class="portfolio-wrapper">
                            <img src="img/legal/VinodRangnath.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Vinod Rangnath</a>
                                <span class="text-category">MEMBER</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio legal" data-cat="legal">
                          <div class="portfolio-wrapper">
                            <img src="img/legal/ShridharRaghvan.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Shridhar Raghvan</a>
                                <span class="text-category">MEMBER</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio legal" data-cat="legal">
                          <div class="portfolio-wrapper">
                            <img src="img/legal/SwanandKirkire.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Swanand Kirkire</a>
                                <span class="text-category">MEMBER</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio legal" data-cat="legal">
                          <div class="portfolio-wrapper">
                            <img src="img/legal/AashishPawaskar.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Aashish Pawaskar</a>
                                <span class="text-category">MEMBER</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio legal" data-cat="legal">
                          <div class="portfolio-wrapper">
                            <img src="img/legal/Debashish.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Debashish</a>
                                <span class="text-category">MEMBER</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio legal" data-cat="legal">
                          <div class="portfolio-wrapper">
                            <img src="img/legal/AtikaChouhan.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Atika Chouhan</a>
                                <span class="text-category">MEMBER</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio legal" data-cat="legal">
                          <div class="portfolio-wrapper">
                            <img src="img/legal/PreetiMamgain.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Preeti Mamgain</a>
                                <span class="text-category">EX-OFFICIO</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio legal" data-cat="legal">
                          <div class="portfolio-wrapper">
                            <img src="img/legal/ZamanHabib.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Zaman Habib</a>
                                <span class="text-category">EX-OFFICIO</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <!--legal end here-->
                        <!--Constitution start here-->
                        <div class="portfolio cc" data-cat="cc">
                          <div class="portfolio-wrapper">
                            <img src="img/cc/JaleesSherwani.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Jalees Sherwani</a>
                                <span class="text-category">CHARIMAN</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio cc" data-cat="cc">
                          <div class="portfolio-wrapper">
                            <img src="img/cc/AnjumRajabali.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Anjum Rajabali</a>
                                <span class="text-category">MEMBER</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio cc" data-cat="cc">
                          <div class="portfolio-wrapper">
                            <img src="img/cc/DanishJaved.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Danish Javed</a>
                                <span class="text-category">MEMBER</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio cc" data-cat="cc">
                          <div class="portfolio-wrapper">
                            <img src="img/cc/SatyamTripathi.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Satyam Tripathi</a>
                                <span class="text-category">MEMBER</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio cc" data-cat="cc">
                          <div class="portfolio-wrapper">
                            <img src="img/cc/ManishaKorde.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Manisha Korde</a>
                                <span class="text-category">MEMBER</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio cc" data-cat="cc">
                          <div class="portfolio-wrapper">
                            <img src="img/cc/SanjayChouhan.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Sanjay Chouhan</a>
                                <span class="text-category">MEMBER</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio cc" data-cat="cc">
                          <div class="portfolio-wrapper">
                            <img src="img/cc/PreetiMamgain.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Preeti Mamgain</a>
                                <span class="text-category">EX-OFFICIO</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio cc" data-cat="cc">
                          <div class="portfolio-wrapper">
                            <img src="img/cc/ZamanHabib.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Zaman Habib</a>
                                <span class="text-category">EX-OFFICIO</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <!--Constitution end here-->
                        <!--event start here-->
                        <div class="portfolio ev" data-cat="ev">
                          <div class="portfolio-wrapper">
                            <img src="img/ev/DanishJaved.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Danish Javed</a>
                                <span class="text-category">CHARIMAN</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio ev" data-cat="ev">
                          <div class="portfolio-wrapper">
                            <img src="img/ev/AbhijeetDeshpande.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Abhijeet Deshpande</a>
                                <span class="text-category">CONVENER</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio ev" data-cat="ev">
                          <div class="portfolio-wrapper">
                            <img src="img/ev/SANJAYCHHEL.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">SANJAY CHHEL</a>
                                <span class="text-category">MEMBER</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio ev" data-cat="ev">
                          <div class="portfolio-wrapper">
                            <img src="img/ev/KalyaniPandit.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Kalyani Pandit</a>
                                <span class="text-category">MEMBER</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio ev" data-cat="ev">
                          <div class="portfolio-wrapper">
                            <img src="img/ev/MitaliMahajan.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Mitali Mahajan</a>
                                <span class="text-category">MEMBER</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio ev" data-cat="ev">
                          <div class="portfolio-wrapper">
                            <img src="img/ev/ParakhChouhan.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Parakh Chouhan</a>
                                <span class="text-category">MEMBER</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio ev" data-cat="ev">
                          <div class="portfolio-wrapper">
                            <img src="img/ev/ShahabAllahabadi.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Shahab Allahabadi</a>
                                <span class="text-category">MEMBER</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio ev" data-cat="ev">
                          <div class="portfolio-wrapper">
                            <img src="img/ev/PreetiMamgain.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Preeti Mamgain</a>
                                <span class="text-category">EX-OFFICIO</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio ev" data-cat="ev">
                          <div class="portfolio-wrapper">
                            <img src="img/ev/ZamanHabib.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Zaman Habib</a>
                                <span class="text-category">EX-OFFICIO</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <!--event end here-->
                        <!--dscbible start here-->
                        <div class="portfolio dscbible" data-cat="dscbible">
                          <div class="portfolio-wrapper">
                            <img src="img/dscbible/Jyoti-Kapoor.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Jyoti Kapoor</a>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio dscbible" data-cat="dscbible">
                          <div class="portfolio-wrapper">
                            <img src="img/dscbible/PreetiMamgain.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Preeti Mamgain</a>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio dscbible" data-cat="dscbible">
                          <div class="portfolio-wrapper">
                            <img src="img/dscbible/RajeshDubey.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Rajesh Dubey</a>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio dscbible" data-cat="dscbible">
                          <div class="portfolio-wrapper">
                            <img src="img/dscbible/VinodRangnath.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Vinod Rangnath</a>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio dscbible" data-cat="dscbible">
                          <div class="portfolio-wrapper">
                            <img src="img/dscbible/ZamanHabib.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Zaman Habib</a>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <!--dscbible end here-->
                        <!--media start here-->
                        <div class="portfolio media" data-cat="media">
                          <div class="portfolio-wrapper">
                            <img src="img/media/AnjumRajabali.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Anjum Rajabali</a>
                                <span class="text-category">CHAIRMAN</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio media" data-cat="media">
                          <div class="portfolio-wrapper">
                            <img src="img/media/Debashish.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Debashish</a>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio media" data-cat="media">
                          <div class="portfolio-wrapper">
                            <img src="img/media/KalyaniPandit.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Kalyani Pandit</a>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio media" data-cat="media">
                          <div class="portfolio-wrapper">
                            <img src="img/media/ManishaKorde.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Manisha Korde</a>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio media" data-cat="media">
                          <div class="portfolio-wrapper">
                            <img src="img/media/RajeshDubey.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Rajesh Dubey</a>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio media" data-cat="media">
                          <div class="portfolio-wrapper">
                            <img src="img/media/SANJAYCHHEL.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">SANJAY CHHEL</a>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio media" data-cat="media">
                          <div class="portfolio-wrapper">
                            <img src="img/media/SwanandKirkire.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Swanand Kirkire</a>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <!--media end here-->
                        <!--mbc start here-->
                        <div class="portfolio mbc" data-cat="mbc">
                          <div class="portfolio-wrapper">
                            <img src="img/mbc/AashishPawaskar.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Aashish Pawaskar</a>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio mbc" data-cat="mbc">
                          <div class="portfolio-wrapper">
                            <img src="img/mbc/AbhijeetDeshpande.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Abhijeet Deshpande</a>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio mbc" data-cat="mbc">
                          <div class="portfolio-wrapper">
                            <img src="img/mbc/AnjumRajabali.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Anjum Rajabali</a>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio mbc" data-cat="mbc">
                          <div class="portfolio-wrapper">
                            <img src="img/mbc/ManishaKorde.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Manisha Korde</a>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio mbc" data-cat="mbc">
                          <div class="portfolio-wrapper">
                            <img src="img/mbc/MeghnaGulzar.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Meghna Gulzar</a>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio mbc" data-cat="mbc">
                          <div class="portfolio-wrapper">
                            <img src="img/mbc/SanjayChouhan.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Sanjay Chouhan</a>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio mbc" data-cat="mbc">
                          <div class="portfolio-wrapper">
                            <img src="img/mbc/ShridharRaghvan.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Shridhar Raghvan</a>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <!--mbc end here-->
                        <!--tvmbc start here-->
                        <div class="portfolio tvmbc" data-cat="tvmbc">
                          <div class="portfolio-wrapper">
                            <img src="img/tvmbc/PreetiMamgain.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Preeti Mamgain</a>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio tvmbc" data-cat="tvmbc">
                          <div class="portfolio-wrapper">
                            <img src="img/tvmbc/RajeshDubey.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Rajesh Dubey</a>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio tvmbc" data-cat="tvmbc">
                          <div class="portfolio-wrapper">
                            <img src="img/tvmbc/SatyamTripathi.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Satyam Tripathi</a>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio tvmbc" data-cat="tvmbc">
                          <div class="portfolio-wrapper">
                            <img src="img/tvmbc/VinodRangnath.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Vinod Rangnath</a>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio tvmbc" data-cat="tvmbc">
                          <div class="portfolio-wrapper">
                            <img src="img/tvmbc/ZamanHabib.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Zaman Habib</a>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <!--tvmbc end here-->
                        <!--lmbc start here-->
                        <div class="portfolio lmbc" data-cat="lmbc">
                          <div class="portfolio-wrapper">
                            <img src="img/lmbc/DanishJaved.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Danish Javed</a>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio lmbc" data-cat="lmbc">
                          <div class="portfolio-wrapper">
                            <img src="img/lmbc/JaleesSherwani.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Jalees Sherwani</a>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio lmbc" data-cat="lmbc">
                          <div class="portfolio-wrapper">
                            <img src="img/lmbc/SANJAYCHHEL.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">SANJAY CHHEL</a>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio lmbc" data-cat="lmbc">
                          <div class="portfolio-wrapper">
                            <img src="img/lmbc/ShahabAllahabadi.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Shahab Allahabadi</a>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio lmbc" data-cat="lmbc">
                          <div class="portfolio-wrapper">
                            <img src="img/lmbc/SwanandKirkire.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Swanand Kirkire</a>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <!--lmbc end here-->
                        <!--web start here-->
                        <div class="portfolio web" data-cat="web">
                          <div class="portfolio-wrapper">
                            <img src="img/web/Debashish.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Debashish</a>
                                <span class="text-category">MEMBER</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio web" data-cat="web">
                          <div class="portfolio-wrapper">
                            <img src="img/web/dinkarsharma.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Dinkar sharma</a>
                                <span class="text-category">WEB CONTENT EDITOR</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio web" data-cat="web">
                          <div class="portfolio-wrapper">
                            <img src="img/web/MitaliMahajan.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Mitali Mahajan</a>
                                <span class="text-category">MEMBER</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio web" data-cat="web">
                          <div class="portfolio-wrapper">
                            <img src="img/web/PreetiMamgain.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Preeti Mamgain</a>
                                <span class="text-category">EX-OFFICIO</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio web" data-cat="web">
                          <div class="portfolio-wrapper">
                            <img src="img/web/RobinBhatt.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Robin Bhatt</a>
                                <span class="text-category">MEMBER</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio web" data-cat="web">
                          <div class="portfolio-wrapper">
                            <img src="img/web/SanjayChouhan.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Sanjay Chouhan</a>
                                <span class="text-category">MEMBER</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio web" data-cat="web">
                          <div class="portfolio-wrapper">
                            <img src="img/web/SanjaySharma.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Sanjay Sharma</a>
                                <span class="text-category">CHARIMAN</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio web" data-cat="web">
                          <div class="portfolio-wrapper">
                            <img src="img/web/ShridharRaghvan.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Shridhar Raghvan</a>
                                <span class="text-category">CONVENOR</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio web" data-cat="web">
                          <div class="portfolio-wrapper">
                            <img src="img/web/SunilSalgia.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Sunil Salgia</a>
                                <span class="text-category">MEMBER</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="portfolio web" data-cat="web">
                          <div class="portfolio-wrapper">
                            <img src="img/web/ZamanHabib.jpg" alt="" />
                            <div class="label">
                              <div class="label-text">
                                <a class="text-title">Zaman Habib</a>
                                <span class="text-category">EX-OFFICIO</span>
                              </div>
                              <div class="label-bg"></div>
                            </div>
                          </div>
                        </div>
                        <!--web end here-->
                      </div>
                      <!--content end here-->
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>  

          <div class="tabs_item">

          </div>
          <div class="tabs_item">

          </div>
        </div>
      </div>
      <?php include_once('footer.php'); ?>
    </div>
    <!-- Modal -->
    <div id="preeti" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content project-details-popup">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <!-- <div class="modal-header">
          <img class="header-img" src="images/preeti1.jpg">
        </div> -->
        <div class="modal-body">
          <div class="row">
            <div class="col-md-4 text-center">     
              <img class="header-img" src="images/preeti1.jpg" style="width: 225px;">
            </div>
            <div class="col-md-8">
              <h4>Preeti Mamgain</h4>
              <h5>Presedent</h5>
              <p class="text-justify">The lady who has served as a writer for many popular serials starting from Jassi Jaisi Koi Nahin, Jab Love Hua, Saara Akaash, Ek Chutki Aasmaan, Phir Subah Hogi to name a few, was on a break from acting for the past one and half years. However, she has been part of Saurabh Shukla’s play ‘Two to Tango, Three to Jive’ for more than a year now.</p>
              <p>Preeti will essay the role of the female protagonist Sanjeeda Sheikh’s mother in the Zee TV show.</p>
            </div>
          </div>
        </div>
        <!--     <div class="modal-footer">  
      </div> -->
    </div>
  </div>
</div>
<script src="js/jquery-1.11.3.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.bxslider.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
<script src="js/jquery.counterup.min.js"></script>
<script src="js/custom.js"></script>
<script type="text/javascript" src="js/jquery.mixitup.min.js"></script>
<script type="text/javascript">
  $(function () {

    $('.tab ul.tabs').addClass('active').find('> li:eq(0)').addClass('current');
    
    $('.tab ul.tabs li a').click(function (g) { 
      var tab = $(this).closest('.tab'), 
      index = $(this).closest('li').index();
      
      tab.find('ul.tabs > li').removeClass('current');
      $(this).closest('li').addClass('current');
      
      tab.find('.tab_content').find('div.tabs_item').not('div.tabs_item:eq(' + index + ')').slideUp();
      tab.find('.tab_content').find('div.tabs_item:eq(' + index + ')').slideDown();
      
      g.preventDefault();
    } );

    var filterList = {
      init: function () {
       // MixItUp plugin
      // http://mixitup.io
      //$('#portfoliolist').mixItUp();


      $('#portfoliolist').mixItUp({
        selectors: {
          target: '.portfolio',
          filter: '.filter' 
        },
        load: {
          filter: '.dsc'  
        }     
      });               
    }
  };
  // Run the show!
  filterList.init();
}); 


</script>
</body>
</html>