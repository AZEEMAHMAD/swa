<!doctype html>
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="icon" type="image/x-icon" href=""/>
      <title>Film Writers Association | Feedback</title>
      <link href="css/custom.css" rel="stylesheet" type="text/css">
      <link href="css/color.css" rel="stylesheet" type="text/css">
      <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
      <link href="css/responsive.css" rel="stylesheet" type="text/css">
      <link href="css/jquery.bxslider.css" rel="stylesheet" type="text/css">
      <link href="css/content_slider_style.css" rel="stylesheet" type="text/css">
      <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
      <link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen"/>
      <link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,300' rel='stylesheet' type='text/css'>
      <link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
   </head>
   <body class="inner-page">
      <div id="wrapper">
         <?php include_once('header.php'); ?>
         <div id="cp-content-wrap">
            <div class="cp_our-story-section">
               <div class="container">
                  <div class="row">
                     <div class="col-md-9">
                        <h1 class="title bold text-center">
                          Feedback
                        </h1>
                        <div class="cp-acticle-box complain-div">
                          <div class="content_slider_wrapper" id="cp_testimonial-slider">
                             <div class="circle_slider_text_wrapper" id="sw0" style="display: none;">
                                <div class="content_slider_text_block_wrap">
                                   <p>Cras id varius nulla. Suspendisse potenti. Suspendisse quis sem ut risus auctor tri stique a a massa. Fusce scelerisque sem libero facilisis malesuada </p>
                                   <h5>Mark Aenry</h5>
                                </div>
                             </div>
                             <div class="circle_slider_text_wrapper" id="sw1" style="display: none;">
                                <div class="content_slider_text_block_wrap">
                                   <p>Cras id varius nulla. Suspendisse potenti. Suspendisse quis sem ut risus auctor tri stique a a massa. Fusce scelerisque sem libero facilisis malesuada </p>
                                   <h5>Nelson</h5>
                                </div>
                             </div>
                             <div class="circle_slider_text_wrapper" id="sw2" style="display: none;">
                                <div class="content_slider_text_block_wrap">
                                   <p>Cras id varius nulla. Suspendisse potenti. Suspendisse quis sem ut risus auctor tri stique a a massa. Fusce scelerisque sem libero facilisis malesuada </p>
                                   <h5>Alliaya Nic</h5>
                                </div>
                             </div>
                             <div class="circle_slider_text_wrapper" id="sw3" style="display: none;">
                                <div class="content_slider_text_block_wrap">
                                   <p>Cras id varius nulla. Suspendisse potenti. Suspendisse quis sem ut risus auctor tri stique a a massa. Fusce scelerisque sem libero facilisis malesuada </p>
                                   <h5>Doe Sameth</h5>
                                </div>
                             </div>
                             <div class="circle_slider_text_wrapper" id="sw4" style="display: none;">
                                <div class="content_slider_text_block_wrap">
                                   <p>Cras id varius nulla. Suspendisse potenti. Suspendisse quis sem ut risus auctor tri stique a a massa. Fusce scelerisque sem libero facilisis malesuada </p>
                                   <h5>Jonh Allia</h5>
                                </div>
                             </div>
                          </div>
                        </div>
                     </div>
                     <div class="col-md-3">
                     </div>
                  </div>
               </div>
            </div>
         </div>
          <?php include_once('footer.php'); ?>
      </div>
      <script src="js/jquery-1.11.3.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="js/jquery.bxslider.min.js"></script>
      <script src="js/owl.carousel.min.js"></script>
      <script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
      <script src="js/jquery.content_slider.min.js"></script>
      <script src="js/custom.js"></script>

      <script type="text/javascript">
      (function($){
        $(document).ready(function() {
          var image_array = new Array();
          image_array = [
            {image: 'images/fbbk1.jpg', link_url: 'images/fbbk1.jpg', link_rel: 'prettyPhoto'},
            {image: 'images/fbbk2.jpg', link_url: 'images/fbbk2.jpg', link_rel: 'prettyPhoto'},
            {image: 'images/fbbk3.jpg', link_url: 'images/fbbk3.jpg', link_rel: 'prettyPhoto'},
            {image: 'images/fbbk4.jpg', link_url: 'images/fbbk4.jpg', link_rel: 'prettyPhoto'},
            {image: 'images/fbbk5.jpg', link_url: 'images/fbbk5.jpg', link_rel: 'prettyPhoto'},
          ];
          $('#cp_testimonial-slider').content_slider({    // bind plugin to div id="slider1"
            map : image_array,        // pointer to the image map
            max_shown_items: 5,       // number of visible circles
            hv_switch: 0,         // 0 = horizontal slider, 1 = vertical
            active_item: 0,         // layer that will be shown at start, 0=first, 1=second...
            wrapper_text_max_height: 300, // height of widget, displayed in pixels
            middle_click: 1,        // when main circle is clicked: 1 = slider will go to the previous layer/circle, 2 = to the next
          under_600_max_height: 1200,   // if resolution is below 600 px, set max height of content
            border_radius:  -1,       // -1 = circle, 0 and other = radius
            automatic_height_resize: 1,
            border_on_off: 0,
            allow_shadow: 0
          });
        });
      })(jQuery);
     </script>
   </body>
</html>