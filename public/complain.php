<!doctype html>
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="icon" type="image/x-icon" href=""/>
      <title>Film Writers Association | Complain Form</title>
      <link href="css/custom.css" rel="stylesheet" type="text/css">
      <link href="css/color.css" rel="stylesheet" type="text/css">
      <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
      <link href="css/responsive.css" rel="stylesheet" type="text/css">
      <link href="css/jquery.bxslider.css" rel="stylesheet" type="text/css">
      <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
      <link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen"/>
      <link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,300' rel='stylesheet' type='text/css'>
      <link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
   </head>
   <body class="inner-page">
      <div id="wrapper">
         <?php include_once('header.php'); ?>
         <div id="cp-content-wrap">
            <div class="cp_our-story-section">
               <div class="container">
                  <div class="row">
                     <div class="col-md-12">
                        <h1 class="title bold text-center">
                          Complaint Form
                        </h1>
                        <div class="cp-acticle-box complain-div">
                          <p>Dear member,<br><br> 
                          Download the Complaint Form and the Undertaking, fill both of them and send to us along with copies of relevant documents by post or in person, at <a href ="javascript:void(0)">our office address</a></p>
                          <p>Please note that the need for making Affidavit on INR 100/- stamp-paper and getting it notarized has been CANCELLED with immediate effect. You just need to give an Undertaking on Oath as mentioned in the Affidavit on plain paper. (This has been done to save the unnecessary hassles our members were going through to acquire the Stamp papers).</p>
                          <p>If you are submitting the complaint in person please also submit hardcopies (photocopies of your work) if the original “disputed” work was registered in the office by manual registration process. If the original work was registered online with digital signature (only those work registered online post 20th June bear digital signature) then you will also need to email us the softcopy of the said work along with other supporting documents and also send us the hardcopiesofall documents by post/in person. (The hardcopies are required for references & consultations).</p>
                          <p>If you are sending these documents by post or email, we would recommend you drop an email to notify the office about your submission.</p>
                          <p>Whichever way you submit your complaint please note you will receive an acknowledgement from the office. Kindly insist on it in case you don’t get.</p>
                        </div>
                        <div class="cp-acticle-box">
                          <div class="download-box"><i class="fa fa-download" aria-hidden="true"></i>&nbsp;Download DSC Complaint Form</div>
                          <div class="download-box"><i class="fa fa-download" aria-hidden="true"></i>&nbsp;Download DSC UNDERTAKING</div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
          <?php include_once('footer.php'); ?>
      </div>
      <script src="js/jquery-1.11.3.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="js/jquery.bxslider.min.js"></script>
      <script src="js/owl.carousel.min.js"></script>
      <script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
      <script src="js/jquery.counterup.min.js"></script>
      <script src="js/custom.js"></script>
   </body>
</html>