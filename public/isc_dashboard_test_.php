<?php session_start();
//echo "<pre>";print_r($_SESSION);die;
if(isset($_SESSION['isc_login']) &&  $_SESSION['isc_login'] == 'true')
{

  include_once('includes/config.php');
  ?>

  <!doctype html>
  <html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/x-icon" href=""/>
    <title>Film Writers Association | My Creations</title>
    <link href="css/custom.css" rel="stylesheet" type="text/css">
    <link href="css/color.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="css/responsive.css" rel="stylesheet" type="text/css">
    <link href="css/jquery.bxslider.css" rel="stylesheet" type="text/css">
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,300' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style media="screen">
    input,textarea{
        width: 85%;
        padding: 3px;
        border: 1px solid #ccc;
        resize: vertical;
    }
    .invalid_msg {
        background: #d1cbc8;
        padding: 15px;
        color: red;
        font-size: 20px;
    }
    .success_msg{
      background: #d1cbc8;
        padding: 15px;
        color: green;
        font-size: 20px;
    }
    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        margin: 0;
    }
    .error {
        border: 1px solid #F00!important;
    }
    .btn{
        font-family: 'Domine', serif;
        color: #fff!important;
        font-size: 14px;
        line-height: 37px;
        padding: 0px 35px;
        background: #000;
        border: 0px;
        border-radius: 0px;
    }
    .amt-lbl,.amt-lbl:hover{
        border: none;
        background: none;
        font-weight: 700;
    }
    .no-bg,.no-bg:hover {
        background: none!important;
    }
    </style>
  </head>
  <body class="inner-page">
    <div id="wrapper" class="inside-menu">
      <?php include_once('isc_header.php'); ?>
      <div id="cp-content-wrap" class="page404 cp-login-page">
        <div class="container">
          <div class="row">
              <?php if(isset($_SESSION['isc_error_msg']))
                   { ?>
                   <div class="col-md-12 invalid_msg">
                      <?php
                        echo $_SESSION['isc_error_msg'];
                        unset($_SESSION['isc_error_msg']);
                      ?>
                   </div>
                   <?php } ?>

                   <?php if(isset($_SESSION['isc_success_msg']))
                   { ?>
                   <div class="col-md-12 success_msg">
                      <?php
                        echo $_SESSION['isc_success_msg'];
                        unset($_SESSION['isc_success_msg']);
                      ?>
                   </div>
                   <?php } ?>

            <div class="col-md-12 main-reg-div">
              <div class="cp-reg-box sub-reg-div">

                <?php
                $isc_type = $_SESSION['isc_type'];
                if($isc_type == 1)
                {
                    $sql = "select fu.*,mem.* from `fwa_users` fu
                    left join `fwa_members` mem on mem.reg_no = fu.reg_no
                    where fu.reg_no = '".$_SESSION['username']."'";
                    $result =  mysqli_query($db,$sql);
                    $row = mysqli_fetch_assoc($result);
                }
                else {
                    $row = null;
                }

                switch ($isc_type) {
                    case '1':
                        $isc_type_name = 'SWA Member';
                        $isc_amount = 2100;
                        break;
                    case '2':
                    $isc_type_name = 'Non-member/Guest';
                    $isc_amount = 3186;
                        break;
                    case '3':
                    $isc_type_name = 'Non-member/Guest - Student';
                    $isc_amount = 2478;
                        break;

                    default:
                    $isc_type_name = 'Non-member/Guest';
                    $isc_amount = 3186;
                        break;
                }

                ?>
                <h4>Your Details</h4>
                <form action="ccavRequestHandler_isc_test.php" method="post" id="form_checkout" enctype="multipart/form-data">
                    <input type="hidden" name="merchant_id" value="2923">
                    <input type="hidden" name="currency" value="INR">
                    <input type="hidden" name="redirect_url" value="http://swaindia.org/ccavResponseHandler_isc_test.php">
                    <input type="hidden" name="cancel_url" value="http://swaindia.org/ccavResponseHandler_isc_test.php">
                    <input type="hidden" name="language" value="EN">
                    <input type="hidden" name="promo_code" value="">
                    <input type="hidden" name="integration_type" value="iframe_normal">
                    <input type="hidden" name="merchant_param3" id="merchant_param3" value="<?php echo $isc_type; ?>">
                    <input type="hidden" name="billing_country" value="India">
                <table class='table table-hover'>
                    <?php if($isc_type == 1 ) { ?>
                    <tr>
                      <th>Membership No.</th>
                      <td><input type="text" name = "merchant_param2" value="<?php echo isset($row['reg_no']) ? $row['reg_no'] : '' ?>" readonly ></td>
                    </tr>
                <?php } ?>
                    <tr>
                      <th>Name</th>
                      <td><input type="text" name = "billing_name" id="billing_name" value="<?php echo isset($row['name']) ? str_replace( array( ';', '<', '>' , '(' , ')' , '{' , '}' ) , ' ', $row['name'] ) : null ?>" <?php echo isset($row['name']) ? 'readonly' : '' ?>  ></td>
                    </tr>
                    <tr>
                      <th>Email</th>
                      <td><input type="email" name = "billing_email" id = "billing_email" value="<?php echo isset($row['email']) ? str_replace( array( ';', '<', '>' , '(' , ')' , '{' , '}' ) , ' ', $row['email'] ) : null ?>"  ></td>
                    </tr>
                    <tr>
                      <th>Mobile no</th>
                      <td><input type="number" name = "billing_tel" id = "billing_tel" value="<?php echo isset($row['mobileno']) ? str_replace( array( ';', '<', '>' , '(' , ')' , '{' , '}' ) , ' ', $row['mobileno'] ) : null ?>"  ></td>
                    </tr>
                    <tr>
                      <th>Address</th>
                      <td><textarea type="text" name = "billing_address" id="billing_address" ><?php echo isset($row['add1']) ? str_replace( array( ';', '<', '>' , '(' , ')' , '{' , '}' ) , ' ', $row['add1'].' '.$row['add2'] ) : null ?></textarea></td>
                    </tr>
                    <tr>
                      <th>City</th>
                      <td><input type="text" name = "billing_city" id = "billing_city" value="<?php echo isset($row['city']) ? $row['city'] : null ?>"></td>
                    </tr>
                    <tr>
                      <th>Pincode</th>
                      <td><input type="number" name = "billing_zip" id="billing_zip"  value="<?php echo isset($row['pin']) ? $row['pin'] : null ?>"></td>
                    </tr>
                    <tr>
                      <th>State</th>
                      <td><input type="text" name = "billing_state" id="billing_state" value="<?php echo isset($row['state']) ? $row['state'] : null ?>"></td>
                    </tr>
                    <?php if($isc_type == 3) { ?>
                    <tr>
                      <th>Institute ID Card **</th>
                      <td><input type="file" name = "file_proof" id="merchant_param4" /></td>
                    </tr>
                    <?php } ?>
                    <tr class="no-bg">
                      <th>5ISC Registration Type</th>
                      <td><?php echo $isc_type_name; ?></td>
                    </tr>
                    <tr class="no-bg">
                      <th>5ISC Registration Amount</th>
                      <td><input class="amt-lbl" value="<?php echo $isc_amount; ?>" name="amount" readonly></td>
                    </tr>
                    <tr class="no-bg">
                        <td colspan="2" class="text-center">
                            <button class="btn" type="submit">Proceed</button>
                        </td>
                    </tr>
                    <?php if($isc_type == 3) { ?>
                    <tr class="no-bg">
                        <td colspan="2">
                            <br><br>
                            <h3 class="text-center">Disclaimer</h3>
                        <strong>
                            I understand and declare that if I'm not able to produce a valid Institute ID Card** to SWA staff, I will pay the difference of INR 600/- (+ 18% GST) to receive my Delegate Card.</strong>
                        </td>
                    </tr>
                    <?php } ?>
              </table>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php include_once('footer.php'); ?>
  </div>
  <script src="js/jquery-1.11.3.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.bxslider.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/custom.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/validate-js/2.0.1/validate.js"></script>
  <script type="text/javascript">
    $('#form_checkout').on('submit',function(eve){
        // eve.preventDefault();

        var flag = 0;
        var EmailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
        var phonePattern = /^[0-9]*$/;

        var billing_name = $('#billing_name').val();
        var billing_email = $('#billing_email').val();
        var billing_tel = $('#billing_tel').val();
        var billing_address = $('#billing_address').val();
        var billing_city = $('#billing_city').val();
        var billing_zip = $('#billing_zip').val();
        var billing_state = $('#billing_state').val();
        var isc_type = $('#merchant_param3').val();
        var id_proof = $('#merchant_param4').val();

        console.log(id_proof);

        if(billing_name=='' ||  billing_name==null)
        {
            $('#billing_name').addClass('error');
            flag++;
        }
        else
        {
            $('#billing_name').removeClass('error');
        }


       if(!EmailPattern.test(billing_email) || billing_email=='' ||  billing_email==null)
       {
           $('#billing_email').addClass('error');
           flag++;
       }
       else
       {
           $('#billing_email').removeClass('error');
       }

       if(!phonePattern.test(billing_tel) || billing_tel=='' ||  billing_tel==null)
       {
           $('#billing_tel').addClass('error');
           flag++;
       }
       else
       {
           $('#billing_tel').removeClass('error');
       }

       if(billing_address=='' ||  billing_address==null)
       {
           $('#billing_address').addClass('error');
           flag++;
       }
       else
       {
           $('#billing_address').removeClass('error');
       }

       if(billing_city=='' ||  billing_city==null)
       {
           $('#billing_city').addClass('error');
           flag++;
       }
       else
       {
           $('#billing_city').removeClass('error');
       }

       if(billing_zip=='' ||  billing_zip==null)
       {
           $('#billing_zip').addClass('error');
           flag++;
       }
       else
       {
           $('#billing_zip').removeClass('error');
       }

       if(billing_state=='' ||  billing_state==null)
       {
           $('#billing_state').addClass('error');
           flag++;
       }
       else
       {
           $('#billing_state').removeClass('error');
       }

       if(isc_type == 3 ) {

           if(id_proof=='' ||  id_proof==null)
           {
               $('#merchant_param4').addClass('error');
               flag++;
           }
           else
           {
               $('#merchant_param4').removeClass('error');
           }
       }

       if(flag == 0)
       {
           return true;
       }
       else {
           return false;
       }

    });
  </script>
</body>
</html>
<?php }
else{
  header('Location: isc_login.php');
} ?>
