<?php session_start();
  if(isset($_REQUEST['q']))
  {
    $q = base64_decode(base64_decode($_REQUEST['q']));
  }
  else{
    header('Location: index.php');
  }
  include_once('includes/config_cms.php');

 $sql = "select art.id, art.art_main_heading, art.art_sub_heading, art.art_main_banner, art.art_content , art.art_author_id, art.view,art.art_date , cat.cat_name, ath.author_name, ath.author_img_path, ath.author_text
from `article` art
left join `category` cat on cat.id = art.art_category_id
left join `authors` ath on ath.id = art.art_author_id
where art.id = ".$q;

$result = mysqli_query($db,$sql);
$row = mysqli_fetch_assoc($result);

$sql_update_view = "UPDATE `article` SET `view` = `view` + 1 WHERE `id` = ".$q;
$result_view = mysqli_query($db,$sql_update_view);

?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/x-icon" href="images/favicon.png" />
    <title>Screenwriters Association | Article</title>

    <meta property="og:title" content="<?php echo $row['art_main_heading']; ?>"/>
    <meta property="og:description" content="<?php echo $row['art_sub_heading']; ?>" />
    <meta property="og:image" content="<?php echo $row['art_main_banner']; ?>" />
    <meta property="og:image:width" content="600" />
    <meta property="og:image:height" content="315" />
    <link href="css/custom.css" rel="stylesheet" type="text/css">
    <link href="css/color.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="css/responsive.css" rel="stylesheet" type="text/css">
    <link href="css/jquery.bxslider.css" rel="stylesheet" type="text/css">
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,300' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
  p,span{font-family: 'Lato', sans-serif!important;line-height: 24px!important;font-size: 14px!important;/*color: #666666!important;*/}
  img{max-width: 100%;height:auto;}
    </style>

</head>
<body class="inner-page">
    <div id="wrapper">
        <?php include_once('header.php'); ?>
        <div id="cp-content-wrap" class="cp-content-wrap">
            <div class="container">
                <div class="row">

                    <div class="col-md-10 col-md-offset-1">
                        <div class="cp-posts-style-1">
                        	<h5 class="art-cat"><a class="underline" data-toggle="tooltip" title="Click to see more from this category" href="javascript:void(0)"><?php echo $row['cat_name']; ?></a></h5>
                        </div>
                        <div class="cp-posts-style-1">

                            <ul class="cp-posts-list cp-post-details">

                                <li class="cp-post">
                                    <div class="cp-thumb"> <img src="<?php echo $row['art_main_banner']; ?>" alt=""> </div>
                                    <div class="cp-post-base">
                                        <div class="cp-post-content">
                                            <ul class="sub-line">
                                                <li data-toggle="tooltip" title="Author"><i class="fa fa-user"></i>&nbsp;<?php echo $row['author_name']; ?></li>
                                                <li data-toggle="tooltip" title="Publish Date"><i class="fa fa-clock-o"></i>&nbsp;<?php  echo date("d F Y", strtotime($row['art_date'])); ?></li>
                                                <li data-toggle="tooltip" title="Articles views"><i class="fa fa-eye"></i>&nbsp;<?php echo $row['view']; ?></li>
                                            </ul>

                                        <h4><?php echo $row['art_main_heading']; ?></h4>
                                        <h5><?php echo $row['art_sub_heading']; ?></h5>
                                            <div id="pagination-1" class="pagination__list art-content">
                                                <?php  echo $row['art_content']; ?>
                                            </div>
	                                        <div class="cp-post-share mar">
	                                        	<div class="row auth-div" id="auth">
		                                            <div class="col-md-3">
		                                            	<img class="auth-img" src="<?php echo $row['author_img_path']; ?>">
		                                             	<!-- <span class="auth-name"><?php //echo $row['author_name']; ?></span>    -->
		                                            </div>
		                                            <div class="col-md-9" >
		                                            	<p class="auth-bio">
		                                            		<?php echo $row['author_text']; ?>
		                                            	</p>
		                                            </div>
	                                            </div>
	                                            <ul>
	                                                <li> <a href="#"><i class="fa fa-facebook"></i> Share Facebook</a> </li>
	                                                <li> <a href="#"><i class="fa fa-twitter"></i> Share Twitter</a> </li>
	                                                <li> <a href="#"><i class="fa fa-google-plus"></i> Share Google</a> </li>
	                                            </ul>
	                                        </div>
                                        </div>
                                        <div id="disqus_thread"></div>
                                    </div>
                                </li>

                            </ul>
                        </div>
                    </div>

                    <!--<div class="col-md-3">
                        <div class="sidebar">
                             <div class="widget featured-posts">
                                <h3>Related Articles</h3>
                                <div class="widget-content">
                                    <ul class="fpost-list">
                                        <li> <img src="images/side-fp1.jpg" alt="neo">
                                            <div class="cp-post-content">
                                                <h5><a href="#">The Studio</a></h5>
                                                <ul class="cp-post-meta">
                                                    <li><a href="#">Feb 23</a></li>
                                                    <li><a href="#">Travel</a></li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li> <img src="images/side-fp2.jpg" alt="neo">
                                            <div class="cp-post-content">
                                                <h5><a href="#">The Studio</a></h5>
                                                <ul class="cp-post-meta">
                                                    <li><a href="#">Feb 23</a></li>
                                                    <li><a href="#">Travel</a></li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li> <img src="images/side-fp3.jpg" alt="neo">
                                            <div class="cp-post-content">
                                                <h5><a href="#">The Studio</a></h5>
                                                <ul class="cp-post-meta">
                                                    <li><a href="#">Feb 23</a></li>
                                                    <li><a href="#">Travel</a></li>
                                                </ul>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="widget featured-posts">
                                <h3>Trending Articles</h3>
                                <div class="widget-content">
                                    <ul class="fpost-list">
                                        <li> <img src="images/side-fp1.jpg" alt="neo">
                                            <div class="cp-post-content">
                                                <h5><a href="#">The Studio</a></h5>
                                                <ul class="cp-post-meta">
                                                    <li><a href="#">Feb 23</a></li>
                                                    <li><a href="#">Travel</a></li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li> <img src="images/side-fp2.jpg" alt="neo">
                                            <div class="cp-post-content">
                                                <h5><a href="#">The Studio</a></h5>
                                                <ul class="cp-post-meta">
                                                    <li><a href="#">Feb 23</a></li>
                                                    <li><a href="#">Travel</a></li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li> <img src="images/side-fp3.jpg" alt="neo">
                                            <div class="cp-post-content">
                                                <h5><a href="#">The Studio</a></h5>
                                                <ul class="cp-post-meta">
                                                    <li><a href="#">Feb 23</a></li>
                                                    <li><a href="#">Travel</a></li>
                                                </ul>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="widget archives-widget">
                                <h3>Archives</h3>
                                <div class="widget-content">
                                    <ul>
                                        <li><a href="javascript:void(0)"> March 2016</a></li>
                                        <li><a href="javascript:void(0)"> September 2016</a></li>
                                        <li><a href="javascript:void(0)"> July 2016</a></li>
                                        <li><a href="javascript:void(0)"> March 2015</a></li>
                                        <li><a href="javascript:void(0)"> September 2015</a></li>
                                        <li><a href="javascript:void(0)"> July 2015</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="widget tags-widget">
                                <h3>Category</h3>
                                <div class="widget-content">
                                    <ul>
                                        <li>
                                        <?php foreach ($tags_arr as $tags) { ?>
                                            <a href="" data-original-title="" title=""><?php echo $tags; ?></a>
                                        <?php } ?>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div> -->

                </div>
            </div>
        </div>

       <?php include_once('footer.php'); ?>
    </div>
    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.bxslider.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/custom.js"></script>
    <script type="text/javascript" src="js/jQuery.paginate.js"></script>

    <script>
    /**
    *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
    *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
    /*
    var disqus_config = function () {
    this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
    this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
    };
    */
    (function() { // DON'T EDIT BELOW THIS LINE
    var d = document, s = d.createElement('script');
    s.src = 'https://swaindia-1.disqus.com/embed.js';
    s.setAttribute('data-timestamp', +new Date());
    (d.head || d.body).appendChild(s);
    })();
    </script>
    <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>


     <script>
     // $('#pagination-1').paginate({
     //        items_per_page: 1
     //    });
     // $('.pagination').click(function() {
     //        $('html,body').animate({
     //        scrollTop: $('.cp-post-base').offset().top - 150},
     //        'slow');
     // });
    </script>
</body>

</html>
