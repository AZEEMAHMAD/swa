<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/x-icon" href=""/>
  <title>SWA Membership</title>
  <link href="css/custom.css" rel="stylesheet" type="text/css">
  <link href="css/color.css" rel="stylesheet" type="text/css">
  <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
  <link href="css/responsive.css" rel="stylesheet" type="text/css">
  <link href="css/jquery.bxslider.css" rel="stylesheet" type="text/css">
  <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen"/>
  <link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,300' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>

      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
    </head>
    <body class="inner-page">
      <div id="wrapper">
       <?php include_once('header.php'); ?>
       <div id="cp-content-wrap">
        <div class="banner_inner">
          <img src="images/banner/membership.jpg">
        </div>
        <div class="cp_our-story-section">
         <div class="container">
          <div class="row">
           <div class="col-md-12">
            <div class="cp-acticle-box abt-div">
              <h1 class="title bold text-center">
                SWA Membership
              </h1>
              <p class="text-justify">Membership of the Screenwriters Association is meant for screenwriters (including writers and lyricists writing for films, TV, audio formats, digital media or any new media), who reside within the boundary of the Union of India and are above 18 years of age*.</p>
              <h4 class="sub-hd">BECOMING SWA MEMBER</h4>
              <p class="text-justify">One can become a SWA member by submitting the duly filled SWA Membership Form along with the required documents and applicable fees at SWA Office, OR by sending the same through post. <span class="italic">SWA Membership is approved only upon the verification of all the mentioned documents.</span><span class="italic">Time Taken: 10-12 working days</span></p>
              <div class="cp-acticle-box" id="membership-form">
                  <a target="_blank" href="pdf/Membership_form_swa.pdf" class="dwn-btn" download=""><div class="download-box"><i class="fa fa-download" aria-hidden="true"></i>&nbsp;Download SWA Membership Form</div></a>
                </div>

              <h4 class="sub-hd">SWA MEMBERSHIP TYPES</h4>
              <ul class="mem">
               <li class="mem-li">
                 <h5>REGULAR MEMBER</h5>
                 <p class="text-justify">The following can become a Regular Member with SWA:</p>
                <ul class="sub-mem">
                  <li class="sub-mem-li"><p class="text-justify">Any person whose work has been certified by CBFC (Central Board of Film Certification) as a feature film (or on digital media), and who has the writing credit (Story/Screenplay/Dialogue) for the same.</p></li>
                  <li class="sub-mem-li"><p class="text-justify">Any person whose TV serial/show has been broadcasted, and who has writing credit (Created/Story/Screenplay/Dialogue) for the same. </p></li>
                  <li class="sub-mem-li"><p class="text-justify">A lyricist whose song has appeared in a film, TV serial a music album.</p></li>
                </ul>
              </li>
              <li class="mem-li">
               <h5>LIFE MEMBER</h5>
               <p class="text-justify">Any Regular member or a person eligible to become a Regular member can become a Life Member and would not require paying the monthly subscription thereafter.</p>
             </li>
             <li class="mem-li">
               <h5>ASSOCIATE MEMBER</h5>
               <p class="text-justify">The following can become an Associate Member with SWA:</p>
               <ul class="sub-mem">
                  <li class="sub-mem-li"><p class="text-justify">Any writer who has been professionally contracted by a producer to write Story, Screenplay, Dialogue or Lyrics for a feature film, TV serial or digital media; with the first cheque having been paid to him/her and deposited.</p></li>
                  <li class="sub-mem-li"><p class="text-justify">Diploma or certificate holders from any recognised institute having studied a screenwriting course with the duration of a minimum of one year.</p></li>
                </ul>
               <p class="text-justify">The duration of this membership is three years. If in those three years, the said member doesn't upgrade to Regular/Life category, his/her Associate membership will lapse. In which case, s/he will have to apply again afresh for membership.<p>
            </li>
            <li class="mem-li">
               <h5>FELLOW MEMBER</h5>
               <p class="text-justify">Any person who has a passion for screenwriting is eligible for Fellow membership.</p>
               <ul class="sub-mem">
                  <li class="sub-mem-li"><p class="text-justify">A Fellow member will be allowed to register his/her work at SWA. However, s/he will not be able to attend SWA AGMs or vote or stand for SWA elections. S/he can attend SWA events like seminars, conferences, workshops, etc., that are open to other members.</p></li>
                </ul>
               <p class="text-justify">The duration of Fellow membership will be three years, within which period s/he will either get himself/herself upgraded to Associate or Regular/Life category, or else the Fellow membership will expire after three years. If s/he wishes to become a Fellow member again, s/he will have to reapply afresh for it by fulfilling all the application protocols.<p>
            </li>
            </ul>
            <br>
            <h4 class="sub-hd">NRI/FOREIGN NATIONALS</h4>
            <p class="text-justify">Any NRI/Foreign nationals including Person of Indian Origin and Overseas citizen of India, who are writers and are working in India, can enroll only a Fellow. However, they can’t upgrade to any other membership category.</p><br>
            <h4 class="sub-hd">SWA MEMBERSHIP UPGRADATION</h4>
            <p class="text-justify">As and when a Fellow member becomes eligible for Associate/Regular Membership, or if an Associate Member becomes eligible for Regular membership, then it is MANDATORY for him/her to upgrade to that category. If the person does not apply for that appropriate category, her/his membership is liable to be cancelled.</p>
            <br>
            <h4 class="sub-hd">REQUIRED DOCUMENTS</h4>
            <ul class="mem">
              <li class="mem-li">FELLOW MEMBERSHIP
                <ol class="req">
                   <li class="req-li"><p class="text-justify"><strong>Duly filled SWA Membership Form</strong></p></li>
                   <li class="req-li"><p class="text-justify"><strong>Address Proof</strong><br>(Xerox Copy of any one of these: Electricity Bill/Ration Card/Aadhar Card/Driving License/Passport/Leave & License Agreement)<br><span class="italic">Self-Attested OR Verified against original documents </span></p>
                   </li>
                   <li class="req-li"><p class="text-justify"><strong>ID Proof</strong><br>(Xerox copy of any one of these: Pan Card/ Voter ID/Driving License /Passport /Aadhar Card)<br><span class="italic">Self-Attested OR Verified against original documents </span></p></li>
                   <li class="req-li"><p class="text-justify"><strong>Two (2) Passport Size Photos</strong></p></li>

                </ol>
              </li>
              <li class="mem-li">ASSOCIATE MEMBERSHIP
                <p class="text-justify">In addition to documents <strong>1,2, 3 & 4</strong>above:</p>
                <ol class="req" start="5">
                   <li class="req-li"><p class="text-justify"><strong>Copy of the Signed Contract, and the proof of payment of signing amount credited in applicant’s account </strong>OR</p></li>
                   <li class="req-li"><p class="text-justify"><strong>Diploma/Certificate of a Program/Course in Screenwriting (of minimum 1 year duration) from a recognized institute</strong></p>
                   </li>
                </ol>
              </li>
              <li class="mem-li">LIFE/REGULAR MEMBERSHIP
                <p class="text-justify">In addition to documents <strong>1,2, 3 & 4</strong>above:</p>
                <ol class="req" start="5">
                   <li class="req-li"><p class="text-justify"><strong>Proof of Writing Credit (Story/Screenplay/Dialogue)</strong>OR</p></li>
                   <li class="req-li"><p class="text-justify"><strong>Proof of Lyrics Credit (in a film, TV serial a music album) </strong><br>(DVD/Clip on Pen Drive/ Hyperlink of the Film/TV Serial/ Web Series, or Credit mentioned on IMDB/Wikipedia page of the Film/TV Serial/Web Series are accepted as proof of writing and lyrics credits.)</p>
                   </li>
                </ol>
              </li>
            </ul>
            <h4 class="sub-hd">SWA MEMBERSHIP FEES & PENALTY** </h4>
              <h5 class="sub-hd">ADMISSION & RENWAL</h5>
              <table class="table table-hover">
                <thead>
                  <tr>
                    <th>Membership Type</th>
                    <th>Admission Fees</th>
                    <th>Validity</th>
                    <th>Renwal Fees</th>
                    <!-- <th>Validity</th> -->
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th scope="row">Life Member</th>
                    <td>INR 21,000/-</td>
                    <td>Total & Lifetime</td>
                    <td>N/A</td>
                    <!-- <td>N/A</td> -->
                  </tr>
                  <tr>
                    <th scope="row">Regular Member</th>
                    <td>INR 4905/-</td>
                    <td>1 Year</td>
                    <td>INR 120/-</td>
                    <!-- <td>1 Year</td> -->
                  </tr>
                  <tr>
                    <th scope="row">Associate Member</th>
                    <td>INR 4011/-</td>
                    <td>3 Years</td>
                    <td>INR 72/-</td>
                    <!-- <td>1 Year</td> -->
                  </tr>
                  <tr>
                    <th scope="row">Fellow Member</th>
                    <td>INR 2811/-</td>
                    <td>3 Years</td>
                    <td>INR 72/-</td>
                    <!-- <td>1 Year</td> -->
                  </tr>
                  <tr>
                    <th scope="row">Fellow NRI/Foreign Nationals</th>
                    <td>INR 10,000/-</td>
                    <td>1 Year</td>
                    <td>INR 72/-</td>
                    <!-- <td>N/A</td> -->
                  </tr>
                  <tr>
                    <td colspan="5">
                       Optional Charges
                    </td>
                  </tr>
                  <tr>
                    <td colspan="1">
                      SWA Membership Form (hard copy)
                    </td>
                    <td colspan="4">
                       INR 10/-
                    </td>
                  </tr>
                   <tr>
                    <td colspan="1">
                      SWA Membership Card (Post-Renewal)
                    </td>
                    <td colspan="4">
                       INR 50/-
                    </td>
                  </tr>
                  <tr>
                    <td colspan="1">
                      Postal Charges (sending SWA Membership Card in Mumbai)
                    </td>
                    <td colspan="4">
                       NIL
                    </td>
                  </tr>
                  <tr>
                    <td colspan="1">
                      Postal Charges (sending SWA Membership Card outside Mumbai)
                    </td>
                    <td colspan="4">
                       INR 50/-
                    </td>
                  </tr>
                  <tr>
                    <td colspan="1">
                      Postal Charges (sending SWA Membership Card outside INDIA)
                    </td>
                    <td colspan="4">
                       INR 300/-
                    </td>
                  </tr>
                </tbody>
              </table>
              <h5 class="sub-hd">UPGRADATION</h5>
              <table class="table table-hover">
                <thead>
                  <tr>
                    <th>Upgradation Type</th>
                    <th>Fees</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th scope="row">Fellow Member TO Associate Member</th>
                    <td>INR 1,000/-</td>
                  </tr>
                  <tr>
                    <th scope="row">Fellow/Associate Member TO Regular Member</th>
                    <td>INR 2,000/-</td>
                  </tr>
                  <tr>
                    <th scope="row">Fellow/Associate/Regular Member TO Life Member</th>
                    <td>INR 21,000/-</td>
                  </tr>
                </tbody>
              </table>
              <h5 class="sub-hd">PENALTY</h5>
              <p class="text-justify">As soon as any SWA member’s membership validity (as printed on the SWA Membership Card) expires, he or she must renew it. Failing to do so shall result in the following penalty</p>
              <table class="table table-hover">
                <thead>
                  <tr>
                    <th>Months post-validity</th>
                    <th>Penalty</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th scope="row">0 - 6</th>
                    <th>NIL</th>
                  </tr>
                  <tr>
                    <th scope="row">6 to 18</th>
                    <th>INR 500/-</th>
                  </tr>
                  <tr>
                    <th scope="row">18 to 24</th>
                    <th>INR 1000/-</th>
                  </tr>
                  <tr>
                    <th scope="row">After 24</th>
                    <th>(Apply for Re-Admission)</th>
                  </tr>
                </tbody>
              </table>
              <!-- <div class="cp-acticle-box ">
                  <p>SWA Membership Form</p>
                   <a target="_blank" href="pdf/swa-constitution.pdf" class="dwn-btn" download=""><div class="download-box"><i class="fa fa-download" aria-hidden="true"></i>&nbsp;Download formats</div></a>
                </div> -->
             <h4 class="sub-hd">PAYMENTS</h4>
             <ul class="mem">
                <li class="mem-li">Cash (only at office)</li>
                <li class="mem-li">Cheque/Demand Draft/Money Order (In the name of 'Screenwriters Association')</li>
              </ul><br>
              <!-- <h6 class="italic">Have a question? Read SWA Membership FAQs <a href="faqs.php">here</a></h6><br> -->
              <span class="italic">*In exceptional cases, subject to the approval of the Executive Committee, membership of the Association may also be given to a person who has not attained the age of 18 years.<br>
              **The Executive Committee reserves the right to revise the fees.</span>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php include_once('footer.php'); ?>
</div>
<script src="js/jquery-1.11.3.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.bxslider.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
<script src="js/jquery.counterup.min.js"></script>
<script src="js/custom.js"></script>
<!-- <script type="text/javascript" src="js/jquery.mixitup.min.js"></script> -->
<!-- <script type="text/javascript">
  $(function () {

    var filterList = {

      init: function () {

        // MixItUp plugin
        // http://mixitup.io
        $('#portfoliolist').mixItUp({
          selectors: {
            target: '.portfolio',
            filter: '.filter'
          },
          load: {
            filter: '.exe'
          }
        });

      }

    };

    // Run the show!
    filterList.init();


  });

</script> -->
</body>
</html>
