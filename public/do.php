<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/x-icon" href=""/>
  <title>SWA Membership</title>
  <link href="css/custom.css" rel="stylesheet" type="text/css">
  <link href="css/color.css" rel="stylesheet" type="text/css">
  <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
  <link href="css/responsive.css" rel="stylesheet" type="text/css">
  <link href="css/jquery.bxslider.css" rel="stylesheet" type="text/css">
  <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen"/>
  <link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,300' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>

      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
    </head>
    <body class="inner-page">
      <div id="wrapper">
       <?php include_once('header.php'); ?>
       <div id="cp-content-wrap">
        <div class="banner_inner">
          <img src="images/bg.jpg">
        </div>
        <div class="cp_our-story-section">
         <div class="container">
          <div class="row">
           <div class="col-md-12">
            <div class="cp-acticle-box abt-div">
              <h1 class="title bold text-center">
                DOs &amp; DON’Ts FOR WRITERS
              </h1>
              <h4 class="sub-hd">DO's</h4>
              <ul class="mem">
               <li class="mem-li">
                 <h5>Deserve Before You Desire</h5>
                <ul class="sub-mem">
                  <li class="sub-mem-li"><p class="text-justify">Write well. Draft a 'production & reader friendly' screenplay which ensures a smooth read and is ready to go into production stage.</p></li>
                  <li class="sub-mem-li"><p class="text-justify">Complete at least a couple of drafts of a screenplay and have at least 3-4 screenplays in different categories in your kitty before you start knocking on producers' doors.</p></li>
                  <li class="sub-mem-li"><p class="text-justify">Practice narration often so you are prepared to pitch. Narrate to your friends, family and confidants and re-work on their suggestions. </p></li>
                </ul> 
              </li> 
             <li class="mem-li">
                 <h5>Be A Professional</h5>
                <ul class="sub-mem">
                  <li class="sub-mem-li"><p class="text-justify">Become a SWA member as soon as you have something to get registered.</p></li>
                  <li class="sub-mem-li"><p class="text-justify">Register ALL the drafts of synopsis/story/script/screenplay/dialogue, at every stage of rewrite, BEFORE sharing it professionally.</p></li>
                  <li class="sub-mem-li"><p class="text-justify">Keep the registered copies of your script/work with utmost safety. </p></li>
                  <li class="sub-mem-li"><p class="text-justify">Prefer using the Online Registration facility at SWA website, as much as possible. The online registered soft copies can easily be shared via emails.</p></li>
                  <li class="sub-mem-li"><p class="text-justify">Email soft-copies of your registered script/work to Producers/Production Houses, unless they insist on a hard copy. </p></li>
                  <li class="sub-mem-li"><p class="text-justify">If submitting a hard copy is necessary, share a Xerox copy of your registered script/work.</p></li>
                  <li class="sub-mem-li"><p class="text-justify">Learn to negotiate with Producers/Production Houses for our price and rights.</p></li>
                  <li class="sub-mem-li"><p class="text-justify">Try writing for Television. TV is truly a medium of writers, as it not only offers financial security, but also an opportunity to whet your craft by writing under pressure.</p></li>
                  <li class="sub-mem-li"><p class="text-justify">Initially, having a side job to support yourself till you make it big actually helps. Consider having one while looking for writing assignments and perfecting your speculative screenplays.</p></li>
                </ul> 
              </li>
              <li class="mem-li">
                 <h5>Narrate it Right</h5>
                <ul class="sub-mem">
                  <li class="sub-mem-li"><p class="text-justify">Insist on getting a ‘Non-Discloser Agreement (NDA)’ OR any other kind of acknowledgement, from Producers/Production Houses/Channels for narrations. If possible, also insist on recording the meeting.</p></li>
                  <li class="sub-mem-li"><p class="text-justify">Before, during or right after the narration, email soft-copies to Producer/Production Houses/Channels with a note that you narrated that particular script/work to them. Attach the soft copy of the mentioned script/work with this email. It’s an evidence of the exchange and will also help you keep a record.</p></li>
                  <li class="sub-mem-li"><p class="text-justify">Keep a diary and note down all relevant dates and times when you have met and narrated a script/work.</p></li>
                </ul> 
              </li> 
              <li class="mem-li">
                 <h5>Sign The Right The Contract</h5>
                <ul class="sub-mem">
                  <li class="sub-mem-li"><p class="text-justify">ALWAYS sign a contract before commencing a project. Do not work without a Contract.</p></li>
                  <li class="sub-mem-li"><p class="text-justify">While signing a contract insist that a copy be given to you immediately.</p></li>
                  <li class="sub-mem-li"><p class="text-justify">Do not sign away your rights in a Contract. Sometimes it is better to refuse an offer rather than regret it at a later date. </p></li>
                  <li class="sub-mem-li"><p class="text-justify">Insist on the model Minimum Basic Contract proposed by the Screenwriters Association. (The producers’ bodies are yet to approve the model contract but it’s strongly recommended by SWA. Download it here.)</p></li>
                  <li class="sub-mem-li"><p class="text-justify">If provided by the producer/production houses, don't sign the contract before you take legal advice on it. You can contact SWA’s lawyer if you have any doubts about any clause of the contract or stages of signing it. (Only for SWA members. Non-members can read the FAQs related to legal advice and our Ask Our Lawyer column.)</p></li>
                  <li class="sub-mem-li"><p class="text-justify">Insist upon inserting a clause in your contract whereby is clearly stated that in case of any dispute/s they will take up the matter to their association’s forum. </p></li>
                  <li class="sub-mem-li"><p class="text-justify">Do not sign contracts that insist that you cannot revert to SWA in case of a dispute and/or ask you to sign away any other right/s.</p></li>
                </ul> 
              </li> 
            </ul>
              <h4 class="sub-hd">DON’TS</h4>
              <ul class="mem">
               <li class="mem-li">Don't copy or plagiarize.</li>
               <li class="mem-li">Don't encourage or work for ‘inspired’ remakes of films unless proper rights to do the same have been procured by the Producer.</li>
               <li class="mem-li">Don't be in a hurry to sell the first draft of your screenplay. Give it time and rework on more drafts, fine-tuning your craft. </li>
               <li class="mem-li">Don't share unregistered work with Producers/Production Houses/Channels/filmmakers. </li>
               <li class="mem-li">DON'T WRITE WITHOUT A CONTRACT. Doing so is unprofessional and gives you no protection - legal, monetary or otherwise.</li>
               <li class="mem-li">Look out if there's a clause in your agreement/contract that prohibits you from approaching SWA in case of a dispute. If so, DON'T sign it.</li>
               <li class="mem-li">Don't wait for your scheduled payments. As and when the producer starts delaying payments, take the help of SWA’s DSC. </li>
               <li class="mem-li">Don't be afraid to name your price and to say 'NO' to Producer/Production houses if something doesn't suit you in the contract.</li>
               </ul>
          </div>
        </div>   
      </div>
    </div>
  </div>
</div>
<?php include_once('footer.php'); ?>
</div>
<script src="js/jquery-1.11.3.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.bxslider.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
<script src="js/jquery.counterup.min.js"></script>
<script src="js/custom.js"></script>
<!-- <script type="text/javascript" src="js/jquery.mixitup.min.js"></script> -->
<!-- <script type="text/javascript">
  $(function () {

    var filterList = {

      init: function () {

        // MixItUp plugin
        // http://mixitup.io
        $('#portfoliolist').mixItUp({
          selectors: {
            target: '.portfolio',
            filter: '.filter' 
          },
          load: {
            filter: '.exe'  
          }     
        });               

      }

    };
    
    // Run the show!
    filterList.init();
    
    
  }); 
  
</script> -->
</body>
</html>