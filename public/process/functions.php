<?php
function sendMail($username,$password,$senderName,$senderEmail,$recName,$recEmail,$body,$subject)
{
	include_once('../PHPMailerAutoload.php');
    $mail = new PHPMailer(true);
    $mail->IsSMTP(); // telling the class to use SMTP
    $mail->IsHTML();
    $mail->SMTPAuth = true; // enable SMTP authentication
    $mail->SMTPSecure = "ssl"; // sets the prefix to the servier
    $mail->Host = "smtp.gmail.com"; // sets GMAIL as the SMTP server
    $mail->Port = 465; // set the SMTP port for the GMAIL server
    $mail->Username = $username; // GMAIL username
    $mail->Password = $password; // GMAIL password

    $mail->AddAddress($recEmail,$recName);
		$mail->AddReplyTo($senderEmail, $senderName);
    // $mail->AddAddress('nirmeet@firsteconomy.com');
    $mail->SetFrom($senderEmail, $senderName);
    $mail->Subject = $subject;
    $mail->Body = $body;

    $result = $mail->Send();
    return $result;
}

function enc_num($number)
{
	for($i=1;$i<=3;$i++)
	{
		$number = strrev(base64_encode($number));
	}
	return $number;
}

function dec_num($number)
{
    for($i=1;$i<=3;$i++)
	{
		$number = base64_decode(strrev($number));
	}
	// var_dump($number);die;
	return $number;
}

?>
