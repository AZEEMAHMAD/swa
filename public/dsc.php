<!doctype html>
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="icon" type="image/x-icon" href=""/>
      <title> Ask The DSC</title>
      <link href="css/custom.css" rel="stylesheet" type="text/css">
      <link href="css/color.css" rel="stylesheet" type="text/css">
      <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
      <link href="css/responsive.css" rel="stylesheet" type="text/css">
      <link href="css/jquery.bxslider.css" rel="stylesheet" type="text/css">
      <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
      <link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen"/>
      <link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,300' rel='stylesheet' type='text/css'>
      <link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
      <style>
      #filters {
  margin:1%;
  padding:0;
  list-style:none;
}

  #filters li {
    float:left;
  }
  
  #filters li span {
    display: block;
    padding:5px 20px;   
    text-decoration:none;
    color:#666;
    cursor: pointer;
  }
  
  #filters li span.active {
    background: #e95a44;
    color:#fff;
  }
 

 
#portfoliolist .portfolio {
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  -o-box-sizing: border-box;
  width:23%;
  margin:1%;
  display:none;
  float:left;
  overflow:hidden;
}

  .portfolio-wrapper {
    overflow:hidden;
    position: relative !important;
    background: #666;
    cursor:pointer;
  }

  .portfolio img {
    max-width:100%;
    position: relative;
    top:0;
    -webkit-transition: all 600ms cubic-bezier(0.645, 0.045, 0.355, 1);
    transition:         all 600ms cubic-bezier(0.645, 0.045, 0.355, 1);   
  }
  
  .portfolio .label {
    position: absolute;
    width: 100%;
    height:40px;
    bottom:-40px;
    -webkit-transition: all 300ms cubic-bezier(0.645, 0.045, 0.355, 1);
    transition:         all 300ms cubic-bezier(0.645, 0.045, 0.355, 1);
  }

    .portfolio .label-bg {
      background: #e95a44;
      width: 100%;
      height:100%;
      position: absolute;
      top:0;
      left:0;
    }
  
    .portfolio .label-text {
      color:#fff;
      position: relative;
      z-index:500;
      padding:5px 8px;
    }
      
      .portfolio .text-category {
        display:block;
        font-size:9px;
      }
  
  .portfolio:hover .label {
    bottom:0;
  }
  .portfolio:hover img {
    top:-30px;
  }  
      </style>
   </head>
   <body class="inner-page">
      <div id="wrapper">
        <?php include_once('header.php'); ?>
         <div id="cp-content-wrap">
          <!-- Banner -->
           <div class="banner_inner">
             <img src="images/bg.jpg">
           </div>
           <!-- End of Banner -->
            <div class="cp_our-story-section">
               <div class="container">
                  <div class="row">
                     <div class="col-md-12 abt-div">
                        <h1 class="title bold text-center">
                           Ask The DSC
                        </h1>
                        <p class="text-justify">The Dispute Settlement Sub-Committee is a QUASI-LEGAL body set up by the Screenwriters Association for the sole purpose of MEDIATION and CONCILIATION between its members and members of other craft associations, including producers/employers. Any SWA member can lodge a complaint, supported by the necessary documents, in the DSC.</p>
                        <p class="text-justify">Read the frequently asked questions related to DSC <a href = "faqs.php">here</a></p>
                        <h4 class="sub-hd" id="guidelines">DSC GUIDELINES & BYE-LAWS </h4>
                        <p class="italic">(We’re in the process of updating DSC Guidelines & Bye-laws. Meanwhile, refer to the Constitution of SWA, <a href = "faqs.php">here</a>)</p>
                        <br>
                        <h4 class="sub-hd" id="complaint">FILING A COMPLAINT</h4>
                        <p class="text-justify">Submit the HARD COPIES (in person OR via post) of the following, to SWA office. Additionally, you can email to us that you’ve submitted (OR dispatched) the documents, while also attaching the soft copies (scanned/PDF) of all the documents.</p>
                        <ul class="mem">
                          <li class="mem-li">
                            <p class="text-justify">Duly filled and signed DSC Complaint Form</p>
                          </li>
                          <li class="mem-li">
                            <p class="text-justify">Duly filled and signed DSC Undertaking Form (on plain paper)</p>
                          </li>
                          <li class="mem-li">
                            <p class="text-justify">Required Documents (whichever is applicable)</p>
                            <ul class="sub-mem">
                              <li class="sub-mem-li"><p class="text-justify">Xerox copy of Registered Script/Work (related to the complaint)</p></li>
                              <li class="sub-mem-li"><p class="text-justify">Xerox copy of Signed Contract (related to the complaint)</p></li>
                              <li class="sub-mem-li"><p class="text-justify">Xerox of E-mails, Text/What’s App Messages, Newspaper-Cuttings, Poster, Screenshots etc. (related to the complaint)</p></li>
                            </ul> 
                          </li> 
                      </ul>
                      <br>
                      <h4 class="sub-hd" id="forms">Write to our DSC</h4>
                      <div class="cp-acticle-box">
                          <div class="download-box"><i class="fa fa-download" aria-hidden="true"></i>&nbsp;Download DSC Complaint Form</div>
                          <div class="download-box"><i class="fa fa-download" aria-hidden="true"></i>&nbsp;Download DSC UNDERTAKING</div>
                        </div>
                        <!-- <div class="cp-acticle-box">
                           <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                             <div class="panel panel-default">
                               <div class="panel-heading" role="tab" id="headingOne">
                                 <h4 class="panel-title">
                                   <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                     <strong class="qa">Q.</strong> I am a new writer. if i become a member with FWA, will it help me if my rights are violated? 
                                   </a>
                                 </h4>
                               </div>
                               <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                 <div class="panel-body">
                                   <p><strong class="qa">A.</strong> Yes. FWA's Dispute Settlement Committee will take measures in helping you with the legal procedure of claiming your due rights in case of a copyright infringement or theft of your registered work or in a matter concerning wages and credit.</p>
                                 </div>
                               </div>
                             </div>
                             <div class="panel panel-default">
                               <div class="panel-heading" role="tab" id="headingTwo">
                                 <h4 class="panel-title">
                                   <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                     <strong class="qa">Q.</strong> Suppose I'm not a member of FWA but right now in a dispute with a party who I feel, has violated my rights. Can I turn to FWA for supprot?
                                   </a>
                                 </h4>
                               </div>
                               <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                 <div class="panel-body">
                                  <p><strong class="qa">A.</strong>No. FWA is a trade union which provides help and support only to its members.</p>
                                 </div>
                               </div>
                             </div>
                             <div class="panel panel-default">
                               <div class="panel-heading" role="tab" id="headingThree">
                                 <h4 class="panel-title">
                                   <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    <strong class="qa">Q.</strong> Can I turn to any other organization/union if I'm not a member of FWA in a case of a copy right infringement? 
                                   </a>
                                 </h4>
                               </div>
                               <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                 <div class="panel-body">
                                  <p><strong class="qa">A.</strong>Yes. You can turn to the court in case of an IPR violation and hire the services of lawyers with expertise in related matters.</p>
                                 </div>
                               </div>
                             </div>
                             <div class="panel panel-default">
                               <div class="panel-heading" role="tab" id="headingFour">
                                 <h4 class="panel-title">
                                   <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    <strong class="qa">Q.</strong>They say I don't get a copyright on an 'idea'. What does that mean? How can I ensure that my original thought gets a leagal acknowledgement?
                                   </a>
                                 </h4>
                               </div>
                               <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                 <div class="panel-body">
                                    <p><strong class="qa">A.</strong>An idea is a seed, germ of a thought; which has not been worked upon to its capacity. An idea, by sheer co-incidence can strike more than one person. The best practics is to develop your ideas into treatments or stories. Please get them registered before narrating to anyone.</p>
                                 </div>
                               </div>
                             </div>
                             <div class="panel panel-default">
                               <div class="panel-heading" role="tab" id="headingFive">
                                 <h4 class="panel-title">
                                   <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    <strong class="qa">Q.</strong> I have been fired from a project i was doing. The director/producer is now working with another writer/s. If my contract confirms this act as a violation should I go to DSC first or send a legal notice directly?  
                                   </a>
                                 </h4>
                               </div>
                               <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                                 <div class="panel-body">
                                    <p><strong class="qa">A.</strong> You should come to the DSC and they shall take your case forward. Taking any legal action without following the proper procedure would bar DSC to intervene at a later stage. Please insist on NOC's if you are taking over the job of another writer. </p>
                                 </div>
                               </div>
                             </div>
                             <div class="panel panel-default">
                               <div class="panel-heading" role="tab" id="headingSix">
                                 <h4 class="panel-title">
                                   <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                    <strong class="qa">Q.</strong> I have had script meetings with a director/procedure and i have given ideas/concepts which were liked. In the meanwhile he got busy with some other things and we stopped meeting. I came to know recently that he's working on a similar idea all by himself (or say with some other writer/s). How can I claim my rights on the matter? 
                                   </a>
                                 </h4>
                               </div>
                               <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                                 <div class="panel-body">
                                    <p><strong class="qa">A.</strong>Keep registering your ideas as and when you narrate them or give them to the producer /s. Keep a reord of your meetings. The DSC shall help you get your dues in such cases. </p>
                                 </div>
                               </div>
                             </div>
                           </div>
                        </div> -->
                     </div>
                  </div>
               </div>
            </div>
         </div>
          <?php include_once('footer.php'); ?>
      </div>
      <script src="js/jquery-1.11.3.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="js/jquery.bxslider.min.js"></script>
      <script src="js/owl.carousel.min.js"></script>
      <script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
      <script src="js/jquery.counterup.min.js"></script>
      <script src="js/custom.js"></script>
   </body>
</html>