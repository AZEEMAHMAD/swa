<!doctype html>
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="icon" type="image/x-icon" href=""/>
      <title>Film Writers Association | Tips</title>
      <link href="css/custom.css" rel="stylesheet" type="text/css">
      <link href="css/color.css" rel="stylesheet" type="text/css">
      <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
      <link href="css/responsive.css" rel="stylesheet" type="text/css">
      <link href="css/jquery.bxslider.css" rel="stylesheet" type="text/css">
      <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
      <link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen"/>
      <link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,300' rel='stylesheet' type='text/css'>
      <link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
   </head>
   <body class="inner-page">
      <div id="wrapper">
         <?php include_once('header.php'); ?>
         <div id="cp-content-wrap">
            <div class="cp_our-story-section">
               <div class="container">
                  <div class="row">
                     <div class="col-md-12">
                        <h1 class="title bold text-center">
                          TIPS
                        </h1>
                        <div class="cp-acticle-box tips-div">
                          <ol class="tips">
                            <li>Do not work without a Contract. When signing a contract insist that a copy be given to you immediately.</li>
                            <li>Do not sign away your rights in a Contract. Sometimes it is better to refuse a offer rather than regret it at a later date. Please refer to ASK OUR LAWYER and ASK FWA.</li>
                            <li>After narrating a subject e-mail the same with a note to the person you have narrated the subject to.</li>
                            <li>Keep a diary and note down all relevant dates and times when you have met and narrated a subject.</li>
                            <li>Be specially careful when you narrate a subject to a channel. Insist on recording the meeting and the subject discussed.</li>
                            <li>Keep your registered copy of your work safe. Never give an original copy to the Producer/Director/Channel.</li>
                            <li>The Dispute Settlement Committee may not entertain your complaint if you go to the media.</li>
                            <li>The Dispute Settlement Committee cannot entertain your case if it is subjudice.</li>
                            <li>The DSC takes time to operate so be patient. Be assured. It has a high rate of success.</li>
                          </ol>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
          <?php include_once('footer.php'); ?>
      </div>
      <script src="js/jquery-1.11.3.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="js/jquery.bxslider.min.js"></script>
      <script src="js/owl.carousel.min.js"></script>
      <script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
      <script src="js/jquery.counterup.min.js"></script>
      <script src="js/custom.js"></script>
   </body>
</html>