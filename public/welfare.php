<!doctype html>
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="icon" type="image/x-icon" href=""/>
      <title>Film Writers Association | Complain Form</title>
      <link href="css/custom.css" rel="stylesheet" type="text/css">
      <link href="css/color.css" rel="stylesheet" type="text/css">
      <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
      <link href="css/responsive.css" rel="stylesheet" type="text/css">
      <link href="css/jquery.bxslider.css" rel="stylesheet" type="text/css">
      <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
      <link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen"/>
      <link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,300' rel='stylesheet' type='text/css'>
      <link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
   </head>
   <body class="inner-page">
      <div id="wrapper">
         <?php include_once('header.php'); ?>
         <div id="cp-content-wrap">
            <!-- Banner -->
             <div class="banner_inner">
               <img src="images/bg.jpg">
             </div>
             <!-- End of Banner -->
            <div class="cp_our-story-section">
               <div class="container">
                  <div class="row">
                     <div class="col-md-12">
                        <h1 class="title bold text-center">
                          Welfare Schemes
                        </h1>
                        <div class="cp-acticle-box ">
                          <p>Hrishikesh Mukherjee Educational Fund For Bedi Rahi Scholarships</p>
                           <a target="_blank" href="pdf/Hrishikesh Mukherjee Page.pdf" class="dwn-btn"><div class="download-box"><i class="fa fa-download" aria-hidden="true"></i>&nbsp;Download Form</div></a>
                        </div>
                        <div class="cp-acticle-box ">
                          <p>Parivarik Sahaeta Kosh</p>
                          <a  target="_blank" href="pdf/Parivarik Sahaeta Kosh.pdf" class="dwn-btn"><div class="download-box"><i class="fa fa-download" aria-hidden="true"></i>&nbsp;Download Form</div></a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
          <?php include_once('footer.php'); ?>
      </div>
      <script src="js/jquery-1.11.3.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="js/jquery.bxslider.min.js"></script>
      <script src="js/owl.carousel.min.js"></script>
      <script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
      <script src="js/jquery.counterup.min.js"></script>
      <script src="js/custom.js"></script>
   </body>
</html>