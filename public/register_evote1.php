<?php session_start();
//echo "<pre>";print_r($_SESSION);die;
if(isset($_SESSION['is_login']) &&  $_SESSION['is_login'] == 'true')
{
    include_once('includes/config.php');

    $sql = "select fu.*,mem.* from `fwa_users` fu
    left join `fwa_members` mem on mem.reg_no = fu.reg_no
    where fu.reg_no = '".$_SESSION['username']."'";
    $result =  mysqli_query($db,$sql);
    $row = mysqli_fetch_assoc($result);

    if($_SESSION['exp_flag'] == 1 || $row['evote_flag'] == 1 )
    {
        $_SESSION['evote']['msg'] = "You've already opted to eVote";
        header('Location: dashboard.php');
    }

    // var_dump(time() , strtotime($evoting_disable_time));die;

    if( time() > strtotime($evoting_disable_time))
    {
        $_SESSION['evote']['msg'] = "Duration for opting to eVote is over";
        header('Location: dashboard.php');
    }
    ?>

    <!doctype html>
    <html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/x-icon" href=""/>
        <title>Film Writers Association | My Creations</title>
        <link href="css/custom.css" rel="stylesheet" type="text/css">
        <link href="css/color.css" rel="stylesheet" type="text/css">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
        <link href="css/responsive.css" rel="stylesheet" type="text/css">
        <link href="css/jquery.bxslider.css" rel="stylesheet" type="text/css">
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen"/>
        <link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,300' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style media="screen">
            .error{ border: 1px solid red!important;}
        </style>
    </head>
    <body class="inner-page">
        <div id="wrapper" class="inside-menu">
            <?php include_once('header.php'); ?>
            <div id="cp-content-wrap" class="page404 cp-login-page">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 main-reg-div">
                            <div class="cp-reg-box sub-reg-div">

                                <?php

                                //var_dump($row);die;
                                ?>
                                <h2>Update Your Details</h2>
                                <form action="register_evote_update.php" method="post" id="update_dets_frm">
                                    <table class="table table-hover">
                                        <tbody>
                                            <tr>
                                                <th>Email</th>
                                                <td><input type="text" class="form-control" id="email" name="email" value="<?php echo $row['email']; ?>"></td>
                                            </tr>
                                            <tr>
                                                <th>Mobile no</th>
                                                <td><input type="text" class="form-control" id="mobileno" name="mobileno" value="<?php echo $row['mobileno']; ?>"></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" class="text-center">
                                                    <button type="submit" href="javascript:void(0)" class="btn btn-success" role="button">Update Details & Proceed</button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php include_once('footer.php'); ?>
        </div>
        <script src="js/jquery-1.11.3.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.bxslider.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/jquery.prettyPhoto.js"></script>
        <script src="js/custom.js"></script>
        <script type="text/javascript">
        $(document).ready(function(){
            $('#update_dets_frm').on('submit',function() {

                var EmailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
                var phonePattern = /^[0-9]*$/;

                var email = $('#email').val();
                var mobile = $('#mobileno').val();

                var flag = 0;

                if(!phonePattern.test(mobile) || mobile=='' ||  mobile==null)
                {
                    $('#mobileno').addClass('error');
                    flag++;
                }
                else
                {
                    $('#mobileno').removeClass('error');
                }

                if(!EmailPattern.test(email) || email=='' ||  email==null)
                {
                    $('#email').addClass('error');
                    flag++;
                }
                else
                {
                    $('#email').removeClass('error');
                }

                if(flag == 0)
                {
                    // console.log('ttttttt');
                    return true;
                }
                else
                {
                    // console.log('ffffff');
                    return false;
                }

            })

        });
        </script>
    </body>
    </html>
<?php }
else{
    header('Location: login.php');
} ?>
