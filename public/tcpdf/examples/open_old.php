<?php
//require_once('../config/lang/eng.php');
//require_once('../tcpdf.php');
require_once('tcpdf_include.php');

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true); 
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); 
//$pdf->setLanguageArray($l); 
//$pdf->AliasNbPages();
$pdf->SetFont("helvetica", "", 10);

$pdf->AddPage();
$htmlcontent = file_get_contents("mypdf.pdf", false);
$pdf->writeHTML($htmlcontent, true, 0, true, 0);

$pdf->Output("newmypdf.pdf", "I");
?>