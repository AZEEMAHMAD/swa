jQuery(document).ready(function($) {
    "use strict";
    var host_url = location.protocol + "//" + location.host;

    // if ($('.gallery').length) {
    //     $("area[data-rel^='prettyPhoto']").prettyPhoto();
    //     $(".gallery:first a[data-rel^='prettyPhoto']").prettyPhoto({
    //         animation_speed: 'normal',
    //         theme: 'light_square',
    //         slideshow: 3000,
    //         autoplay_slideshow: false
    //     });
    // }




  $('li.drop-down').click(function() {
     console.log('cliked');
    $('li.drop-down > ul').not($(this).children("ul").toggle()).hide();
    // console.log('cliked');
    $('li.drop-down').not($(this).toggleClass('changed')).removeClass('changed');
});


    if ($('#cp-banner-1').length) {
        $('#cp-banner-1').bxSlider({
            infiniteLoop: true,
            auto: true,
            hideControlOnEnd: true
        });
    }
    if ($('.cp-post-slider').length) {
        $('.cp-post-slider').bxSlider({
            infiniteLoop: true,
            auto: true,
            hideControlOnEnd: true
        });
    }
    if ($('#cp-about-slider').length) {
        $('#cp-about-slider').bxSlider({
            infiniteLoop: true,
            auto: true,
            pager: false,
            nextText: '',
            prevText: ' ',
        });
    }
    if ($('.cp-home2-slider').length) {
        $('.cp-home2-slider').bxSlider({
            auto: true,
            pager: true,
            controls: false,
        });
    }
    if ($('#cp-post-carousel').length) {
        $("#cp-post-carousel").owlCarousel({
            autoPlay: 3000,
            items: 3,
            pagination: false,
            itemsCustom: false,
            itemsDesktop: [1199, 3],
            lazyLoad: true,
            navigation: true,
            itemsDesktopSmall: [979, 3]
        });
    }
    if ($('#cp_post-slider').length) {
        $("#cp_post-slider").owlCarousel({
            autoPlay: 3000,
            items: 1,
            pagination: true,
            itemsDesktop: [1199, 1],
            itemsDesktopSmall: [979, 1],
            itemsTablet: [768, 1],
            itemsMobile: [479, 1],
            lazyLoad: true,
            navigation: false,
        });
    }
    if ($('.counter').length) {
        $('.counter').counterUp({
            delay: 10,
            time: 1000
        });
    }
    $('.burger').on('click', function() {
        if ($('.burger').is('.expand')) {
            $('.search').fadeOut('fast');
            $(this).delay(100).queue(function() {
                $(this).removeClass('expand').dequeue();
            });
        } else {
            $(this).delay(100).queue(function() {
                $(this).addClass('expand').dequeue();
            });
            $('.search').delay(200).fadeIn('fast');
        }
    });
    if ($('#defaultCountdown').length) {
        var austDay = new Date();
        austDay = new Date(2016, 11 - 1, 29);
        $('#defaultCountdown').countdown({
            until: austDay
        });
        $('#year').text(austDay.getFullYear());
    }
    if ($(".cp-grid-isotope .isotope").length) {
        var $container = $('.cp-grid-isotope .isotope');
        $container.isotope({
            itemSelector: '.item',
            transitionDuration: '0.6s',
            masonry: {
                columnWidth: $container.width() / 12
            },
            layoutMode: 'masonry'
        });
        $(window).resize(function() {
            $container.isotope({
                masonry: {
                    columnWidth: $container.width() / 12
                }
            });
        });
    }
    if ($(".cp-news-isotope .isotope").length) {
        var $container = $('.cp-news-isotope .isotope');
        $container.isotope({
            itemSelector: '.item',
            transitionDuration: '0.6s',
            masonry: {
                columnWidth: $container.width() / 12
            },
            layoutMode: 'masonry'
        });
        $(window).resize(function() {
            $container.isotope({
                masonry: {
                    columnWidth: $container.width() / 12
                }
            });
        });
    }

     if ($('#review-slider').length) {
        $('#review-slider').bxSlider({
            auto: true,
            controls: false,
            mode: 'vertical',
            slideMargin: 10,
            hideControlOnEnd:true
        });
    }
    if ($('#swa_article').length) {
        $('#swa_article').bxSlider({
            auto: true,
            pager: false,
            touchEnabled:true,
             preventDefaultSwipeY: false,
            controls: true,
        });
    }

    /* Demo purposes only */
    $(".hover").mouseleave(
      function () {
        $(this).removeClass("hover");
      }
    );

    /** login */
    $('#login_btn').on('click',function(){
        var username = $('#username').val();
        var password = $('#password').val();
        var flag = 0;
        if(username=='' ||  username==null)
        {
            $('#username').addClass('error');
            flag++;
        }
        else
        {
            $('#username').removeClass('error');
        }

        if(password=='' ||  password==null)
        {
            $('#password').addClass('error');
            flag++;
        }
        else
        {
            $('#password').removeClass('error');
        }
        if(flag==0)
        {
        var ajax = '1';
        var ajax_data = {
                    ajax : ajax,
                password : password,
                username : username,
            }
            $.ajax({
                type: "POST",
                url: host_url + '/process/ajax.php',
                data: ajax_data,
                success: function(result) {
                    if(result == 'Success')
                    {
                        window.location.href = "dashboard.php";
                    }
                    else
                    {
                        $('#login_message').html('<span class="label label-danger">'+result+'</span>')
                    }

                },
                error: function() {

                }
            });
        }
        });
     /** login end */

    /** reset password */
    $('#reset_btn').on('click',function(){
        var username = $('#username').val();
        var email = $('#email').val();
        var flag = 0;
        if(username=='' ||  username==null)
        {
            $('#username').addClass('error');
            flag++;
        }
        else
        {
            $('#username').removeClass('error');
        }

        if(email=='' ||  email==null)
        {
            $('#email').addClass('error');
            flag++;
        }
        else
        {
            $('#email').removeClass('error');
        }

        if(flag==0)
        {
        var ajax = '4';
        var ajax_data = {
                ajax : ajax,
                email : email,
                username : username,
            }
            $.ajax({
                type: "POST",
                url: host_url + '/process/ajax.php',
                data: ajax_data,
                beforeSend:function(result) {
                    $('#reset_btn').html('Please wait..');
                },
                success: function(result) {
                    $('#reset_btn').html('Reset Password');
                    if(result == '1')
                    {
                        $('#reset_message').html('<span class="label label-success">An email containing reset link was sent to your email id.</span>');
                    }
                    else if(result == '2')
                    {
                        $('#reset_message').html('<span class="label label-danger">Invalid registration no. or email id / user not found</span>');
                    }
                    else
                    {
                        $('#reset_message').html('<span class="label label-danger">Something went wrong</span>');
                    }
                },
                error: function() {
                    $('#reset_btn').html('Reset Password');
                }
            });
        }
        });
     /** reset password end */

     /** reset update password */
    $('#setpassword_btn').on('click',function(){
        var password = $('#password').val();
        var re_password = $('#re_password').val();
        var q = $('#q').val();
        var flag = 0;
        if(password=='' ||  password==null)
        {
            $('#password').addClass('error');
            flag++;
        }
        else
        {
            $('#password').removeClass('error');
        }

        if(re_password=='' ||  re_password==null || password != re_password )
        {
            $('#re_password').addClass('error');
            flag++;
        }
        else
        {
            $('#re_password').removeClass('error');
        }

        if(flag==0)
        {
        var ajax = '5';
        var ajax_data = {
                ajax : ajax,
                q : q,
                password : password,
            }
            $.ajax({
                type: "POST",
                url: host_url + '/process/ajax.php',
                data: ajax_data,
                beforeSend: function(result) {

                },
                success: function(result) {
                    if(result == '1')
                    {
                        $('#setpassword_message').html('<span class="label label-success">Password successfully updated</span>');
                        $('#password,#re_password').val('');
                        //redirect to login.php
                        setTimeout(function(){  window.location.href = 'login.php'; }, 5000);
                    }
                    else if(result == '2')
                    {
                        $('#setpassword_message').html('<span class="label label-danger">User not found</span>');
                    }
                    else
                    {
                        $('#setpassword_message').html('<span class="label label-danger">Something went wrong</span>');
                    }
                },
                error: function() {
                    $('#setpassword_message').html('<span class="label label-danger">Something went wrong</span>');
                }
            });
        }
        });
     /** reset update password end */

    /** Register step1 */
    $('#reg_btn').on('click',function(){
        var reg_no = $('#reg_no').val();
        //console.log(reg_no);
        // var name = $('#name').val();
        // var email = $('#email').val();
        // var dob = $('#dob').val();
        var mobile = $('#mobile').val();
        var flag = 0;
        var EmailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
        var phonePattern = /^[0-9]*$/;

        if(reg_no=='' ||  reg_no==null)
        {
            $('#reg_no').addClass('error');
            flag++;
        }
        else
        {
            $('#reg_no').removeClass('error');
        }

        // if(name=='' ||  name==null)
        // {
        //     $('#name').addClass('error');
        //     flag++;
        // }
        // else
        // {
        //     $('#name').removeClass('error');
        // }


        // if(!EmailPattern.test(email) || email=='' ||  email==nul)
        // {
        //     $('#email').addClass('error');
        //     flag++;
        // }
        // else
        // {
        //     $('#email').removeClass('error');
        // }

        // if(dob=='' ||  dob==null)
        // {
        //     $('#dob').addClass('error');
        //     flag++;
        // }
        // else
        // {
        //     $('#dob').removeClass('error');
        // }

        // if(!phonePattern.test(mobile) || mobile=='' ||  mobile==null)
        // {
        //     $('#mobile').addClass('error');
        //     flag++;
        // }
        // else
        // {
        //     $('#mobile').removeClass('error');
        // }

        if(flag==0)
        {
        $('#reg_btn').prop('disabled', true).html('Please Wait');
        var ajax = '2';
        var ajax_data = {
                ajax : ajax,
                reg_no : reg_no,
                // name : name,
                // email:email,
                // dob:dob,
                // mobile:mobile,
            }
            $.ajax({
                type: "POST",
                url: host_url + '/process/ajax.php',
                data: ajax_data,
                success: function(result) {
                  $('#reg_btn').prop('disabled', false).html('Sign Up');
                    result = result.replace('Sent.','');
                    console.log(result);
                    var response=JSON.parse(result);
                    console.log(response.success+' | '+response.message);
                    if(response.success == 1)
                    {
                        $('#reg_msg').html('<span class="label label-success">'+response.message+'</span>');
                        $('#ref_frm').addClass('hidden');
                        $('#code_frm').removeClass('hidden');
                        $('#reg_no_sec').val(response.reg_no);
                        // $('#mobile_sec').val(response.mobile);
                    }
                    else
                    {
                        $('#reg_msg').html('<span class="label label-warning">'+response.message+'</span>');
                    }
                },
                error: function() {
                  $('#reg_btn').prop('disabled', false).html('Sign Up');
                    $('#reg_msg').html('<span class="label label-danger">Something went wrong</span>');
                }
            });
        }
        });
     /** Register step 2*/
    $('#securecode_reg').on('click',function(){
        var reg_no = $('#reg_no_sec').val();
        var mobile = $('#mobile_sec').val();
        var securecode = $('#securecode').val();
        var flag = 0;

        if(securecode=='' ||  securecode==null)
        {
            $('#securecode').addClass('error');
            flag++;
        }
        else
        {
            $('#securecode').removeClass('error');
        }

        if(flag==0)
        {
        var ajax = '6';
        var ajax_data = {
                ajax : ajax,
                reg_no : reg_no,
                // mobile:mobile,
                securecode:securecode,
            }
            $.ajax({
                type: "POST",
                url: host_url + '/process/ajax.php',
                data: ajax_data,
                success: function(result) {
                    var response =  JSON.parse(result);
                    if(response.success == 1){
                        window.location.href = 'setpassword.php';
                    }
                    else
                    {
                        $('#reg_msg').html('<span class="label label-warning">'+response.message+'</span>');
                    }
                },
                error: function() {
                    $('#reg_msg').html('<span class="label label-warning">Something went wrong</span>');
                }
            });
        }
        });

    /** Register for Web Set Password */
    $('#web_setpassword_btn').on('click',function(){
        var password = $('#password').val();
        var re_password = $('#re_password').val();
        var flag = 0;

        if(password=='' ||  password==null)
        {
            $('#password').addClass('error');
            flag++;
        }
        else
        {
            $('#password').removeClass('error');
        }

        if(re_password=='' ||  re_password==null || password != re_password)
        {
            $('#re_password').addClass('error');
            flag++;
        }
        else
        {
            $('#re_password').removeClass('error');
        }

        if(flag==0)
        {
        var ajax = '7';
        var ajax_data = {
                ajax : ajax,
                password : password,
            }
            $.ajax({
                type: "POST",
                url: host_url + '/process/ajax.php',
                data: ajax_data,
                success: function(result) {
                    var response =  JSON.parse(result);
                    if(response.success == 1){
                        window.location.href = 'dashboard.php';
                    }
                    else
                    {
                        $('#setpassword_message').html('<span class="label label-warning">'+response.message+'</span>');
                    }
                },
                error: function() {
                    $('#setpassword_message').html('<span class="label label-warning">Something went wrong</span>');
                }
            });
        }
        });
    /** Co Author step 1 */
    $('#coa_otp').on('click',function(){
        var coa_mem = $('#coa_membership').val();
        var flag = 0;

        if(coa_mem=='' ||  coa_mem==null)
        {
            $('#coa_membership').addClass('error');
            flag++;
        }
        else
        {
            $('#coa_membership').removeClass('error');
        }

        if(flag==0)
        {
        var ajax = '8';
        var ajax_data = {
                ajax : ajax,
                coa_mem : coa_mem,
            }
            $.ajax({
                type: "POST",
                url: host_url + '/process/ajax.php',
                data: ajax_data,
                success: function(result) {
                    var response =  JSON.parse(result);
                    if(response.success == 1){
                        $('#coa_msg').html(response.message);
                        $('#coa_frm').addClass('hidden');
                        $('#coa_ss_frm').removeClass('hidden');
                        $('#coa_mem').val(response.coa_mem);
                    }
                    else
                    {
                      $('#coa_msg').html('<span class="label label-warning">'+response.message+'</span>');
                    }
                },
                error: function() {
                    $('#coa_msg').html('<span class="label label-warning">Something went wrong</span>');
                }
            });
        }
        });
    /** Co Author step 2 */
    $('#coa_reg').on('click',function(){
        var coa_sc = $('#coa_securecode').val();
        var coa_mem = $('#coa_mem').val();
        var flag = 0;

        if(coa_sc=='' ||  coa_sc==null)
        {
            $('#coa_securecode').addClass('error');
            flag++;
        }
        else
        {
            $('#coa_securecode').removeClass('error');
        }

        if(flag==0)
        {
        var ajax = '9';
        var ajax_data = {
                ajax : ajax,
                coa_sc : coa_sc,
                coa_mem:coa_mem,
            }
            $.ajax({
                type: "POST",
                url: host_url + '/process/ajax.php',
                data: ajax_data,
                success: function(result) {
                    var response =  JSON.parse(result);
                    if(response.success == 1){
                      $('#coa_msg').html('<span class="label label-success">Author Added</span>');
                      setTimeout(function(){
                        //   $('#coa_msg').html('');
                        // $('#coa_membership').val('');
                        // $('#coa_mem').val('');
                        // $('#coa_securecode').val('');
                        // $('#coa_frm').removeClass('hidden');
                        // $('#coa_ss_frm').addClass('hidden');
                        // $('#coa_ss_frm').addClass('hidden');
                        // $('#authModel').modal('hide');
                        window.location.reload();
                      }, 1500);
                    }
                    else
                    {
                      $('#coa_msg').html('<span class="label label-warning">'+response.message+'</span>');
                    }
                },
                error: function() {
                    $('#coa_msg').html('<span class="label label-warning">Something went wrong</span>');
                }
            });
        }
        });

    /** Co Author remove */
    $('.remove_auth').on('click',function(){
        var coa_mem = $(this).data('coa');
        // alert(coa_mem);return false;
        var ajax = '10';
        var ajax_data = {
                ajax : ajax,
                coa_mem:coa_mem,
            }
            $.ajax({
                type: "POST",
                url: host_url + '/process/ajax.php',
                data: ajax_data,
                success: function(result) {
                    var response =  JSON.parse(result);
                    if(response.success == 1){
                      window.location.reload();
                    }
                    else
                    {
                      // $('#coa_msg').html('<span class="label label-warning">'+response.message+'</span>');
                      $.notify("Something went wrong", "error");
                    }
                },
                error: function() {
                    $.notify("Something went wrong", "error");
                }
            });
        });


        /** Contact Form */
        $('#contact_form').on('click',function(){
            var name = $('#name').val();
            var email = $('#email').val();
            var reg_no = $('#reg_no').val();
            var subject = $('#subject').val();
            // var mobile = $('#mobile').val();
            var comments = $('#comments').val();

            var flag = 0;
            var EmailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
            var phonePattern = /^[0-9]*$/;
            var ismember = '';

            if(!$('#nonmember').is(':checked')) //is member or not
            {
              // is member
              var ismember = 1;
              if(reg_no=='' ||  reg_no==null)
              {
                  $('#reg_no').addClass('error');
                  flag++;
              }
              else
              {
                  $('#reg_no').removeClass('error');
              }
            }
            else {
              // not a member
              var ismember = 0;
            }

            if(name=='' ||  name==null)
            {
                $('#name').addClass('error');
                flag++;
            }
            else
            {
                $('#name').removeClass('error');
            }

            if(comments=='' ||  comments==null)
            {
                $('#comments').addClass('error');
                flag++;
            }
            else
            {
                $('#comments').removeClass('error');
            }

            if(subject=='' ||  subject==null)
            {
                $('#subject').addClass('error');
                flag++;
            }
            else
            {
                $('#subject').removeClass('error');
            }


            if(!EmailPattern.test(email) || email=='' ||  email==null)
            {
                $('#email').addClass('error');
                flag++;
            }
            else
            {
                $('#email').removeClass('error');
            }

            // if(!phonePattern.test(mobile) || mobile=='' ||  mobile==null)
            // {
            //     $('#mobile').addClass('error');
            //     flag++;
            // }
            // else
            // {
            //     $('#mobile').removeClass('error');
            // }

            if(flag==0)
            {
            $('#contact_form').prop('disabled', true).html('Please Wait');
            var ajax = '11';
            var ajax_data = {
                    ajax : ajax,
                    email:email,
                    name : name,
                    reg_no : reg_no,
                    subject:subject,
                    comments:comments,
                    ismember:ismember,
                }
                $.ajax({
                    type: "POST",
                    url: host_url + '/process/ajax.php',
                    data: ajax_data,
                    success: function(result) {
                        $('#contact_form').prop('disabled', false).html('Submit');
                        if(result == 1)
                        {
                          $('#contact_form_msg').html('<span class="label label-success">Thank you for contacting us.</span>');
                          setTimeout(function(){ location.reload(); }, 3000);
                          // $('#name,#email,#reg_no,#comments').val('');
                          // $("#subject option").val('Select Subject');
                        }
                        else {
                          $('#contact_form_msg').html('<span class="label label-warning">Something went wrong.</span>');
                        }
                    },
                    error: function() {
                        $('#contact_form').prop('disabled', false).html('Submit');

                    }
                });
            }
            });
            /** Contact Form End*/
            /** User details update*/
            $('#address_update').on('click',function(){
                var add1 = $('#add1').val();
                var add2 = $('#add2').val();
                var city = $('#city').val();
                var pin = $('#pin').val();
                var state = $('#state').val();
                var sadd1 = $('#sadd1').val();
                var sadd2 = $('#sadd2').val();
                var scity = $('#scity').val();
                var spin = $('#spin').val();
                var sstate = $('#sstate').val();
                // var mobile = $('#mobile').val();

                var flag = 0;
                var EmailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
                var phonePattern = /^[0-9]*$/;
                var ismember = '';

                if(add1=='' ||  add1==null)
                {
                    $('#add1').addClass('nerror');
                    flag++;
                }
                else
                {
                    $('#add1').removeClass('nerror');
                }

                if(add2=='' ||  add2==null)
                {
                    $('#add2').addClass('nerror');
                    flag++;
                }
                else
                {
                    $('#add2').removeClass('nerror');
                }

                if(city=='' ||  city==null)
                {
                    $('#city').addClass('nerror');
                    flag++;
                }
                else
                {
                    $('#city').removeClass('nerror');
                }

                if(pin=='' ||  pin==null)
                {
                    $('#pin').addClass('nerror');
                    flag++;
                }
                else
                {
                    $('#pin').removeClass('nerror');
                }

                if(state=='' ||  state==null)
                {
                    $('#state').addClass('nerror');
                    flag++;
                }
                else
                {
                    $('#state').removeClass('nerror');
                }

                var address_use =  $('#address_use :selected').val();
                $('#sadd1,#sadd2,#scity,#spin,#sstate').removeClass('nerror');
                if(address_use==2)
                {
                  if(sadd1=='' ||  sadd1==null)
                  {
                      $('#sadd1').addClass('nerror');
                      flag++;
                  }
                  else
                  {
                      $('#sadd1').removeClass('nerror');
                  }

                  if(sadd2=='' ||  sadd2==null)
                  {
                      $('#sadd2').addClass('nerror');
                      flag++;
                  }
                  else
                  {
                      $('#sadd2').removeClass('nerror');
                  }

                  if(scity=='' ||  scity==null)
                  {
                      $('#scity').addClass('nerror');
                      flag++;
                  }
                  else
                  {
                      $('#scity').removeClass('nerror');
                  }

                  if(spin=='' ||  spin==null)
                  {
                      $('#spin').addClass('nerror');
                      flag++;
                  }
                  else
                  {
                      $('#spin').removeClass('nerror');
                  }

                  if(sstate=='' ||  sstate==null)
                  {
                      $('#sstate').addClass('nerror');
                      flag++;
                  }
                  else
                  {
                      $('#sstate').removeClass('nerror');
                  }
                }

                // if(!EmailPattern.test(email) || email=='' ||  email==null)
                // {
                //     $('#email').addClass('error');
                //     flag++;
                // }
                // else
                // {
                //     $('#email').removeClass('error');
                // }

                if(flag==0)
                {
                $('#address_update').prop('disabled', true).html('Please Wait');
                var ajax = '15';
                var ajax_data = {
                        ajax : ajax,
                        add1:add1,
                        add2:add2,
                        city:city,
                        pin:pin,
                        state:state,
                        sadd1:sadd1,
                        sadd2:sadd2,
                        scity:scity,
                        spin:spin,
                        sstate:sstate,
                        address_use:address_use
                    }
                    $.ajax({
                        type: "POST",
                        url: host_url + '/process/ajax.php',
                        data: ajax_data,
                        success: function(result) {
                            $('#address_update').prop('disabled', false).html('UPDATE');
                            if(result == 1)
                            {
                              $('#data_msg').html('<div class="alert alert-success" role="alert">Details updated successfully</div>');
                            }
                            else {
                              $('#data_msg').html('<div class="alert alert-danger" role="alert">Something went wrong</div>');
                            }

                        },
                        error: function() {
                            $('#address_update').prop('disabled', false).html('UPDATE');
                            $('#data_msg').html('<div class="alert alert-danger" role="alert">Something went wrong</div>');
                        }
                    });
                }
                });
                /** Update address End*/
            /** User mobile no update step 1*/
            $('#generate_otp').on('click',function(){

                var new_number = $('#new_number').val();
                var flag = 0;
                var phonePattern = /^[0-9]*$/;

                if(!phonePattern.test(new_number) || new_number=='' ||  new_number==null)
                {
                    $('#new_number').addClass('nerror');
                    flag++;
                }
                else
                {
                    $('#new_number').removeClass('nerror');
                }

                if(flag==0)
                {
                $('#generate_otp').prop('disabled', true).html('Please Wait');
                var ajax = '16';
                var ajax_data = {
                        ajax : ajax,
                        new_number:new_number
                    }
                    $.ajax({
                        type: "POST",
                        url: host_url + '/process/ajax.php',
                        data: ajax_data,
                        success: function(result) {
                            // if(result == 1)     /* uncomment */
                            if(1)
                            {
                              alert(result);
                              $('#generate_otp').addClass('hidden');
                              $('#update_number').removeClass('hidden');
                              $('#otp_div').removeClass('hidden');
                              $('#new_number').prop('readonly',true);
                            }
                            else {
                              $('#mobile_msg').html('Something went wrong');
                            }
                        },
                        error: function() {
                            $('#mobile_msg').html('Something went wrong');
                        }
                    });
                }
                });
                /** User mobile no update End*/
            /** User mobile no update step 2*/
            $('#update_number').on('click',function(){

                var otp_mobile_usr = $('#otp_mobile_usr').val();
                var flag = 0;
                var phonePattern = /^[0-9]*$/;

                if(otp_mobile_usr=='' ||  otp_mobile_usr==null)
                {
                    $('#otp_mobile_usr').addClass('nerror');
                    flag++;
                }
                else
                {
                    $('#otp_mobile_usr').removeClass('nerror');
                }

                if(flag==0)
                {
                $('#update_number').prop('disabled', true).html('Please Wait');
                var ajax = '17';
                var ajax_data = {
                        ajax : ajax,
                        otp_mobile_usr:otp_mobile_usr
                    };
                    $.ajax({
                        type: "POST",
                        url: host_url + '/process/ajax.php',
                        data: ajax_data,
                        success: function(result) {
                            $('#update_number').html('Proceed');
                            if(result == 1)
                            {
                              $('#mobile_msg').html('Mobile no updated successfully');
                              $('#update_number').unbind();
                            }
                            else if(result == 3){
                              $('#mobile_msg').html('OTP missmatch');
                            }
                            else {
                              $('#mobile_msg').html('Something went wrong');
                            }
                        },
                        error: function() {
                            $('#mobile_msg').html('Something went wrong');
                        }
                    });
                }
                });
                /** User mobile no update End*/
                /** User mail update step 1*/
                $('#generate_mail_otp').on('click',function(){

                    var new_mail = $('#new_mail').val();
                    var flag = 0;
                    var EmailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;

                    if(!EmailPattern.test(new_mail) || new_mail=='' ||  new_mail==null)
                    {
                        $('#new_mail').addClass('nerror');
                        flag++;
                    }
                    else
                    {
                        $('#new_mail').removeClass('nerror');
                    }

                    if(flag==0)
                    {
                    $('#generate_mail_otp').prop('disabled', true).html('Please Wait');
                    var ajax = '18';
                    var ajax_data = {
                            ajax : ajax,
                            new_mail:new_mail
                        }
                        $.ajax({
                            type: "POST",
                            url: host_url + '/process/ajax.php',
                            data: ajax_data,
                            success: function(result) {
                                // if(result == 1)     /* uncomment */
                                if(1)
                                {
                                  alert(result);
                                  $('#generate_mail_otp').addClass('hidden');
                                  $('#update_mail').removeClass('hidden');
                                  $('#otp_mail_div').removeClass('hidden');
                                  $('#new_mail').prop('readonly',true);
                                }
                                else {
                                  $('#mail_msg').html('Something went wrong');
                                }
                            },
                            error: function() {
                                $('#mail_msg').html('Something went wrong');
                            }
                        });
                    }
                    });
                    /** User mail update End*/
                /** User mail update step 2*/
                $('#update_mail').on('click',function(){

                    var otp_mail_usr = $('#otp_mail_usr').val();
                    var flag = 0;
                    var phonePattern = /^[0-9]*$/;

                    if(otp_mail_usr=='' ||  otp_mail_usr==null)
                    {
                        $('#otp_mail_usr').addClass('nerror');
                        flag++;
                    }
                    else
                    {
                        $('#otp_mail_usr').removeClass('nerror');
                    }

                    if(flag==0)
                    {
                    $('#update_mail').prop('disabled', true).html('Please Wait');
                    var ajax = '19';
                    var ajax_data = {
                            ajax : ajax,
                            otp_mail_usr:otp_mail_usr
                        };
                        $.ajax({
                            type: "POST",
                            url: host_url + '/process/ajax.php',
                            data: ajax_data,
                            success: function(result) {
                                $('#update_mail').html('Proceed');
                                if(result == 1)
                                {
                                  $('#mail_msg').html('Email id updated successfully');
                                  $('#update_mail').unbind();
                                }
                                else if(result == 3){
                                  $('#mail_msg').html('OTP missmatch');
                                }
                                else {
                                  $('#mail_msg').html('Something went wrong');
                                }
                            },
                            error: function() {
                                $('#mail_msg').html('Something went wrong');
                            }
                        });
                    }
                    });
                    /** User mail update End*/

                    /** ISC Login as member */
                    $('#isc_login_btn').on('click',function(){
                        var username = $('#username').val();
                        var password = $('#password').val();
                        var flag = 0;
                        if(username=='' ||  username==null)
                        {
                            $('#username').addClass('error');
                            flag++;
                        }
                        else
                        {
                            $('#username').removeClass('error');
                        }

                        if(password=='' ||  password==null)
                        {
                            $('#password').addClass('error');
                            flag++;
                        }
                        else
                        {
                            $('#password').removeClass('error');
                        }
                        if(flag==0)
                        {
                        var ajax = '1';
                        var ajax_data = {
                                    ajax : ajax,
                                password : password,
                                username : username,
                            }
                            $.ajax({
                                type: "POST",
                                url: host_url + '/process/ajax_isc.php',
                                data: ajax_data,
                                success: function(result) {
                                    if(result == 'Success')
                                    {
                                        window.location.href = "isc_dashboard.php";
                                    }
                                    else
                                    {
                                        $('#username').val('');
                                        $('#password').val('');
                                        $('#login_message').html('<span class="label label-danger">'+result+'</span>')
                                    }

                                },
                                error: function() {
                                    $('#username').val('');
                                    $('#password').val('');
                                    $('#login_message').html('<span class="label label-danger">Something went wrong</span>')
                                }
                            });
                        }
                        });
                    /** ISC Login as member End */

                    /** ISC Login as guest */
                    $('#isc_guest_login_btn').on('click',function(){

                        var ajax = '2';
                        var ajax_data = {
                                    ajax : ajax,
                            }
                            $.ajax({
                                type: "POST",
                                url: host_url + '/process/ajax_isc.php',
                                data: ajax_data,
                                success: function(result) {
                                    if(result == 'Success')
                                    {
                                        window.location.href = "isc_dashboard.php";
                                    }
                                    else
                                    {
                                        $('#username').val('');
                                        $('#password').val('');
                                        $('#login_message').html('<span class="label label-danger">'+result+'</span>')
                                    }

                                },
                                error: function() {
                                    $('#username').val('');
                                    $('#password').val('');
                                    $('#login_message').html('<span class="label label-danger">Something went wrong</span>')
                                }
                            });
                        });
                    /** ISC Login as guest End */
                    /** ISC Login as student */
                    $('#isc_student_login_btn').on('click',function(){

                        var ajax = '3';
                        var ajax_data = {
                                    ajax : ajax,
                            }
                            $.ajax({
                                type: "POST",
                                url: host_url + '/process/ajax_isc.php',
                                data: ajax_data,
                                success: function(result) {
                                    if(result == 'Success')
                                    {
                                        window.location.href = "isc_dashboard.php";
                                    }
                                    else
                                    {
                                        $('#username').val('');
                                        $('#password').val('');
                                        $('#login_message').html('<span class="label label-danger">'+result+'</span>')
                                    }

                                },
                                error: function() {
                                    $('#username').val('');
                                    $('#password').val('');
                                    $('#login_message').html('<span class="label label-danger">Something went wrong</span>')
                                }
                            });
                        });
                    /** ISC Login as student End */


     $(".menu-bar").click(function(){
        $(".nav-other").toggleClass("open");
    });
});
