<?php session_start(); ?>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-111181953-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-111181953-1');
</script>

<header id="header" class="inner-header">
 <div class="search">
   <!-- <script>
     (function() {
       var cx = '004231188845848405683:nh5utauzsnw';
       var gcse = document.createElement('script');
       gcse.type = 'text/javascript';
       gcse.async = true;
       gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
       var s = document.getElementsByTagName('script')[0];
       s.parentNode.insertBefore(gcse, s);
     })();
   </script>
   <gcse:search></gcse:search> -->
  <form id="cp-top-search">
   <input name="search" type="text" placeholder="Enter your search" required>
   <button type="submit"><i class="fa fa-search"></i></button>
 </form>
</div>
<div class="cp-top-bar">
  <div class="container">
   <div class="row">
     <div class="col-md-3">
      <div class="left-box name-title">
        <?php /*if(!empty($_SESSION['is_login']) && $_SESSION['is_login'] == 'true') { ?>
        Welcome <span><a href="details.php"><?php echo $_SESSION['name']; ?></a></span>
      <?php } */ ?>
      </div>
    </div>
    <div class="col-md-6 col-xs-9">
     <div class="left-box">

                        <marquee class="ticker">Whenever you write, whatever you write, never make the mistakes of assuming the audience is any less intelligent than you are. - Rod serling</marquee>
                      </div>
                    </div>
                    <div class="col-md-3 col-xs-3">
                     <div class="right-box linkbox">
                     <!--  <div class="cp-header-search-box">
                       <div class="burger">
                        <i class="fa fa-search"></i>
                      </div>
                    </div> -->
              		<!-- <div class="cp-contact">
                    <a href="javascript:void(0)">Ask Our Lawyer</a>
                  </div> -->
                  <div class="cp-contact">
                    <?php if(!empty($_SESSION['is_login']) && $_SESSION['is_login'] == 'true') { ?>
                    <!-- <a href="logout.php">Sign Out</a> -->
                          <div class="other-menu">
                            <a href="details.php"><?php echo $_SESSION['name']; ?>&nbsp;<i class="fa fa-angle-down" aria-hidden="true"></i></a>
                            <ul class="other-menu-ul">
                              <li><a href="dashboard.php">Your Dashboard</a></li>
                              <li><a href="details.php">Your Details</a></li>
                              <li><a href="logout.php">Sign Out</a></li>
                            </ul>
                          </div>
                    <?php  } else { ?>
                    <a href="login.php">Sign In</a>
                    <?php } ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="cp-navigation-section">

         <nav class="navbar navbar-inverse">

          <div class="container-fluid">
          <div class="navbar-header col-md-3 text-center">
            <button type="button" class="navbar-toggle " data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            <strong class="cp-logo-style-1"><a href="index.php"><img class="art-logo" src="images/swa_logo.png" alt="img"></a></strong>
          </div>
         <div class="collapse navbar-collapse" id="navbar">
          <ul id="nav" class="navbar-nav col-md-9">
            <li><a href="#">About Us</a>
              <ul class="submenu" >
                <li><a href="mission.php">Our Mission</a></li>
                <li><a href="mission.php#constitution">Our Constitution</a></li>
                <li>
                  <a href="team.php">People at SWA</a>
                  <ul class="inner_submenu">
                    <li><a href="team.php">Executive Committee</a></li>
                    <li><a href="team.php?tab=sc">Sub Committees</a></li>
                    <li><a href="team.php?tab=st">Staff</a></li>
                  </ul>
                </li>
                <li><a href="history.php">History of SWA</a></li>
                <!-- <li><a href="javascript:void(0)">Photo Gallery</a></li> -->
              </ul>
            </li>

            <li><a href="javascript:void(0)">Become A Member</a>
              <ul class="submenu">
                <li><a href="membership.php">SWA Membership</a></li>
                <li><a href="membership.php#membership-form">SWA Membership Form</a></li>
                <li><a href="faqs.php">Frequently Asked Questions</a></li>
              </ul>
            </li>
            <?php if(!empty($_SESSION['is_login']) && $_SESSION['is_login'] == 'true') { ?>
            <li><a href="javascript:void(0)">Register Your Work</a>
              <ul class="submenu">
               <li><a href="dashboard.php">Your Dashboard</a></li>
               <li><a href="register_script.php">Register Your Work</a></li>
               <li><a href="mycreation.php">Your Registered Work</a></li>
               <li><a href="details.php">Your details</a></li>
               <!-- <li><a href="javascript:void(0)">Renew Your Membership<br>(coming soon)</a></li> -->
               <li><a href="changepass.php">Change Password</a></li>
               <li><a href="contact.php">Technical Support</a></li>
             </ul>
           </li>
            <?php } else { ?>
            <li><a href="javascript:void(0)" data-toggle="modal" data-target="#myModal">Register Your Work</a></li>
            <?php } ?>
           <li><a href="#">ASK SWA</a>
            <ul class="submenu">
              <li><a href="lawyer.php">Ask Our Lawyer</a></li>
              <li><a href="dsc.php">Ask Our DSC</a>
                <ul class="inner_submenu">
                  <li><a href="dsc.php">About DSC</a></li>
                  <li><a href="dsc.php#guidelines">DSC Guidelines & Byelaws</a></li>
                  <li><a href="dsc.php#complaint">Filing A Complaint</a></li>
                  <li><a href="dsc.php#forms">DSC Complaint Form</a></li>
                  <li><a href="dsc.php#forms">DSC Undertaking Form</a></li>
                </ul>
              </li>
              <li><a href="faqs.php">Frequently Asked Questions</a></li>
            </ul>
          </li>
          <li><a href="contact.php">Contact Us</a></li>
        </ul>
        </div>
      </div>
    </nav>
  </div>
</header>
<nav class="nav-other">
  <div class="menu-bar">MORE</div>
  <ul class="navbar-side">
    <!-- <li class="drop-down"><a href="#">SWA RECOMMENDS</a>
      <ul class="fc-navbar">
        <li><a href="#">Welfare Schemes</a></li>
        <li><a href="#">Minimum Basic Contracts</a></li>
        <li><a href="#">Minimum Basic Rate Card</a></li>
        <li><a href="#">DO’s Don’ts for Writers</a></li>
        <li><a href="#">Writers’ Charter</a></li>
      </ul>
    </li> -->
    <li><a href="do.php">DO's & DON'Ts FOR WRITERS</a></li>
    <li><a href="writer.php">WRITER'S CHARTER</a></li>
    <li><a href="downloads.php">ALL DOWNLOADS</a></li>
    <!-- <li class="drop-down"><a href="#">DOWNLOADS</a>
      <ul class="fc-navbar">
        <li><a href="#">Constitution</a></li>
        <li><a href="#">DSC Byelaws</a></li>
        <li><a href="#">Membership Form</a></li>
        <li><a href="#">DSC Form</a></li>
        <li><a href="#">Minimum Basic Contract Films</a></li>
        <li><a href="#">Minimum Basic Contract TV & Digital</a></li>
        <li><a href="#">Minimum Basic Contract Lyrics</a></li>
        <li><a href="#">Minimum Basic Rate Card</a></li>
      </ul>
    </li> -->
    </ul>
</nav>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content project-details-popup">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      <div class="modal-header">
        <img class="header-img" src="images/pensil.jpg">
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-4">
            <h4>Not Signed In?</h4>
            <p><a href="login.php">Click here</a> to Sign In and register your work.</p>
          </div>
          <div class="col-md-4 bdr">
            <h4>Not Signed Up?</h4>
            <p><a href="email-reg.php">Click here</a>, if you're a first time user.</p>
          </div>
          <div class="col-md-4">
            <h4>Not a Member?</h4>
            <p>Only SWA Members can register their work. <a href="membership.php">Click here</a> to know more.</p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
<!-- changing title for now later change in each page -->
<script type="text/javascript">
document.title = 'Screenwriters Association';
</script>
