<?php session_start();
if(!empty($_SESSION['panel']['adm_email'])) {
  include("includes/db_config.php");

  if(!empty($_POST['reg_no']))
  {
    $reg_no = $_POST['reg_no'];
    $reg_no = reg_no_chk($reg_no);
  }
  else {
    header('Location: dashboard.php');
  }

  if(!empty($_POST['email']))
  {
    $email = $_POST['email'];
  }
  else {
    header('Location: dashboard.php');
  }

  if(!empty($_POST['mobile']))
  {
    $mobile = $_POST['mobile'];
  }
  else {
    header('Location: dashboard.php');
  }

  $sql_member = "UPDATE `fwa_members` SET `mobileno` = '".$mobile."' WHERE `reg_no` = '".$reg_no."'";
  if(mysqli_query($db, $sql_member))
  {
    $sql_user = "UPDATE `fwa_users` SET `email` = '".$email."' WHERE `reg_no` = '".$reg_no."'";
    if(mysqli_query($db, $sql_user))
    {
      $_SESSION['panel']['msg'] = "<span class='label label-success'>Updated successfully</span>";
    }
    else {
      $_SESSION['panel']['msg'] = "<span class='label label-danger'>Something went wrong</span>";
    }
  }
  else {
    $_SESSION['panel']['msg'] = "<span class='label label-danger'>Something went wrong</span>";
  }
}
else {
  header('Location: index.php');
}

header('Location: dashboard.php');


function reg_no_chk($reg_no)
{
	$length = strlen($reg_no);

	switch ($length) {
		case 1:
		$reg_no = '00000'.$reg_no;
		break;

		case 2:
		$reg_no = '0000'.$reg_no;
		break;

		case 3:
		$reg_no = '000'.$reg_no;
		break;

		case 4:
		$reg_no = '00'.$reg_no;
		break;

		case 5:
		$reg_no = '0'.$reg_no;
		break;

	}
	return $reg_no;
}
