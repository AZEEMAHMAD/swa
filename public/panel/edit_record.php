<?php session_start();
//var_dump(!empty($_SESSION['panel']['adm_email']));die;
if(!empty($_SESSION['panel']['adm_email'])) {
  include("includes/db_config.php");
  ?>
  <!DOCTYPE html>
  <!--
  This is a starter template page. Use this page to start your new project from
  scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SWA Display Panel</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.5 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
  page. However, you can choose any other skin. Make sure you
  apply the skin class to the body tag so the changes take effect.
-->
<link rel="stylesheet" href="dist/css/skins/skin-blue.min.css">
<link rel="stylesheet" href="plugins/datatables/jquery.dataTables.min.css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <!-- Main Header -->
    <?php include_once 'includes/header.php'; ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

      <!-- sidebar: style can be found in sidebar.less -->
      <?php include_once 'includes/sidebar.php'; ?>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Members Data
        </h1>
      </section>
      <?php
      if(!empty($_REQUEST['reg_no']))
      {
        $reg_no = $_REQUEST['reg_no'];
      }
      else {
        header('Location: dashboard.php');
      }

      $sql = "SELECT usr.reg_no, usr.pass,usr.email, usr.type_id, usr.membership_expiry_date, ty.type, mem.name, mem.mobileno
      FROM `fwa_users` usr
      INNER JOIN `fwa_members` mem ON mem.reg_no = usr.reg_no
      INNER JOIN `mem_type` ty ON  usr.type_id = ty.type_id
      WHERE usr.reg_no = ".$reg_no;
      $result = mysqli_query($db, $sql);
      $row = mysqli_fetch_assoc($result);
      ?>

      <!-- Main content -->
      <section class="content">
        <!-- Your Page Content Here -->
        <div class="row">
          <div class="col-md-12">
              <form action="edit_record_submit.php" method="post">
                <div class="form-group">
                  <label>Membership Number</label>
                  <input type="text" readonly class="form-control" id="reg_no" name="reg_no" placeholder="Membership Number" value="<?php echo $row['reg_no']; ?>">
                </div>
                <div class="form-group">
                  <label>Name</label>
                  <input type="name" readonly class="form-control" id="name" placeholder="Name" value="<?php echo $row['name']; ?>">
                </div>
                <div class="form-group">
                  <label>Email address</label>
                  <input type="email" class="form-control" id="email" name="email" placeholder="Email" required  value="<?php echo $row['email']; ?>">
                </div>
                <div class="form-group">
                  <label>Mobile No</label>
                  <input type="tel" class="form-control" id="mobile" name="mobile" placeholder="Mobile Number" required  value="<?php echo $row['mobileno']; ?>">
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
              </form>
          </div>
        </div>
      </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include_once 'includes/footer.php'; ?>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Create the tabs -->
    </aside><!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
  </div><!-- ./wrapper -->

  <!-- REQUIRED JS SCRIPTS -->

  <!-- jQuery 2.1.4 -->
  <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
  <!-- Bootstrap 3.3.5 -->
  <script src="bootstrap/js/bootstrap.min.js"></script>
  <!-- AdminLTE App -->
  <script src="dist/js/app.min.js"></script>
  <script src="plugins/datatables/jquery.dataTables.min.js"></script>
  <script>
  $(document).ready( function () {

  });
  </script>
</body>
</html>
<?php
}
else {
  header('Location: index.php');
}
?>
