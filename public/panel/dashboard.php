<?php session_start();
//var_dump(!empty($_SESSION['panel']['adm_email']));die;
if(!empty($_SESSION['panel']['adm_email'])) {
  include("includes/db_config.php");
?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SWA Display Panel</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link rel="stylesheet" href="dist/css/skins/skin-blue.min.css">
    <link rel="stylesheet" href="plugins/datatables/jquery.dataTables.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <!-- Main Header -->
      <?php include_once 'includes/header.php'; ?>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <?php include_once 'includes/sidebar.php'; ?>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Members Data
          </h1>
        </section>
        <?php
        $limit = 500;

        $sql = "SELECT usr.reg_no, usr.pass,usr.email, usr.type_id, usr.membership_expiry_date, ty.type, mem.name, mem.mobileno
                      FROM `fwa_users` usr
                      INNER JOIN `fwa_members` mem ON mem.reg_no = usr.reg_no
                      INNER JOIN `mem_type` ty ON  usr.type_id = ty.type_id ";

        if(empty($_REQUEST['page']) && empty($_REQUEST['search']))
        {
          $page = 1;
          $offset = (($page-1) * $limit);

          $sql_count = $sql;
          $result_count = mysqli_query($db, $sql_count);
          $totalrecords = mysqli_num_rows($result_count);

          $sql = $sql . "ORDER BY `reg_no` DESC LIMIT ".$limit." OFFSET ".$offset;
          // var_dump($sql);die;
          $result = mysqli_query($db, $sql);
          // var_dump($totalrecords);die;
          $totalpage = ceil($totalrecords / $limit);
        }
        else if(!empty($_REQUEST['page']) && empty($_REQUEST['search']) ) {
          $page = $_REQUEST['page'];
          $offset = (($page-1) * $limit);

          $sql_count = $sql;

          $result_count = mysqli_query($db, $sql_count);
          $totalrecords = mysqli_num_rows($result_count);

          $sql = $sql . "ORDER BY `reg_no` DESC LIMIT ".$limit." OFFSET ".$offset;

          $result = mysqli_query($db, $sql);
          // var_dump($totalrecords);die;
          $totalpage = ceil($totalrecords / $limit);
        }
        else if(empty($_REQUEST['page']) && !empty($_REQUEST['search']) ) {

          $search = $_REQUEST['search'];
          $sql_count = $sql."WHERE usr.reg_no LIKE '%".$search."%' OR usr.email LIKE '%".$search."%' OR mem.name LIKE '%".$search."%'";
          $result_count = mysqli_query($db, $sql_count);
          $totalrecords = mysqli_num_rows($result_count);

          $page = 1;

          $offset = (($page-1) * $limit);
          $sql = $sql . "WHERE usr.reg_no LIKE '%".$search."%' OR usr.email LIKE '%".$search."%' OR mem.name LIKE '%".$search."%'
                        ORDER BY `reg_no` DESC LIMIT ".$limit." OFFSET ".$offset;

          $result = mysqli_query($db, $sql);

          // var_dump($totalrecords);die;
          $totalpage = ceil($totalrecords / $limit);
        }
        else {
          $page = $_REQUEST['page'];
          $search = $_REQUEST['search'];
          $offset = (($page-1) * $limit);

          $sql_count = $sql."WHERE usr.reg_no LIKE '%".$search."%' OR usr.email LIKE '%".$search."%' OR mem.name LIKE '%".$search."%'";

          $result_count = mysqli_query($db, $sql_count);
          $totalrecords = mysqli_num_rows($result_count);


          $sql = $sql . "WHERE usr.reg_no LIKE '%".$search."%' OR usr.email LIKE '%".$search."%' OR mem.name LIKE '%".$search."%'
                        ORDER BY `reg_no` DESC LIMIT ".$limit." OFFSET ".$offset;



          $result = mysqli_query($db, $sql);
          // var_dump($totalrecords);die;
          $totalpage = ceil($totalrecords / $limit);
        }
        // var_dump($totalpage);die;
        ?>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-12">
              <?php if(isset($_SESSION['panel']['msg'])) {
                echo $_SESSION['panel']['msg'];
                unset($_SESSION['panel']['msg']);
              } ?>
            </div>
          </div>
          <!-- Your Page Content Here -->
          <?php if(!empty($_REQUEST['search'])) { $s = $_REQUEST['search']; } else { $s = ''; } ?>
          <div class="row">
            <div class="col-md-3 pull-left">
              <div class="input-group">
                <span class="input-group-btn">
                <input type="text" class="form-control" id="search_val" value="<?php echo $s; ?>" placeholder="Search using membership no or name or email id">
                  <button class="btn btn-secondary" id="search_btn"  type="button">Search</button>
                </span>
              </div>
              <div class="input-group">
                <button type="button" id="reset_btn" class="btn btn-secondary input-control">Reset Results</button>
              </div>
            </div>
            <div class="col-md-12">
              <table class="table table-condensed display">
                <thead>
                  <tr>
                    <th>Membership No</th>
                    <th>Name</th>
                    <th>Password</th>
                    <th>Email</th>
                    <th>Mobile No</th>
                    <th>Expiry Date</th>
                    <th>Membership Type</th>
                    <th>Edit</th>
                  </tr>
                </thead>
                <tbody>
                  <?php while ($row = mysqli_fetch_array($result)) { ?>
                    <tr>
                        <td><?php echo $row['reg_no']; ?></td>
                        <td><?php echo $row['name']; ?></td>
                        <td><?php echo $row['pass']; ?></td>
                        <td><?php echo $row['email']; ?></td>
                        <td><?php echo $row['mobileno']; ?></td>
                        <td><?php echo date("d-m-Y", strtotime($row['membership_expiry_date']));  ?></td>
                        <td><?php echo $row['type']; ?></td>
                        <td><a href="edit_record.php?reg_no=<?php echo $row['reg_no']; ?>">Edit</a></td>
                    </tr>
                  <?php } ?>

                </tbody>
              </table>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <nav aria-label="Page navigation example">
              <ul class="pagination">
                <?php if(empty($_REQUEST['search'])){
                 for($i=1;$i<=$totalpage;$i++) { ?>
                <li class="page-item <?php if($page == $i){echo 'active'; } ?>"><a class="page-link" href="dashboard.php?page=<?php echo $i; ?>"><?php echo $i; ?></a></li>
              <?php } } else {
                $search = $_REQUEST['search'];
                for($i=1;$i<=$totalpage;$i++) {
                ?>
                <li class="page-item <?php if($page == $i){echo 'active'; } ?>"><a class="page-link" href="dashboard.php?page=<?php echo $i; ?>&search=<?php echo  $search; ?>"><?php echo $i; ?></a></li>
              <?php } } ?>
              </ul>
            </nav>
            </div>
          </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <!-- Main Footer -->
      <?php include_once 'includes/footer.php'; ?>

      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
      </aside><!-- /.control-sidebar -->
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script>
      $(document).ready( function () {
        // $('#table_id').DataTable({
        //   "pageLength": 100
        // });

        $('#search_btn').on('click', function(){
          var search_val = $('#search_val').val();
          search_val = search_val.trim();
          if(search_val!= '' && search_val!=null)
          {
            window.location.href="dashboard.php?search="+search_val;
          }
        });
        $('#reset_btn').on('click', function(){
            window.location.href="dashboard.php";
        });

      });
    </script>
    <!-- Optionally, you can add Slimscroll and FastClick plugins.
         Both of these plugins are recommended to enhance the
         user experience. Slimscroll is required when using the
         fixed layout. -->
  </body>
</html>
<?php
}
else {
  header('Location: index.php');
}
?>
