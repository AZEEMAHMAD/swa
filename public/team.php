<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/x-icon" href=""/>
	<title>People at SWA</title>
	<link href="css/custom.css" rel="stylesheet" type="text/css">

	<link href="css/color.css" rel="stylesheet" type="text/css">
	<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="css/teampage.css" rel="stylesheet" type="text/css">
	<link href="css/responsive.css" rel="stylesheet" type="text/css">
	<link href="css/jquery.bxslider.css" rel="stylesheet" type="text/css">
	<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<!-- <link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen"/> -->
	<link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>

  </style>
</head>
<body class="inner-page">
	<div id="wrapper">
		<?php include_once('header.php'); ?>
		<div id="cp-content-wrap">
			<!-- Banner -->
			<div class="banner_inner">
				<img src="images/bg.jpg">
			</div>
			<!-- End of Banner -->
			<div class="cp_our-story-section tab">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<ul class="tabs">
								<li><a id="ec" href="javascript:void(0)">Executive Committee</a></li>
								<li><a id="sc" href="javascript:void(0)">Sub Committee</a></li>
								<li><a id="st" href="javascript:void(0)">Staff</a></li>
							</ul> <!-- / tabs -->
						</div>
					</div>
				</div>
				<div class="tab_content">
					<div class="tabs_item">
						<div class="container">
							<div class="row">
								<div class="col-md-12">
									<!--exe start-->
									<div id="ExecutiveComm" class="mg-40">
										<h5>EXECUTIVE COMMITTEE</h5>
										<p class="text-justify">SWA’s Executive Committee, comprising of 31 members (including 7 Office Bearers, 21 regular members and 3 associate members); administers the affairs of the Association. It is duly elected biennially in the General Body Election Meeting. The Executive Committee meets once a month. However, emergency meetings can be held whenever necessary.</p>
										<h5>OFFICE BEARERS</h5>
										<p class="text-justify">The 7 Officer Bearers oversee the functioning of the EC, helping it take decisions effectively and efficiently and are responsible for implementing the objectives of the Association.</p>
										<p class="text-justify">The <strong>PRESIDENT</strong> presides over the meetings of the Executive Committee.<br>
											The two  <strong>VICE PRESIDENTS</strong> fulfill the duties of the President in his/her absence.<br>
											The <strong>GENERAL SECRETARY</strong> serves as the Chief Executive Officer, on honorary basis, of the Association and is responsible for the day-to-day functioning of its office.
											The two <strong>JOINT SECRETARIES</strong> assist the General Secretary in the performance of his/her tasks and duties, and in his/her absence.<br>
											The <strong>TREASURER</strong> is responsible for maintaining due accounts of the finances of the Association.</p>

											<!-- Starts Executive -->
											<div id="Executiveteams" class="centerdiv" >
											<div class="portfolio exe"  data-toggle="modal" data-target="#preeti" >
												<div class="portfolio-wrapper">
													<img src="img/exe/PreetiMamgain.jpg" alt="" />
													<div class="label">
														<div class="label-text">
															<a class="text-title">Preeti Mamgain</a>
															<span class="text-category">PRESIDENT</span>
														</div>
														<div class="label-bg"></div>
													</div>
												</div>
											</div>
											<div class="portfolio exe" >
												<div class="portfolio-wrapper">
													<img src="img/exe/ZamanHabib.jpg" alt="" />
													<div class="label">
														<div class="label-text">
															<a class="text-title">Zaman Habib</a>
															<span class="text-category">GENERAL SECRETARY</span>
														</div>
														<div class="label-bg"></div>
													</div>
												</div>
											</div>
											<div class="portfolio exe" >
												<div class="portfolio-wrapper">
													<img src="img/exe/DhananjayKumar.jpg" alt="" />
													<div class="label">
														<div class="label-text">
															<a class="text-title">Dhananjay Kumar</a>
															<span class="text-category">TREASURER</span>
														</div>
														<div class="label-bg"></div>
													</div>
												</div>
											</div>
											<div class="portfolio exe" >
												<div class="portfolio-wrapper">
													<img src="img/exe/DanishJaved.jpg" alt="" />
													<div class="label">
														<div class="label-text">
															<a class="text-title">Danish Javed</a>
															<span class="text-category">VICE PRESIDENT</span>
														</div>
														<div class="label-bg"></div>
													</div>
												</div>
											</div>
											<div class="portfolio exe" >
												<div class="portfolio-wrapper">
													<img src="img/exe/VinodRangnath.jpg" alt="" />
													<div class="label">
														<div class="label-text">
															<a class="text-title">Vinod Ranganath</a>
															<span class="text-category">VICE PRESIDENT</span>
														</div>
														<div class="label-bg"></div>
													</div>
												</div>
											</div>
											<div class="portfolio exe" >
												<div class="portfolio-wrapper">
													<img src="img/exe/ManishaKorde.jpg" alt="" />
													<div class="label">
														<div class="label-text">
															<a class="text-title">Manisha Korde</a>
															<span class="text-category">JOINT SECRETARY</span>
														</div>
														<div class="label-bg"></div>
													</div>
												</div>
											</div>
											<div class="portfolio exe" >
												<div class="portfolio-wrapper">
													<img src="img/exe/AbhijeetDeshpande.jpg" alt="" />
													<div class="label">
														<div class="label-text">
															<a class="text-title">Abhijeet Deshpande</a>
															<span class="text-category">JOINT SECRETARY</span>
														</div>
														<div class="label-bg"></div>
													</div>
												</div>
											</div>
											<div class="portfolio exe" >
												<div class="portfolio-wrapper">
													<img src="img/exe/JaleesSherwani.jpg" alt="" />
													<div class="label">
														<div class="label-text">
															<a class="text-title">Jalees Sherwani</a>
															<span class="text-category">Regular EC Member</span>
														</div>
														<div class="label-bg"></div>
													</div>
												</div>
											</div>
											<div class="portfolio exe" >
												<div class="portfolio-wrapper">
													<img src="img/exe/RobinBhatt.jpg" alt="" />
													<div class="label">
														<div class="label-text">
															<a class="text-title">Robin Bhatt</a>
															<span class="text-category">Regular EC Member</span>
														</div>
														<div class="label-bg"></div>
													</div>
												</div>
											</div>
											<div class="portfolio exe" >
												<div class="portfolio-wrapper">
													<img src="img/exe/AnjumRajabali.jpg" alt="" />
													<div class="label">
														<div class="label-text">
															<a class="text-title">Anjum Rajabali</a>
															<span class="text-category">Regular EC Member</span>
														</div>
														<div class="label-bg"></div>
													</div>
												</div>
											</div>
											<div class="portfolio exe" >
												<div class="portfolio-wrapper">
													<img src="img/exe/RajeshDubey.jpg" alt="" />
													<div class="label">
														<div class="label-text">
															<a class="text-title">Rajesh Dubey</a>
															<span class="text-category">Regular EC Member</span>
														</div>
														<div class="label-bg"></div>
													</div>
												</div>
											</div>
											<div class="portfolio exe" >
												<div class="portfolio-wrapper">
													<img src="img/exe/SwanandKirkire.jpg" alt="" />
													<div class="label">
														<div class="label-text">
															<a class="text-title">Swanand Kirkirey</a>
															<span class="text-category">Regular EC Member</span>
														</div>
														<div class="label-bg"></div>
													</div>
												</div>
											</div>
											<div class="portfolio exe" >
												<div class="portfolio-wrapper">
													<img src="img/exe/SANJAYCHHEL.jpg" alt="" />
													<div class="label">
														<div class="label-text">
															<a class="text-title">SANJAY CHHEL</a>
															<span class="text-category">Regular EC Member</span>
														</div>
														<div class="label-bg"></div>
													</div>
												</div>
											</div>
											<div class="portfolio exe" >
												<div class="portfolio-wrapper">
													<img src="img/exe/ShridharRaghvan.jpg" alt="" />
													<div class="label">
														<div class="label-text">
															<a class="text-title">Shridhar Raghavan</a>
															<span class="text-category">Regular EC Member</span>
														</div>
														<div class="label-bg"></div>
													</div>
												</div>
											</div>
											<div class="portfolio exe" >
												<div class="portfolio-wrapper">
													<img src="img/exe/SanjayChouhan.jpg" alt="" />
													<div class="label">
														<div class="label-text">
															<a class="text-title">Sanjay Chouhan</a>
															<span class="text-category">Regular EC Member</span>
														</div>
														<div class="label-bg"></div>
													</div>
												</div>
											</div>
											<!-- <div class="portfolio exe" >
												<div class="portfolio-wrapper">
													<img src="img/exe/MeghnaGulzar.jpg" alt="" />
													<div class="label">
														<div class="label-text">
															<a class="text-title">Meghna Gulzary</a>
														</div>
														<div class="label-bg"></div>
													</div>
												</div>
											</div> -->
											<div class="portfolio exe" >
												<div class="portfolio-wrapper">
													<img src="img/exe/SatyamTripathi.jpg" alt="" />
													<div class="label">
														<div class="label-text">
															<a class="text-title">Satyam Tripathy</a>
															<span class="text-category">Regular EC Member</span>
														</div>
														<div class="label-bg"></div>
													</div>
												</div>
											</div>
											<!-- <div class="portfolio exe" >
												<div class="portfolio-wrapper">
													<img src="img/exe/SanjaySharma.jpg" alt="" />
													<div class="label">
														<div class="label-text">
															<a class="text-title">Sanjay Sharma</a>
														</div>
														<div class="label-bg"></div>
													</div>
												</div>
											</div> -->
											<div class="portfolio exe" >
												<div class="portfolio-wrapper">
													<img src="img/exe/Jyoti-Kapoor.jpg" alt="" />
													<div class="label">
														<div class="label-text">
															<a class="text-title">Jyoti Kapoor</a>
															<span class="text-category">Regular EC Member</span>
														</div>
														<div class="label-bg"></div>
													</div>
												</div>
											</div>
											<div class="portfolio exe" >
												<div class="portfolio-wrapper">
													<img src="img/exe/SachinDarekar.jpg" alt="" />
													<div class="label">
														<div class="label-text">
															<a class="text-title">Sachin Darekar</a>
															<span class="text-category">Regular EC Member</span>
														</div>
														<div class="label-bg"></div>
													</div>
												</div>
											</div>
											<div class="portfolio exe" >
												<div class="portfolio-wrapper">
													<img src="img/exe/saumyajoshi.jpg" alt="" />
													<div class="label">
														<div class="label-text">
															<a class="text-title">Saumya Joshi</a>
															<span class="text-category">Regular EC Member</span>
														</div>
														<div class="label-bg"></div>
													</div>
												</div>
											</div>
											<div class="portfolio exe" >
												<div class="portfolio-wrapper">
													<img src="img/exe/SunilSalgia.jpg" alt="" />
													<div class="label">
														<div class="label-text">
															<a class="text-title">Sunil Salgia</a>
															<span class="text-category">Regular EC Member</span>
														</div>
														<div class="label-bg"></div>
													</div>
												</div>
											</div>
											<div class="portfolio exe" >
												<div class="portfolio-wrapper">
													<img src="img/exe/ManojHansraj.jpg" alt="" />
													<div class="label">
														<div class="label-text">
															<a class="text-title">Manoj Hansraj</a>
															<span class="text-category">Regular EC Member</span>
														</div>
														<div class="label-bg"></div>
													</div>
												</div>
											</div>
											<div class="portfolio exe" >
												<div class="portfolio-wrapper">
													<img src="img/exe/AtikaChouhan.jpg" alt="" />
													<div class="label">
														<div class="label-text">
															<a class="text-title">Atika Chohan</a>
															<span class="text-category">Regular EC Member</span>
														</div>
														<div class="label-bg"></div>
													</div>
												</div>
											</div>
											<!-- <div class="portfolio exe" >
												<div class="portfolio-wrapper">
													<img src="img/exe/RadhikaBorkar.jpg" alt="" />
													<div class="label">
														<div class="label-text">
															<a class="text-title">Radhika Borkar</a>
														</div>
														<div class="label-bg"></div>
													</div>
												</div>
											</div> -->
											<div class="portfolio exe" >
												<div class="portfolio-wrapper">
													<img src="img/exe/MitaliMahajan.jpg" alt="" />
													<div class="label">
														<div class="label-text">
															<a class="text-title">Mitali Mahajan</a>
															<span class="text-category">Regular EC Member</span>
														</div>
														<div class="label-bg"></div>
													</div>
												</div>
											</div>
											<div class="portfolio exe" >
												<div class="portfolio-wrapper">
													<img src="img/exe/ShahabAllahabadi.jpg" alt="" />
													<div class="label">
														<div class="label-text">
															<a class="text-title">Shahab Allahabadi</a>
															<span class="text-category">Regular EC Member</span>
														</div>
														<div class="label-bg"></div>
													</div>
												</div>
											</div>
											<div class="portfolio exe" >
												<div class="portfolio-wrapper">
													<img src="img/exe/Debashish.jpg" alt="" />
													<div class="label">
														<div class="label-text">
															<a class="text-title">Debashish Irengbam</a>
															<span class="text-category">Regular EC Member</span>
														</div>
														<div class="label-bg"></div>
													</div>
												</div>
											</div>
											<div class="portfolio exe" >
												<div class="portfolio-wrapper">
													<img src="img/exe/Pooja Varma.jpg" style="max-width: 118px;max-height: 112px;" alt="" />
													<div class="label">
														<div class="label-text">
															<a class="text-title">Pooja Varma</a>
															<span class="text-category">Regular EC Member</span>
														</div>
														<div class="label-bg"></div>
													</div>
												</div>
											</div>
											<div class="portfolio exe" >
												<div class="portfolio-wrapper">
													<img src="img/exe/AashishPawaskar.jpg" alt="" />
													<div class="label">
														<div class="label-text">
															<a class="text-title">Aashish Pawaskary</a>
															<span class="text-category">Associate EC Member</span>
														</div>
														<div class="label-bg"></div>
													</div>
												</div>
											</div>
											<div class="portfolio exe" >
												<div class="portfolio-wrapper">
													<img src="img/exe/KalyaniPandit.jpg" alt="" />
													<div class="label">
														<div class="label-text">
															<a class="text-title">Kalyani Pandit</a>
															<span class="text-category">Associate EC Member</span>
														</div>
														<div class="label-bg"></div>
													</div>
												</div>
											</div>
											<div class="portfolio exe" >
												<div class="portfolio-wrapper">
													<img src="img/exe/ParakhChouhan.jpg" alt="" />
													<div class="label">
														<div class="label-text">
															<a class="text-title">Parakh Chouhan</a>
															<span class="text-category">Associate EC Member</span>
														</div>
														<div class="label-bg"></div>
													</div>
												</div>
											</div>
											</div>

											<!-- End team -->

										</div>
									</div>
								</div>
							</div>
							<!--exe end here-->
						</div>
						<div class="tabs_item">
							<div class="container">
								<div class="row">
									<div class="col-md-12">
										<div class="cp-acticle-box mg-40">
											<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
												<h5>SUB-COMMITTEES</h5>
												<p class="text-justify">The Executive Committee appoints various sub-committees for the purpose of fulfilling the aims and objectives of SWA, which periodically report to the Executive Committee. The various permanent sub-committees are: </p>
												<!--content start here-->
												<ul id="filters" class="clearfix">
													<!-- <li><span class="filter" data-filter=".exe">Core Members</span></li> -->
													<li><span class="filter cat active" data-filter="dsc">DSC</span></li>
													<li><span class="filter cat" data-filter="wel">Welfare</span></li>
													<li><span class="filter cat" data-filter="mvc"> Membership-Verification</span></li>
													<li><span class="filter cat" data-filter="legal">Legal</span></li>
													<!-- <li><span class="filter cat" data-filter="cc">Constitution (Bye-laws)</span></li> -->
													<li><span class="filter cat" data-filter="ev">Event</span></li>
													<!-- <li><span class="filter cat" data-filter="dscbible">DSC Bible</span></li> -->
													<li><span class="filter cat" data-filter="media">Media</span></li>
													<li><span class="filter cat" data-filter="mbc">Film – MBC</span></li>
													<li><span class="filter cat" data-filter="tvmbc"> TV – MBC</span></li>
													<li><span class="filter cat" data-filter="lmbc">Lyricist - MBC</span></li>
													<li><span class="filter cat" data-filter="web">Website</span></li>
												</ul>
												<div id="portfoliolist">

													<!--dsc start here-->
													<p class="text-justify portfolio dsc">
														The Dispute Settlement Sub-Committee is a QUASI-LEGAL body set up by the Screenwriters Association for the sole purpose of MEDIATION and CONCILIATION between its members and members of other craft associations, including producers/employers.
													</p>

															<div id="subteams" class="centerdiv" >
													<div class="portfolio dsc" data-cat="dsc">
														<div class="portfolio-wrapper">
															<img src="img/dsc/VinodRangnath.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Vinod Ranganath</a>
																	<span class="text-category">Chairperson</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio dsc" data-cat="dsc">
														<div class="portfolio-wrapper">
															<img src="img/dsc/Jyoti-Kapoor.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Jyoti Kapoor</a>
																	<span class="text-category">Convener</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio dsc" data-cat="dsc">
														<div class="portfolio-wrapper">
															<img src="img/dsc/RajeshDubey.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Rajesh Dubey</a>
																	<span class="text-category">MEMBER</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio dsc" data-cat="dsc">
														<div class="portfolio-wrapper">
															<img src="img/dsc/DanishJaved.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Danish Javed</a>
																	<span class="text-category">MEMBER</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio dsc" data-cat="dsc">
														<div class="portfolio-wrapper">
															<img src="img/dsc/DhananjayKumar.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Dhananjay Kumar</a>
																	<span class="text-category">MEMBER</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio dsc" data-cat="dsc">
														<div class="portfolio-wrapper">
															<img src="img/dsc/AashishPawaskar.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Aashish Pawaskar</a>
																	<span class="text-category">MEMBER</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio dsc" data-cat="dsc">
														<div class="portfolio-wrapper">
															<img src="img/dsc/AtikaChouhan.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Atika Chohan</a>
																	<span class="text-category">MEMBER</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio dsc" data-cat="dsc">
														<div class="portfolio-wrapper">
															<img src="img/dsc/saumyajoshi.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Saumya Joshi</a>
																	<span class="text-category">MEMBER</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio dsc" data-cat="dsc">
														<div class="portfolio-wrapper">
															<img src="img/dsc/SunilSalgia.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Sunil Salgia</a>
																	<span class="text-category">MEMBER</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio dsc" data-cat="dsc">
														<div class="portfolio-wrapper">
															<img src="img/dsc/ParakhChouhan.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Parakh Chouhan</a>
																	<span class="text-category">MEMBER</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>

													<div class="portfolio dsc" data-cat="dsc">
														<div class="portfolio-wrapper">
															<img src="img/dsc/PreetiMamgain.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Preeti Mamgain</a>
																	<span class="text-category">EX-OFFICIO</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio dsc" data-cat="dsc">
														<div class="portfolio-wrapper">
															<img src="img/dsc/ZamanHabib.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Zaman Habib</a>
																	<span class="text-category">EX-OFFICIO</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<!--dsc end here-->
													<!--welfare start here-->
													<p class="text-justify portfolio wel">
														The Welfare Sub-Committee administers and executes the welfare schemes run by the Association. To run welfare activities for needy and elderly SWA members (Life, Regular and Associate only) in the form of medial-aid, pension and scholarships (to their children), is an ancillary function of SWA.
													</p>
													<div class="portfolio wel" data-cat="wel">
														<div class="portfolio-wrapper">
															<img src="img/wel/RobinBhatt.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Robin Bhatt</a>
																	<span class="text-category">Chairperson</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio wel" data-cat="wel">
														<div class="portfolio-wrapper">
															<img src="img/wel/JaleesSherwani.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Jalees Sherwani</a>
																	<span class="text-category">MEMBER</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio wel" data-cat="wel">
														<div class="portfolio-wrapper">
															<img src="img/wel/DhananjayKumar.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Dhananjay Kumar</a>
																	<span class="text-category">MEMBER</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio wel" data-cat="wel">
														<div class="portfolio-wrapper">
															<img src="img/wel/SanjayChouhan.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Sanjay Chouhan</a>
																	<span class="text-category">Co-Chairperson</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio wel" data-cat="wel">
														<div class="portfolio-wrapper">
															<img src="img/wel/SatyamTripathi.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Satyam Tripathy</a>
																	<span class="text-category">MEMBER</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio wel" data-cat="wel">
														<div class="portfolio-wrapper">
															<img src="img/wel/SachinDarekar.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Sachin Darekar</a>
																	<span class="text-category">MEMBER</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio wel" data-cat="wel">
														<div class="portfolio-wrapper">
															<img src="img/wel/ShahabAllahabadi.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Shahab Allahabadi</a>
																	<span class="text-category">MEMBER</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio wel" data-cat="wel">
														<div class="portfolio-wrapper">
															<img src="img/wel/ManojHansraj.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Manoj Hansraj</a>
																	<span class="text-category">MEMBER</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio wel" data-cat="wel">
														<div class="portfolio-wrapper">
															<img src="img/wel/PreetiMamgain.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Preeti Mamgain</a>
																	<span class="text-category">EX-OFFICIO</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio wel" data-cat="wel">
														<div class="portfolio-wrapper">
															<img src="img/wel/ZamanHabib.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Zaman Habib</a>
																	<span class="text-category">EX-OFFICIO</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<!--welfare end here-->
													<!--mvc start here-->
													<p class="text-justify portfolio mvc">
														The Membership Verification Sub-Committee, set-up by the Executive Committee of the Screenwriters Association, scrutinizes and verifies the applications for Life, Regular and Associate membership.
													</p>
													<div class="portfolio mvc" data-cat="mvc">
														<div class="portfolio-wrapper">
															<img src="img/mvc/JaleesSherwani.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">JaleesSherwani</a>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio mvc" data-cat="mvc">
														<div class="portfolio-wrapper">
															<img src="img/mvc/ManojHansraj.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Manoj Hansraj</a>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio mvc" data-cat="mvc">
														<div class="portfolio-wrapper">
															<img src="img/mvc/ShahabAllahabadi.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Shahab Allahabadi</a>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<!--mvc end here-->
													<!--legal start here-->
													<p class="text-justify portfolio legal">
														SWA’s Legal Sub-Committee secures representation of its members on delegations, commissions, committees etc. set-up by the government or other bodies, including the court of law, with issues concerning screenwriters or screenwriting. It has also engaged a Copyright expert lawyer (SWA’s Legal Officer) to provide legal consultation to SWA members and judicial backing to the Association.
													</p>
													<div class="portfolio legal" data-cat="legal">
														<div class="portfolio-wrapper">
															<img src="img/legal/AnjumRajabali.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Anjum Rajabali</a>
																	<span class="text-category">Chairperson </span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio legal" data-cat="legal">
														<div class="portfolio-wrapper">
															<img src="img/legal/VinodRangnath.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Vinod Ranganath</a>
																	<span class="text-category">MEMBER</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio legal" data-cat="legal">
														<div class="portfolio-wrapper">
															<img src="img/legal/ShridharRaghvan.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Shridhar Raghavan</a>
																	<span class="text-category">MEMBER</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio legal" data-cat="legal">
														<div class="portfolio-wrapper">
															<img src="img/legal/SwanandKirkire.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Swanand Kirkire</a>
																	<span class="text-category">MEMBER</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio legal" data-cat="legal">
														<div class="portfolio-wrapper">
															<img src="img/legal/AashishPawaskar.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Aashish Pawaskar</a>
																	<span class="text-category">Co-Chairperson</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio legal" data-cat="legal">
														<div class="portfolio-wrapper">
															<img src="img/legal/Debashish.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Debashish Irengbam</a>
																	<span class="text-category">MEMBER</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio legal" data-cat="legal">
														<div class="portfolio-wrapper">
															<img src="img/legal/AtikaChouhan.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Atika Chohan</a>
																	<span class="text-category">MEMBER</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio legal" data-cat="legal">
														<div class="portfolio-wrapper">
															<img src="img/legal/PreetiMamgain.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Preeti Mamgain</a>
																	<span class="text-category">EX-OFFICIO</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio legal" data-cat="legal">
														<div class="portfolio-wrapper">
															<img src="img/legal/ZamanHabib.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Zaman Habib</a>
																	<span class="text-category">EX-OFFICIO</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<!--legal end here-->
													<!--Constitution start here-->
													<p class="text-justify portfolio cc">

													</p>
													<div class="portfolio cc" data-cat="cc">
														<div class="portfolio-wrapper">
															<img src="img/cc/JaleesSherwani.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Jalees Sherwani</a>
																	<span class="text-category">CHARIMAN</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio cc" data-cat="cc">
														<div class="portfolio-wrapper">
															<img src="img/cc/AnjumRajabali.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Anjum Rajabali</a>
																	<span class="text-category">MEMBER</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio cc" data-cat="cc">
														<div class="portfolio-wrapper">
															<img src="img/cc/DanishJaved.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Danish Javed</a>
																	<span class="text-category">MEMBER</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio cc" data-cat="cc">
														<div class="portfolio-wrapper">
															<img src="img/cc/SatyamTripathi.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Satyam Tripathy</a>
																	<span class="text-category">MEMBER</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio cc" data-cat="cc">
														<div class="portfolio-wrapper">
															<img src="img/cc/ManishaKorde.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Manisha Korde</a>
																	<span class="text-category">MEMBER</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio cc" data-cat="cc">
														<div class="portfolio-wrapper">
															<img src="img/cc/SanjayChouhan.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Sanjay Chouhan</a>
																	<span class="text-category">MEMBER</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio cc" data-cat="cc">
														<div class="portfolio-wrapper">
															<img src="img/cc/PreetiMamgain.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Preeti Mamgain</a>
																	<span class="text-category">EX-OFFICIO</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio cc" data-cat="cc">
														<div class="portfolio-wrapper">
															<img src="img/cc/ZamanHabib.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Zaman Habib</a>
																	<span class="text-category">EX-OFFICIO</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<!--Constitution end here-->
													<!--event start here-->
													<p class="text-justify portfolio ev">
														The Event Sub-Committee of SWA conducts events, including seminars, workshops, panel discussions, and talks etc., which promote the aims and objectives of the Association. SWA events provide a platform to SWA members as well as other screenwriters to network and interact.
													</p>
													<div class="portfolio ev" data-cat="ev">
														<div class="portfolio-wrapper">
															<img src="img/ev/DanishJaved.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Danish Javed</a>
																	<span class="text-category">Chairperson</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio ev" data-cat="ev">
														<div class="portfolio-wrapper">
															<img src="img/ev/AbhijeetDeshpande.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Abhijeet Deshpande</a>
																	<span class="text-category">CONVENER</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio ev" data-cat="ev">
														<div class="portfolio-wrapper">
															<img src="img/ev/SANJAYCHHEL.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">SANJAY CHHEL</a>
																	<span class="text-category">MEMBER</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio ev" data-cat="ev">
														<div class="portfolio-wrapper">
															<img src="img/ev/KalyaniPandit.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Kalyani Pandit</a>
																	<span class="text-category">MEMBER</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio ev" data-cat="ev">
														<div class="portfolio-wrapper">
															<img src="img/ev/MitaliMahajan.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Mitali Mahajan</a>
																	<span class="text-category">MEMBER</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio ev" data-cat="ev">
														<div class="portfolio-wrapper">
															<img src="img/ev/ParakhChouhan.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Parakh Chouhan</a>
																	<span class="text-category">MEMBER</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio ev" data-cat="ev">
														<div class="portfolio-wrapper">
															<img src="img/ev/ShahabAllahabadi.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Shahab Allahabadi</a>
																	<span class="text-category">MEMBER</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio ev" data-cat="ev">
														<div class="portfolio-wrapper">
															<img src="img/ev/PreetiMamgain.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Preeti Mamgain</a>
																	<span class="text-category">EX-OFFICIO</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio ev" data-cat="ev">
														<div class="portfolio-wrapper">
															<img src="img/ev/ZamanHabib.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Zaman Habib</a>
																	<span class="text-category">EX-OFFICIO</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio ev" data-cat="ev">
														<div class="portfolio-wrapper">
															<img src="img/exe/SatyamTripathi.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Satyam Tripathy</a>
																	<span class="text-category">Co-Chairperson</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<!--event end here-->
													<!--dscbible start here-->
													<p class="text-justify portfolio dscbible">
													</p>
													<div class="portfolio dscbible" data-cat="dscbible">
														<div class="portfolio-wrapper">
															<img src="img/dscbible/Jyoti-Kapoor.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Jyoti Kapoor</a>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio dscbible" data-cat="dscbible">
														<div class="portfolio-wrapper">
															<img src="img/dscbible/PreetiMamgain.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Preeti Mamgain</a>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio dscbible" data-cat="dscbible">
														<div class="portfolio-wrapper">
															<img src="img/dscbible/RajeshDubey.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Rajesh Dubey</a>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio dscbible" data-cat="dscbible">
														<div class="portfolio-wrapper">
															<img src="img/dscbible/VinodRangnath.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Vinod Ranganath</a>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio dscbible" data-cat="dscbible">
														<div class="portfolio-wrapper">
															<img src="img/dscbible/ZamanHabib.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Zaman Habib</a>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<!--dscbible end here-->
													<!--media start here-->
													<p class="text-justify portfolio media">
														The Media Sub-Committee aims to encourage the presence and prominence of screenwriters; by educating, sensitizing, and promoting the appreciation of screenwriting in media and the public mind, while also spreading awareness about the struggle and importance of SWA.

													</p>
													<div class="portfolio media" data-cat="media">
														<div class="portfolio-wrapper">
															<img src="img/media/AnjumRajabali.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Anjum Rajabali</a>
																	<span class="text-category">Co-Chairperson</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio media" data-cat="media">
														<div class="portfolio-wrapper">
															<img src="img/media/Debashish.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Debashish Irengbam</a>
																	<span class="text-category">Chairperson</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio media" data-cat="media">
														<div class="portfolio-wrapper">
															<img src="img/media/KalyaniPandit.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Kalyani Pandit</a>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio media" data-cat="media">
														<div class="portfolio-wrapper">
															<img src="img/media/ManishaKorde.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Manisha Korde</a>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio media" data-cat="media">
														<div class="portfolio-wrapper">
															<img src="img/media/RajeshDubey.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Rajesh Dubey</a>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio media" data-cat="media">
														<div class="portfolio-wrapper">
															<img src="img/media/SANJAYCHHEL.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">SANJAY CHHEL</a>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio media" data-cat="media">
														<div class="portfolio-wrapper">
															<img src="img/media/SwanandKirkire.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Swanand Kirkire</a>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<!--media end here-->
													<!--mbc start here-->
													<p class="text-justify portfolio mbc">
														The aim all three MBC sub-committees (Film, TV & Lyricist) is to draft the Minimum Basic Contracts (model contracts) and the minimum rate cards, and ensure, via collective bargain and negotiations, that they’re accepted as standard industry practices. The Minimum Basic Contracts safeguard the fees, credit, copyright, etc. of screenwriters.
													</p>
													<div class="portfolio mbc" data-cat="mbc">
														<div class="portfolio-wrapper">
															<img src="img/mbc/AashishPawaskar.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Aashish Pawaskar</a>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio mbc" data-cat="mbc">
														<div class="portfolio-wrapper">
															<img src="img/mbc/AbhijeetDeshpande.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Abhijeet Deshpande</a>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio mbc" data-cat="mbc">
														<div class="portfolio-wrapper">
															<img src="img/mbc/AnjumRajabali.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Anjum Rajabali</a>
																	<span class="text-category">Chairperson</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio mbc" data-cat="mbc">
														<div class="portfolio-wrapper">
															<img src="img/mbc/ManishaKorde.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Manisha Korde</a>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<!-- <div class="portfolio mbc" data-cat="mbc">
														<div class="portfolio-wrapper">
															<img src="img/mbc/MeghnaGulzar.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Meghna Gulzar</a>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div> -->
													<div class="portfolio mbc" data-cat="mbc">
														<div class="portfolio-wrapper">
															<img src="img/mbc/SanjayChouhan.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Sanjay Chouhan</a>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio mbc" data-cat="mbc">
														<div class="portfolio-wrapper">
															<img src="img/mbc/ShridharRaghvan.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Shridhar Raghavan</a>
																	<span class="text-category">Co-Chairperson</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<!--mbc end here-->
													<!--tvmbc start here-->
													<p class="text-justify portfolio tvmbc">
														The aim all three MBC sub-committees (Film, TV & Lyricist) is to draft the Minimum Basic Contracts (model contracts) and the minimum rate cards, and ensure, via collective bargain and negotiations, that they’re accepted as standard industry practices. The Minimum Basic Contracts safeguard the fees, credit, copyright, etc. of screenwriters.
													</p>
													<div class="portfolio tvmbc" data-cat="tvmbc">
														<div class="portfolio-wrapper">
															<img src="img/tvmbc/PreetiMamgain.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Preeti Mamgain</a>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio tvmbc" data-cat="tvmbc">
														<div class="portfolio-wrapper">
															<img src="img/tvmbc/RajeshDubey.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Rajesh Dubey</a>
																	<span class="text-category">Co-Chairperson</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio tvmbc" data-cat="tvmbc">
														<div class="portfolio-wrapper">
															<img src="img/tvmbc/SatyamTripathi.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Satyam Tripathy</a>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio tvmbc" data-cat="tvmbc">
														<div class="portfolio-wrapper">
															<img src="img/tvmbc/VinodRangnath.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Vinod Ranganath</a>
																	<span class="text-category">Chairperson</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio tvmbc" data-cat="tvmbc">
														<div class="portfolio-wrapper">
															<img src="img/tvmbc/ZamanHabib.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Zaman Habib</a>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<!--tvmbc end here-->
													<!--lmbc start here-->
													<p class="text-justify portfolio lmbc">
														The aim all three MBC sub-committees (Film, TV & Lyricist) is to draft the Minimum Basic Contracts (model contracts) and the minimum rate cards, and ensure, via collective bargain and negotiations, that they’re accepted as standard industry practices. The Minimum Basic Contracts safeguard the fees, credit, copyright, etc. of screenwriters.

													</p>
													<div class="portfolio lmbc" data-cat="lmbc">
														<div class="portfolio-wrapper">
															<img src="img/lmbc/DanishJaved.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Danish Javed</a>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio lmbc" data-cat="lmbc">
														<div class="portfolio-wrapper">
															<img src="img/lmbc/JaleesSherwani.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Jalees Sherwani</a>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio lmbc" data-cat="lmbc">
														<div class="portfolio-wrapper">
															<img src="img/lmbc/SANJAYCHHEL.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">SANJAY CHHEL</a>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio lmbc" data-cat="lmbc">
														<div class="portfolio-wrapper">
															<img src="img/lmbc/ShahabAllahabadi.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Shahab Allahabadi</a>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio lmbc" data-cat="lmbc">
														<div class="portfolio-wrapper">
															<img src="img/lmbc/SwanandKirkire.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Swanand Kirkire</a>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<!--lmbc end here-->
													<!--web start here-->
													<p class="text-justify portfolio web">
														The Website Sub-Committee intends to create an efficient digital platform between SWA members and the Association via the SWA website and social media. The SWA website offers Online Script Registration facility, online membership management system, and content which promotes the aims and objectives of SWA, helping screenwriters nurture their craft and being informed.
													</p>
													<div class="portfolio web" data-cat="web">
														<div class="portfolio-wrapper">
															<img src="img/web/Debashish.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Debashish Irengbam</a>
																	<span class="text-category">MEMBER</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<!-- <div class="portfolio web" data-cat="web">
														<div class="portfolio-wrapper">
															<img src="img/web/dinkarsharma.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Dinkar sharma</a>
																	<span class="text-category">WEB CONTENT EDITOR</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div> -->
													<div class="portfolio web" data-cat="web">
														<div class="portfolio-wrapper">
															<img src="img/web/MitaliMahajan.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Mitali Mahajan</a>
																	<span class="text-category">Chairperson</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio web" data-cat="web">
														<div class="portfolio-wrapper">
															<img src="img/web/PreetiMamgain.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Preeti Mamgain</a>
																	<span class="text-category">EX-OFFICIO</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio web" data-cat="web">
														<div class="portfolio-wrapper">
															<img src="img/web/RobinBhatt.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Robin Bhatt</a>
																	<span class="text-category">MEMBER</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio web" data-cat="web">
														<div class="portfolio-wrapper">
															<img src="img/web/SanjayChouhan.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Sanjay Chouhan</a>
																	<span class="text-category">MEMBER</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio web" data-cat="web">
														<div class="portfolio-wrapper">
															<img src="img/exe/Pooja Varma.jpg" style="max-width: 118px;max-height: 112px;" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Pooja Varma</a>
																	<span class="text-category">MEMBER</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>

													<div class="portfolio web" data-cat="web">
														<div class="portfolio-wrapper">
															<img src="img/web/ShridharRaghvan.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Shridhar Raghavan</a>
																	<span class="text-category">Co-chairperson</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio web" data-cat="web">
														<div class="portfolio-wrapper">
															<img src="img/web/SunilSalgia.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Sunil Salgia</a>
																	<span class="text-category">MEMBER</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<div class="portfolio web" data-cat="web">
														<div class="portfolio-wrapper">
															<img src="img/web/ZamanHabib.jpg" alt="" />
															<div class="label">
																<div class="label-text">
																	<a class="text-title">Zaman Habib</a>
																	<span class="text-category">EX-OFFICIO</span>
																</div>
																<div class="label-bg"></div>
															</div>
														</div>
													</div>
													<!--web end here-->

													</div>
												</div>
												<!--content end here-->
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>


						<div class="tabs_item">
							<div class="container">
								<div class="row">
									<div class="col-md-12 mg-40">
										<h5>OFFICE STAFF</h5>
										<ul class="office">
											<li>
												<span class="off-name">Dinkar Sharma</span>
												<span class="off-dsg">Executive Coordinator & Website Editor</span>
											</li>
											<li>
												<span class="off-name">Pramila Kadam</span>
												<span class="off-dsg">Office Secretary</span>
											</li>
											<li>
												<span class="off-name">Heema Shirvaikar</span>
												<span class="off-dsg">Legal Officer</span>
											</li>
											<li>
												<span class="off-name">Sumant Prajapati</span>
												<span class="off-dsg">Assistant Executive Secretary</span>
											</li>
											<li>
												<span class="off-name">Ragini Shengunshi</span>
												<span class="off-dsg">Accounts Secretary</span>
											</li>
											<li>
												<span class="off-name">Yashwant Bamgude</span>
												<span class="off-dsg">Senior Assistant</span>
											</li>
											<li>
												<span class="off-name">Pravin Mandavkar</span>
												<span class="off-dsg">Junior Assistant</span>
											</li>
											<li>
												<span class="off-name">Yash Shellar</span>
												<span class="off-dsg">Junior Assistant</span>
											</li>
											<li>
												<span class="off-name">Santosh Patil</span>
												<span class="off-dsg">Office Boy</span>
											</li>
											<li>
												<span class="off-name">Amar Jadhav</span>
												<span class="off-dsg">Office Boy</span>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php include_once('footer.php'); ?>
			</div>
			<!-- Modal -->
			<div id="preeti" class="modal fade" role="dialog">
				<div class="modal-dialog modal-lg">
					<!-- Modal content-->
					<div class="modal-content project-details-popup">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
          <!-- <div class="modal-header">
          <img class="header-img" src="images/preeti1.jpg">
      </div> -->
      <div class="modal-body">
      	<div class="row">
      		<div class="col-md-4 text-center">
      			<img class="header-img" src="images/preeti1.jpg" style="width: 225px;">
      		</div>
      		<div class="col-md-8">
      			<h4>Preeti Mamgain</h4>
      			<h5>Presedent</h5>
      			<p class="text-justify">The lady who has served as a writer for many popular serials starting from Jassi Jaisi Koi Nahin, Jab Love Hua, Saara Akaash, Ek Chutki Aasmaan, Phir Subah Hogi to name a few, was on a break from acting for the past one and half years. However, she has been part of Saurabh Shukla’s play ‘Two to Tango, Three to Jive’ for more than a year now.</p>
      			<p>Preeti will essay the role of the female protagonist Sanjeeda Sheikh’s mother in the Zee TV show.</p>
      		</div>
      	</div>
      </div>
        <!--     <div class="modal-footer">
    </div> -->
</div>
</div>
</div>
<script src="js/jquery-1.11.3.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.bxslider.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
<script src="js/jquery.counterup.min.js"></script>
<script src="js/custom.js"></script>
<script type="text/javascript" src="js/jquery.mixitup.min.js"></script>
<script type="text/javascript">
	$(function () {
		$('.dsc').addClass('show');

		/** filter for sub commitee */
		$('.cat').on('click', function(){

			$('.cat').removeClass('active');
			$(this).addClass('active');

			$('#portfoliolist .portfolio').removeClass('show');
			var cat = $(this).data('filter');
			$('.'+cat).addClass('show');


		});
		/** filter for sub commitee end */


// 	var filterList = {
// 		init: function () {
//        	// MixItUp plugin
//       	// http://mixitup.io
//       	$('#portfoliolist').mixItUp();

//       	$('#portfoliolist').mixItUp({
//       	selectors: {
//       		target: '.portfolio',
//       		filter: '.filter'
//       	},
//       	load: {
//       		filter: '.dsc'
//       	}
//       });
//   }
// };
//   // Run the show!
//   filterList.init();

$('.tab ul.tabs').addClass('active').find('> li:eq(0)').addClass('current');

$('.tab ul.tabs li a').click(function (g) {
	var tab = $(this).closest('.tab'),
	index = $(this).closest('li').index();

	tab.find('ul.tabs > li').removeClass('current');
	$(this).closest('li').addClass('current');

	tab.find('.tab_content').find('div.tabs_item').not('div.tabs_item:eq(' + index + ')').slideUp();
	tab.find('.tab_content').find('div.tabs_item:eq(' + index + ')').slideDown();

	g.preventDefault();
});


var tab = getParameterByName('tab');
if(tab != '' && tab != null)
{
	$('#'+tab).click();
}
function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
            var regexS = "[\\?&]" + name + "=([^&#]*)";
            var regex = new RegExp(regexS);
            var results = regex.exec(window.location.search);
            if (results == null)
                return "";
            else
                return decodeURIComponent(results[1].replace(/\+/g, " "));
        }
});


</script>
</body>
</html>
