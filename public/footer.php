  <footer class="cp-footer">
  	<div class="footer-top">
  		<div class="container">
  			<div class="row">
  				<div class="col-md-3">
  					<div class="widget textwidget">
  						<h3>SWA</h3>
  						<p>The Screenwriters Association (SWA, formerly Film Writers' Association - Regd. Under the Trade Union Act 1926, Regd. No. 3726) is a Trade Union of screenwriters and lyricists who work for Films, TV and Digital Media in India.</p>
  						<ul class="footer-social">
  							<li><a href="https://www.facebook.com/swaindiaorg" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
  							<!-- <li><a href="#"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a></li> -->
  							<li><a href="https://www.youtube.com/screenwritersassociation" target="_blank"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
  							<li><a href="https://twitter.com/swaindiaorg" target="_blank"><i class="fa fa-twitter"></i></a></li>
  						</ul>
  					</div>
  				</div>
  				<div class="col-md-3 hidden-xs hidden-sm">
  					<div class="widget">
  						<h3>SITE LINKS</h3>
  						<ul class="footer-links">
  							<li><a href="mission.php">ABOUT US</a></li>
                <li><a href="membership.php">BECOME A MEMBER</a></li>
                  <?php if(!empty($_SESSION['is_login']) && $_SESSION['is_login'] == 'true') { ?>
                <li><a href="register_script.php">REGISTER YOUR WORK</a></li>
              <?php } else { ?>
              <li><a href="javascript:void(0)" data-toggle="modal" data-target="#myModal">REGISTER YOUR WORK</a></li>
              <?php } ?>
                <li><a href="lawyer.php">ASK SWA</a></li>
                <li><a href="contact.php">CONTACT US</a></li>
                <!-- <li><a href="mission.php#constitution">Our Constitution</a></li> -->
  							<!-- <li><a href="team.php">People at SWA</a></li> -->
  							<!-- <li><a href="lawyer.php">Ask Our Lawyer</a></li> -->
  							<!-- <li><a href="dsc.php">Ask Our DSC</a></li> -->
  						</ul>
  					</div>
  				</div>
  				<div class="col-md-3 hidden-xs hidden-sm">
            <div class="widget">
              <h3>OTHER LINKS</h3>
              <ul class="footer-links">
                <li><a href="history.php">History of SWA</a></li>
                <li><a href="writer.php">THE WRITERS CHARTER</a></li>
                <li><a href="do.php">DOs & DON’Ts FOR WRITERS</a></li>
                <li><a href="faqs.php">Frequently Asked Questions</a></li>
                <li><a href="downloads.php">Download Center</a></li>
                <!-- <li><a href="contact.php">CONTACT US</a></li> -->
              </ul>
            </div>
          </div>
          <div class="col-md-3 hidden-xs hidden-sm">
  					<div class="widget">
  						<h3>CONTACT US</h3>
  						<p class="cont-footer">
              <ul class="add-ul clr-foot">
              <li class="add"><span><a href="contact.php">View on Map</span></li>
              <li class="tele"></i>+91 22 2673 3027 / +91 22 2673 3108 / +91 22 6692 2899</li>
              <li class="emailid"><a href="mailto:contact@swaindia.org">contact@swaindia.org</a></li>
            </ul>
            </p>
  					</div>
  				</div>
  				<!-- <div class="col-md-3">
  					<div class="widget cp-newsltter">
  						<h3>SUBSCRIBE FOR UPDATES</h3>
  						<form class="newsletter">
  							<input type="email" required placeholder="Enter email for subscription..." name="email">
  							<button type="submit">Submit</button>
  						</form>
  					</div>
  				</div> -->
  			</div>
  		</div>
  	</div>
  	<div class="cp-footer-bottom">
  		<p>&copy; 2017 Powered by <a href="http://firsteconomy.com" target="_blank">Firsteconomy.com</a> </p>
  	</div>
  </footer>
