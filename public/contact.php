<?php session_start(); ?>
<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/x-icon" href=""/>
  <title>About Us</title>
  <link href="css/custom.css" rel="stylesheet" type="text/css">
  <link href="css/color.css" rel="stylesheet" type="text/css">
  <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
  <link href="css/responsive.css" rel="stylesheet" type="text/css">
  <link href="css/jquery.bxslider.css" rel="stylesheet" type="text/css">
  <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen"/>
  <link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,300' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>

      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
    </head>
    <body class="inner-page">
      <div id="wrapper">
       <?php include_once('header.php'); ?>
       <div id="cp-content-wrap">
         <div class="cp_our-story-section">
           <div class="container">
          <!-- <div class="row">
           <div class="col-md-12">
              <div class="cp-acticle-box abt-div">
                <h1 class="title bold text-center">
                  Contact
                </h1>
              </div>
           </div>
         </div> -->
        <?php if(!empty($_SESSION['is_login']) && $_SESSION['is_login'] == 'true') {
          if(isset($_SESSION['username']))
          {
            $reg_id = $_SESSION['username'];
          }
          else {
            $reg_id = '';
          }

          if(isset($_SESSION['User_Email']))
          {
            $email = $_SESSION['User_Email'];
          }
          else {
            $email = '';
          }

          if(isset($_SESSION['name']))
          {
            $name = $_SESSION['name'];
          }
          else {
            $name = '';
          }
        }
        else {
          $reg_id = '';
          $email = '';
          $name = '';
        } ?>
         <div class="row">
          <div class="col-md-7">
            <div class="cp-acticle-box abt-div cp-contact-form">
              <h4 class="title bold text-center">
                Write to SWA
              </h4>
              <form action="javascript:void(0)">
                <div class="row">
                  <div class="col-md-6">
                    <input type="text" id="name"  placeholder="Name*" class="form-control" value="<?php echo $name; ?>">
                  </div>
                  <div class="col-md-6">
                    <input type="text" id="email" placeholder="Email*" class="form-control"  value="<?php echo $email; ?>">
                  </div>
                </div>
                <?php if(!empty($_SESSION['is_login']) && $_SESSION['is_login'] == 'true') { ?>
                <div class="row">
                  <div class="col-md-12">
                    <input type="text" id="reg_no"  value="<?php echo $_SESSION['username']; ?>" placeholder="SWA Membership Number*" class="form-control" readonly>
                  </div>
                </div>
              <?php } else { ?>
                <div class="row">
                  <div class="col-md-8">
                    <input type="text" id="reg_no"  placeholder="SWA Membership Number*" class="form-control">
                  </div>
                  <div class="col-md-4">
                    <input type="checkbox" class="non-mem-check" name="nonmember" id="nonmember" value="nonmember">&nbsp;Not A Member<br>
                  </div>
                </div>

              <?php } ?>
                <div class="row">
                  <div class="col-md-12">
                    <select class="sub-select" id="subject">
                      <option disabled selected value="">Select Subject</option>
                      <option value="Query">Query</option>
                      <option value="Feedback">Feedback</option>
                      <option value="Other">Other</option>
                      <option value="Legal_Consultation">Legal Officer - Consultation</option>
                      <option value="Legal_Officer">Legal Officer - Appointment</option>
                      <option value="Dsc">To The DSC</option>
                      <option value="Tech_Sign">Technical Support - Not Able to Sign In/Sign Up</option>
                      <option value="Tech_Reg">Technical Support - Online Script Registration</option>
                    </select>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <textarea name="comments" id="comments" placeholder="Comments*"></textarea>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <button class="frm-btn" id="contact_form" type="submit">Submit</button>
                  </div>
                </div>
                <br><br>
                <div class="row">
                  <div class="col-md-12">
                    <div id="contact_form_msg"></div>
                  </div>
                </div>
              </form>
            </div>
          </div>
          <div class="col-md-5">
           <div class="">
            <div class="widget address-holder">
             <h4 class="title bold text-center">
              Address & Contact
            </h4>
            <ul class="add-ul">
              <li class="add"><span>201 - 204, Richa Building, Plot No. B - 29, <br>Off New Link Road, Opposite Citi Mall, <br>Andheri (West) Mumbai,<br> Maharashtra - 400 053,<br>India</span></li>
              <li class="tele"></i>+91 22 2673 3027 / +91 22 2673 3108 / +91 22 6692 2899</li>
              <li class="emailid"><a href="mailto:contact@swaindia.org">contact@swaindia.org</a></li>
              <li class="clockic"><strong>Office Time*:</strong> Monday - Saturday  | 11.00 am - 7.00 pm<br>
              <li class="clockic"><strong>Membership (New/Renewal):</strong> Monday - Saturday  | 11.30 am - 6.00 pm<br>
                                               <strong>Script Registration Day:</strong> Monday - Friday  | 2.00 pm - 5.30 pm<br>
                                               </li>
              <li class="italic">*Closed on Sunday and Bank Holidays.</li>

            </ul>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
<div class="banner_inner">
  <div id="map" style="width: 100%;min-height: 500px"></div>
</div>
</div>
<?php include_once('footer.php'); ?>
</div>
<script src="js/jquery-1.11.3.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.bxslider.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
<!-- <script src="js/jquery.counterup.min.js"></script> -->
<script src="js/custom.js"></script>
<script type="text/javascript" src="js/jquery.mixitup.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCPLaTboCJrq0dwB4IbnTZVxP1gmApt7IM&callback=initMap"
async defer></script>
<script>
  function initMap() {
    var myLatLng = {lat: 19.1398266, lng: 72.8338993};
        // Create a map object and specify the DOM element for display.
        var map = new google.maps.Map(document.getElementById('map'), {
          center: myLatLng,
          scrollwheel: false,
          zoom: 17
        });

        var contentString = '<h4>Screenwriters Association</h4><p>201 - 204, Richa Building,<br> Plot No. B - 29, <br>Off New Link Road, Opposite Citi Mall,<br>Andheri (West) Mumbai,<br> Maharashtra - 400 053,<br>India</p>';

        var infowindow = new google.maps.InfoWindow({
          content: contentString
        });

  // Create a marker and set its position.
  var marker = new google.maps.Marker({
    map: map,
    position: myLatLng,
    title: 'SWA Office'
  });
  marker.addListener('click', function() {
    infowindow.open(map, marker);
  });
}

$('#nonmember').on('click',function(){
    if( $('#nonmember').is(':checked') ){
      $('#reg_no').prop('disabled',true).addClass('disab').removeClass('error');
    }
    else
    {
     $('#reg_no').prop('disabled',false).removeClass('disab');
    }
});
</script>
</body>
</html>
