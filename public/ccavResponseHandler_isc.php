<?php	session_start();
	include('Cryptoif.php');
	include("includes/config.php");
	// include("includes/config_sync.php");
	// production keys
	$working_key='E93C7C8FA026C576DCD2941297B6E255';		//Working Key should be provided here.

	//test keys
	// $working_key='BFCC3D4DF0DEA863AFB73F9DAF8B5C18';//Shared by CCAVENUES


	$encResponse=$_POST["encResp"];			//This is the response sent by the CCAvenue Server

	$rcvdString=decrypt($encResponse,$working_key);		//Crypto Decryption used as per the specified working key.
	//var_dump($rcvdString);die;
	$order_status="";
	$decryptValues=explode('&', $rcvdString);
	$dataSize=sizeof($decryptValues);
	// echo "<center>";
	// echo '<pre>';
	// var_dump($decryptValues);
	// echo '</pre>';
	// die;
	for($i = 0; $i < $dataSize; $i++)
	{
		$information=explode('=',$decryptValues[$i]);
		if($i==3)	$order_status=$information[1];
		if($i==0)	$order_id=$information[1];
		if($i==1)	$trk_id=$information[1];
		if($i==2)	$bnk_ref=$information[1];
		if($i==4)	$f_msg=$information[1];
		if($i==5)	$p_mode=$information[1];
		if($i==6)	$c_name=$information[1];
		if($i==10)	$amt=$information[1];
		if($i==11)	$b_name=$information[1];
		if($i==18)	$b_email=$information[1];
		if($i==26)	$order_row_id=$information[1];
		if($i==27)	$member_reg_no=$information[1];
		if($i==28)	$reg_type=$information[1];
		// if($i==29)	$membership_card_status=$information[1];
		if($i==30)	$reg_no=$information[1];
		if($i==25)	$dcl=$information[1];
	}

	$sql_order_update = "UPDATE `isc_orders` SET `order_status` = '".$order_status."' , `payment_mode` = '".$p_mode."'   , `tracking_id` = '".$trk_id."'  , `failure_message` = '".$f_msg."' , `bank_ref_no` = '".$bnk_ref."' , `card_name` = '".$c_name."' , `billing_name` = '".$b_name."' WHERE `id` = ".$order_row_id;
	// var_dump($sql_order_update);die;
	//var_dump($order_status);die;
	// print_r($result_t);die;
	$res_order_update = mysqli_query($db, $sql_order_update);

	if($order_status==="Success")
	{
		include_once('process/functions.php');

		$body = "Dear Delegate!<br><br>

				Thank you, for registering for the 5th Indian Screenwriters Conference.<br><br>

				Here're your details:<br><br>

				Order ID: ".$order_id."<br><br>
				CC Avenue Reference Number (Tracking Id): ".$trk_id."<br><br>

				Please, show the Print Out/Print Screen/PDF of this Confirmation Message/Email, to collect your Delegate Card from SWA office (July 28th 2018 onwards) OR at the venue.<br><br>

				Cheers to you for supporting SWA!<br><br>
				Looking forward to seeing you!<br><br>

				Team 5ISC";
		$subject = "Your Online Delegate Registration for 5ISC has been confirmed!";

		/** email id and password from where mail will be send */
		$username = 'assist@firsteconomy.com';
		$password = 'f3e6f3e6';

		$senderName = 'Team 5ISC';
		$senderEmail = 'web@swaindia.org';

		$recName = $b_name;
		$recEmail = $b_email;

		// $result_email = sendMail($username,$password,$senderName,$senderEmail,$recName,$recEmail,$body,$subject);
		include_once('PHPMailerAutoload.php');
		$mail = new PHPMailer(true);
		$mail->IsSMTP(); // telling the class to use SMTP
		$mail->IsHTML();
		$mail->SMTPAuth = true; // enable SMTP authentication
		$mail->SMTPSecure = "ssl"; // sets the prefix to the servier
		$mail->Host = "smtp.gmail.com"; // sets GMAIL as the SMTP server
		$mail->Port = 465; // set the SMTP port for the GMAIL server
		$mail->Username = $username; // GMAIL username
		$mail->Password = $password; // GMAIL password

		$mail->AddAddress($recEmail,$recName);
			$mail->AddReplyTo($senderEmail, $senderName);
		// $mail->AddAddress('nirmeet@firsteconomy.com');
		$mail->SetFrom($senderEmail, $senderName);
		$mail->Subject = $subject;
		$mail->Body = $body;

		$result_email = $mail->Send();

		$_SESSION['isc_success_msg'] = "Order placed successfully";
	  	header('Location: isc_thank_you.php');
	}
	else if($order_status==="Aborted")
	{
		$_SESSION['isc_error_msg'] = "Order aborted, please try again";
	  	header('Location: isc_thank_you.php');
	}
	else if($order_status==="Failure")
	{
		$_SESSION['isc_error_msg'] = "Order failed, please try again";
	  	header('Location: isc_thank_you.php');
	}
	else
	{
		$_SESSION['isc_error_msg'] = "Something went wrong, please try again";
    	header('Location: isc_thank_you.php');
	}

	?>
