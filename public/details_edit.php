<?php session_start();
//echo "<pre>";print_r($_SESSION);die;
if(isset($_SESSION['is_login']) &&  $_SESSION['is_login'] == 'true')
{
  include_once('includes/config.php');
  ?>

  <!doctype html>
  <html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/x-icon" href=""/>
    <title>Film Writers Association | My Creations</title>
    <link href="css/custom.css" rel="stylesheet" type="text/css">
    <link href="css/color.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="css/responsive.css" rel="stylesheet" type="text/css">
    <link href="css/jquery.bxslider.css" rel="stylesheet" type="text/css">
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen"/>
    <link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,300' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
    .input-ed{
    width:100%;
    border: #dddddd 1px solid;
    }
    .input-ed::-webkit-inner-spin-button,
    .input-ed::-webkit-outer-spin-button {
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    margin: 0;
}{

    }
    /**
    * Tabs
    */
    .tabs {
    display: flex;
    flex-wrap: wrap; // make sure it wraps
    }
    .tabs label {
    order: 1; // Put the labels first
    display: block;
    padding: 1rem 2rem;
    margin-right: 0.2rem;
    cursor: pointer;
    background: #eff0f2;
    font-weight: normal;
    transition: background ease 0.2s;
    border-bottom: 2px solid #000;
    }
    .tabs .tab {
    order: 99; // Put the tabs last
    flex-grow: 1;
    width: 100%;
    display: none;
    padding: 1rem;
    background: #fff;
    }
    .tabs input[type="radio"] {
    display: none;
    }
    .tabs input[type="radio"]:checked + label {
    background: #000;
    color: #fff;
    border-bottom: 2px solid #000;
    }
    .tabs input[type="radio"]:checked + label + .tab {
    display: block;
    }
    @media (max-width: 45em) {
    .tabs .tab,
    .tabs label {
    order: initial;
    }
    .tabs label {
    width: 100%;
    margin-right: 0;
    margin-top: 0.2rem;
    }
    }
    .mem{margin-left: 35px;}
    .nerror{border: 1px solid red!important;}
    </style>
  </head>
  <body class="inner-page">
    <div id="wrapper" class="inside-menu">
      <?php include_once('header.php'); ?>
      <div id="cp-content-wrap" class="page404 cp-login-page">
        <div class="container">
          <div class="row">
            <div class="col-md-12 main-reg-div">
              <div class="cp-reg-box sub-reg-div">
                <!-- <h5 class="sub-hd">WELCOME  <?php //echo $_SESSION['name'];?></h5> -->
                <?php
                $sql = "select fu.*,mem.* from `fwa_users` fu
                left join `fwa_members` mem on mem.reg_no = fu.reg_no
                where fu.reg_no = '".$_SESSION['username']."'";
                $result =  mysqli_query($db,$sql);
                $row = mysqli_fetch_assoc($result);
                ?>
                <h4>Your Details <span class="pull-right"></h4>
                  <input type="hidden" value="<?php echo $row['reg_no']; ?>" id="reg_no">
                  <div class="tabs">
                    <input type="radio" name="tabs" id="tabone" checked="checked">
                    <label for="tabone">Address</label>
                    <div class="tab">
                      <div class="cp-acticle-box">
                        <table class='table table-hover'>
                            <tr>
                              <th>Membership No.</th>
                              <td><?php echo $row['reg_no']; ?></td>
                            </tr>
                            <tr>
                              <td colspan="2" class="text-center"><strong>Primary Address</strong></td>
                            </tr>
                            <tr>
                              <th>Address 1</th>
                              <td><input type="text" class="input-ed" name="add1" id="add1" value="<?php echo $row['add1']; ?>"></td>
                            </tr>
                            <tr>
                              <th>Address 2</th>
                              <td><input type="text" class="input-ed" name="add2" id="add2" value="<?php echo $row['add2']; ?>"></td>
                            </tr>
                            <tr>
                              <th>City</th>
                              <td><input type="text" class="input-ed" name="city" id="city" value="<?php echo $row['city']; ?>"></td>
                            </tr>
                            <tr>
                              <th>Pin</th>
                              <td><input type="text" class="input-ed" name="pin" id="pin" value="<?php echo $row['pin']; ?>"></td>
                            </tr>
                            <tr>
                              <th>State</th>
                              <td><input type="text" class="input-ed" name="state" id="state" value="<?php echo $row['state']; ?>"></td>
                            </tr>
                            <tr>
                              <td colspan="2" class="text-center"><strong>Secondary Address</strong></td>
                            </tr>
                            <tr>
                              <th>Address 1</th>
                              <td><input type="text" class="input-ed" name="sadd1" id="sadd1" value="<?php echo $row['sadd1']; ?>"></td>
                            </tr>
                            <tr>
                              <th>Address 2</th>
                              <td><input type="text" class="input-ed" name="sadd2" id="sadd2" value="<?php echo $row['sadd2']; ?>"></td>
                            </tr>
                            <tr>
                              <th>City</th>
                              <td><input type="text" class="input-ed" name="scity" id="scity" value="<?php echo $row['scity']; ?>"></td>
                            </tr>
                            <tr>
                              <th>Pin</th>
                              <td><input type="text" class="input-ed" name="spin" id="spin" value="<?php echo $row['spin']; ?>"></td>
                            </tr>
                            <tr>
                              <th>State</th>
                              <td><input type="text" class="input-ed" name="sstate" id="sstate" value="<?php echo $row['sstate']; ?>"></td>
                            </tr>
                            <tr>
                              <td colspan="2" class="text-center"><strong>Mailing address to be used:</strong>
                                <select id="address_use" name="address_use">
                                  <option <?php if($row['address_in_use'] == '1') { echo 'selected'; }  ?> value="1">Primary</option>
                                  <option <?php if($row['address_in_use'] == '2') { echo 'selected'; }  ?> value="2">Secondary</option>
                                </select>
                              </td>
                            </tr>
                            <tr>
                              <td colspan="2" class="text-center"><a id="address_update" href="javascript:void(0)" class="btn btn-success" role="button">UPDATE</a></td>
                            </tr>
                      </table>
                      <div id="data_msg"></div>
                      </div>
                    </div>
                    <input type="radio" name="tabs" id="tabtwo">
                    <label for="tabtwo">Mobile No</label>
                    <div class="tab">
                      <div class="cp-acticle-box">
                        <table class='table table-hover'>
                            <tr>
                              <th>Membership No.</th>
                              <td><?php echo $row['reg_no']; ?></td>
                            </tr>
                            <tr>
                              <th>Mobile No</th>
                              <td><?php echo $row['mobileno']; ?></td>
                            </tr>
                            <tr>
                              <th>Enter New Number</th>
                              <td><input type="number" class="input-ed" id="new_number"></td>
                            </tr>
                            <tr id="otp_div" class="hidden">
                              <th>Enter OTP</th>
                              <td><input type="number" class="input-ed" id="otp_mobile_usr" placeholder="Enter OTP"></td>
                            </tr>
                            <tr>
                              <td colspan="2" class="text-center">
                                <a id="generate_otp" href="javascript:void(0)" class="btn btn-success" role="button">Get OTP</a>
                                <a id="update_number" href="javascript:void(0)" class="btn btn-success hidden" role="button">Proceed</a></td>
                            </tr>
                          </table>
                            <div id="mobile_msg"></div>
                      </div>
                    </div>
                    <input type="radio" name="tabs" id="tabthree">
                    <label for="tabthree">Email id</label>
                    <div class="tab">
                      <div class="cp-acticle-box">
                        <table class='table table-hover'>
                          <tr>
                            <th>Membership No.</th>
                            <td><?php echo $row['reg_no']; ?></td>
                          </tr>
                          <tr>
                            <th>Email Id</th>
                            <td><?php echo $row['email']; ?></td>
                          </tr>
                          <tr>
                            <th>Enter New Email Id</th>
                            <td><input type="text" class="input-ed" id="new_mail"></td>
                          </tr>
                          <tr id="otp_mail_div" class="hidden">
                            <th>Enter OTP</th>
                            <td><input type="number" class="input-ed" id="otp_mail_usr" placeholder="Enter OTP"></td>
                          </tr>
                          <tr>
                            <td colspan="2" class="text-center">
                              <a id="generate_mail_otp" href="javascript:void(0)" class="btn btn-success" role="button">Get OTP</a>
                              <a id="update_mail" href="javascript:void(0)" class="btn btn-success hidden" role="button">Proceed</a></td>
                          </tr>
                      </table>
                        <div id="mail_msg"></div>
                      </div>
                    </div>
                  </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php include_once('footer.php'); ?>
  </div>
  <script src="js/jquery-1.11.3.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.bxslider.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.prettyPhoto.js"></script>
  <script src="js/custom.js"></script>
</body>
</html>
<?php }
else{
  header('Location: login.php');
} ?>
