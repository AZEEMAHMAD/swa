<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/x-icon" href="images/favicon.png"/>
	<title>Home</title>
	<link href="css/custom.css" rel="stylesheet" type="text/css">
	<link href="css/color.css" rel="stylesheet" type="text/css">
	<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="css/responsive.css" rel="stylesheet" type="text/css">
	<link href="css/jquery.bxslider.css" rel="stylesheet" type="text/css">
	<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.3/assets/owl.carousel.min.css" rel="stylesheet" type="text/css">
	<link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/vertical.news.slider.css?v=1.0">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.20/jquery.fancybox.min.css" />
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
     <style type="text/css">
     	.bx-wrapper .bx-controls-direction a{
     		z-index: 999!important;
     	}
      #header{
        position: relative;
      }
      .homepage .cp-navigation-section {
          border-bottom: none !important;
          background: #161616;
          border-top: 1px solid;
      }
      #cp-banner-style-1 .bx-wrapper .bx-pager, #cp-banner-style-1 .bx-wrapper .bx-controls-auto{
        bottom: 35px;
      }

      .holder {
          width: auto !important;
          padding: 5px 15px;
      }
      #slider_1 .holder{
        background: rgb(206, 110, 15);
      }
      #slider_2 .holder{
        background: rgba(61, 129, 117, 0.92);
      }
      #slider_3 .holder{
        background: rgba(112, 38, 37, 0.88);
      }
			#cp-banner-1 li:before{
       display: none;
     }

	 .adj{
	 	  line-height: 24px!important;
		  font-size: 19px!important;
	 }
   @media(max-width: 768px){
    #cp-banner-style-1 .caption .holder {
        position: absolute;
        bottom: -30px;
        left: 200px;
        padding: 2px 10px;
    }
   }
     </style>
  </head>
  <body class="homepage">
  	<div id="wrapper">
      <?php include_once('header.php'); ?>
  <div id="cp-banner-style-1">
  	<ul id="cp-banner-1">
<!--
			<li onclick="window.open('http://swaindia.org/blog/swa-vartalaap-in-conversation-with-screenwriters-and-lyricists-making-a-difference/', '_blank');" id="slider_3">
				<img src="images/banner/vaartalap_banner.jpg" alt="img">
				<div class="caption">
				</div>
			</li> -->


			<!-- <li onclick="window.open('https://www.facebook.com/events/168024757249636/', '_blank');" id="slider_2">
				<img src="images/banner/varun_grover.jpg" alt="img">
				<div class="caption">
					<div class="container">
						<div class="holder">
							<h1>In conversation with Varun Grover,<br> National Award winning lyricist and screenwriter </h1>
						</div>
					</div>
				</div>
			</li> -->

			<!-- <li onclick="window.open('http://swaindia.org/article_dyn.php?q=TWpRPQ', '_blank');"  id="slider_1">
				<img src="images/banner/auditorium.jpg" alt="img">
				<div class="caption">
					<div class="container">
						<div class="holder">
							<h1>Programme of the 5th Indian Screenwriters Conference 2018</h1>
						</div>
					</div>
				</div>
			</li>
			<li onclick="window.open('http://swaindia.org/5ISCregistrations/', '_blank');"  id="slider_2">
				<img src="images/banner/banner_isc.jpg" alt="img">
				<div class="caption">
					<div class="container">
						<div class="holder">
							<h1>Register Now</h1>
						</div>
					</div>
				</div>
			</li> -->


			<li onclick="window.open('http://swaindia.org/blog/5isc-%e0%a4%a8%e0%a5%87-%e0%a4%96%e0%a5%80%e0%a4%82%e0%a4%9a%e0%a4%be-%e0%a4%aa%e0%a5%8d%e0%a4%b0%e0%a5%88%e0%a4%b8-%e0%a4%94%e0%a4%b0-%e0%a4%ae%e0%a5%80%e0%a4%a1%e0%a4%bf%e0%a4%af%e0%a4%be-%e0%a4%95/', '_blank');"  id="slider_1">
				<img src="images/banner/writers_vs_producers.jpg" alt="img">
				<div class="caption">
					<div class="container">
						<div class="holder">
							<h1>पांचवीं इंडियन स्क्रीनराइटर्स कॉन्फ़्रेंस (5ISC) ने खींचा प्रैस और मीडिया का ध्यान!</h1>
						</div>
					</div>
				</div>
			</li>


			<li onclick="window.open('https://www.facebook.com/pg/swaindiaorg/photos/?tab=album&album_id=1797518636969565', '_blank');"  id="slider_2">
				<img src="images/banner/award_ceremony.jpg" alt="img">
				<div class="caption">
					<div class="container">
						<div class="holder">
							<h1>5ISC Photo Album</h1>
						</div>
					</div>
				</div>
			</li>

			<li onclick="window.open('http://swaindia.org/article_dyn.php?q=TWpZPQ', '_blank');"  id="slider_3">
				<img src="images/banner/banner_legal.jpg" alt="img">
				<div class="caption">
					<div class="container">
						<div class="holder">
							<h1><span style="font-size: 16px;text-decoration: underline;" >SWA विशेष</span><br>कॉपीराइट सुरक्षा के मूलभूत नियम</h1>
						</div>
					</div>
				</div>
			</li>
			<!-- <li onclick="window.open('http://swaindia.org/article_dyn.php?q=TWpNPQ', '_blank');"  id="slider_3">
				<img src="images/banner/sacred_games_poster.jpg" alt="img">
				<div class="caption">
					<div class="container">
						<div class="holder">
							<h1>‘Sacred Games’ writer Varun Grover speaks with SWA</h1>
						</div>
					</div>
				</div>
			</li> -->

			<li onclick="window.open('http://swaindia.org/blog/selected-entrants-for-the-the-first-round-of-mpa-apsa-launch-your-script-program-supported-by-swa/', '_blank');"  id="slider_2">
				<img src="images/banner/aps_event_banner.gif" alt="img">
				<div class="caption">
					<div class="container">
						<div class="holder">
							<h1>Shortlisted entries of MPA-APSA and SWA's 'Launch Your Script' Program</h1>
						</div>
					</div>
				</div>
			</li>

			<!-- <li onclick="window.open('http://swaindia.org/blog/aamir-khan-to-be-the-chief-guest-for-the-5th-indian-screenwriters-conference/', '_blank');" id="slider_1">
				<img src="images/banner/banner_aamir.jpg" alt="img">
				<div class="caption">
					<div class="container">
						<div class="holder">
							<h1 class="adj">AAMIR KHAN to be the CHIEF-GUEST <br>for the 5th INDIAN SCREENWRITERS CONFERENCE</h1>
						</div>
					</div>
				</div>
			</li> -->

			<!-- <li onclick="window.open('http://swaindia.org/apsaevent/', '_blank');">
				<img src="images/banner/aps_event_banner.gif" alt="img">
			</li> -->

			<!-- <li onclick="window.open('https://www.facebook.com/events/231487577433298/', '_blank');" id="slider_2">
				<img src="images/banner/banner_sc_sq.jpg" alt="img">
				<div class="caption">
					<div class="container">
						<div class="holder" style="float:right">
							<h1>In conversation with Screenwriters and Lyricists making a difference</h1>
						</div>
					</div>
				</div>
			</li> -->


			<!-- <li onclick="window.open('http://swaindia.org/blog/registrations-open-for-the-new-batch-of-how-to-write-a-comedy-script-a-workshop-by-anuvab-pal-supported-by-swa/', '_blank');" id="slider_3">
				<img src="images/banner/banner_pal.jpg" alt="img">
				<div class="caption">
					<div class="container">
						<div class="holder">

						</div>
					</div>
				</div>
			</li> -->
			<!-- <li onclick="window.open('article_dyn.php?q=TWpBPQ', '_blank');" id="slider_1">
				<img src="images/banner/banner_nimiki.jpg" alt="img">
				<div class="caption">
					<div class="container">
						<div class="holder">
							<h1>‘निमकी मुखिया’ के लेखक-निर्माता ज़माँ हबीब के साथ एक मुलाकात</h1>
						</div>
					</div>
				</div>
			</li> -->

			<!-- <li onclick="window.open('article_dyn.php?q=TWpFPQ', '_blank');" id="slider_3">
				<img src="images/banner/banner_mukkabaaz.jpg" alt="img">
				<div class="caption">
					<div class="container">
						<div class="holder">
							<h1>
								Writer of Mukkabaaz K D Satyam speaks with SWA
							</h1>
						</div>
					</div>
				</div>
			</li> -->
			<!-- <li onclick="window.open('http://swaindia.org/blog/how-to-write-a-comedy-script-a-workshop-by-anuvab-pal-supported-by-swa/', '_blank');" id="slider_1">
				<img src="images/banner/banner_workshop.jpg" alt="img">
				<div class="caption">
					<div class="container">
						<div class="holder" style="float:right">
							<h1>
								Updated Dates: March 10 & 11, 2018
							</h1>
						</div>
					</div>
				</div>
			</li> -->
			<!-- <li onclick="window.open('article_dyn.php?q=TVRrPQ', '_blank');" id="slider_2">
				<img src="images/banner/banner4.jpg" alt="img">
				<div class="caption">
					<div class="container">
						<div class="holder">
							<h1>
								Writer-director Neeraj Pandey talks to SWA
							</h1>
						</div>
					</div>
				</div>
			</li> -->

			<!-- <li onclick="window.open('http://swaindia.org/article_dyn.php?q=TVRnPQ', '_blank');" id="slider_1">
  			<img src="images/banner/hindiMedium.jpg" alt="img">
  			<div class="caption">
  				<div class="container">
  					<div class="holder">
  						<h1>Writer-director of Hindi Medium<br>Saket Chaudhary speaks with SWA</h1>
  					</div>
  				</div>
  			</div>
  		</li> -->
			<!-- <li onclick="window.open('https://www.youtube.com/watch?v=l1l0ta8xCPg&feature=youtu.be', '_blank');" style="cursor:pointer">

				<img src="images/option/slider/javed.jpg" alt="img">
				<div class="caption">
					<div class="container">
						<div class="holder">
							<h1>
								Javed Saahab Se Ek Mulaqaat
							</h1>
						</div>
					</div>
				</div>
			</li> -->
    <!-- <li onclick="window.location.href = 'article.php'" id="slider_1">
        <img src="images/option/slider/1.jpg" alt="img">
        <div class="caption">
          <div class="container">
            <div class="holder">
              <ul>
                <li>Reading</li>
              </ul>
              <h1>Film Writers Association: The Diamond Years
              </h1>
            </div>
          </div>
        </div>
      </li> -->
			<!-- <li onclick="window.open('article_dyn.php?q=TVRjPQ', '_blank');" id="slider_2">
					<img src="images/banner/aakrosh.jpg" alt="img">
					<div class="caption">
						<div class="container">
							<div class="holder">
								 <h1>
									 ‘आक्रोश’ के ‘अर्धसत्य’ की खोज में लगा लेखक
								 </h1>
							</div>
						</div>
					</div>
				</li> -->
    <!-- <li onclick="window.location.href = 'article_dyn.php?q=TXc'" id="slider_2">
        <img src="images/option/slider/2.jpg" alt="img">
        <div class="caption">
          <div class="container">
            <div class="holder">
              <ul>
                <li>EDITOR'S COLUMN</li>
              </ul>
              <h1>Whose Script Is it, Anyway?</h1>
            </div>
          </div>
        </div>
      </li> -->
  		<!-- <li onclick="window.open('https://scriptcontest.cinestaan.com/', '_blank');" id="slider_1">
  			<img src="images/banner/cinestaan.jpg" alt="img">
  			<div class="caption">
  				<div class="container">
  					<div class="holder">
  						<h1>Deadline Extended till Jan 31st 2018</h1>
  					</div>
  				</div>
  			</div>
  		</li> -->
  		<!-- <li>
  			<img src="images/banner-img-2.jpg" alt="img">
  			<div class="caption">
  				<div class="container">
  					<div class="holder">
  						<ul>
  							<li>Reading</li>
  						</ul>
  						<h1>Film Writers Association: The Diamond Years
  						</h1>
  					</div>
  				</div>
  			</div>
  		</li> -->
  		<!-- <li>
  			<img src="images/banner-img-3.jpg" alt="img">
  			<div class="caption">
  				<div class="container">
  					<div class="holder">
  						<ul>
  							<li>Old is Gold</li>
  						</ul>
  						<h1>
  						  Bollywood Classics
  						</h1>
  					</div>
  				</div>
  			</div>
  		</li> -->
      <!-- <li onclick="window.location.href = 'article_dyn.php?q=TkE'" id="slider_3">
        <img src="images/option/slider/3.jpg" alt="img">
        <div class="caption">
          <div class="container">
            <div class="holder">
              <ul>
                <li>FILM DESK</li>
              </ul>
              <h1>
               KARTHICK NAREN: The Wonder Boy of Tamil Neo-Noir
              </h1>
            </div>
          </div>
        </div>
      </li> -->
      <!-- <li onclick="window.open('article_dyn.php?q=TVRZPQ', '_blank');" id="slider_3">
        <img src="images/banner/bl.jpg" alt="img">
        <div class="caption">
          <div class="container">
            <div class="holder">

              <h1>
               Shreyas Jain, the star-writer of Dangal and<br> Bareilly Ki Barfi speaks to SWA
              </h1>
            </div>
          </div>
        </div>
      </li> -->
  	</ul>
  </div>
  <div id="cp-content-wrap" class="cp-content-wrap">
  	<div class="container">
  		<div class="row">
  			<div class="col-md-9 left_block col-xs-12">
  				<div class="content-inner">
  					<div class="page-section">
  						<div class="">
  							<h1 class="title bold text-center" data-bgtext="LATEST VIDEOS">
  								LATEST VIDEOS
  							</h1>
  						</div>
  					</div>
  				</div>
  				<div class="news-holder cf">
  					<ul class="news-headlines">
              <li class="selected">A tribute to Dr Rahi Masoom Raza!</li>
  						<li>A tribute to Shailendra!</li>
  						<li>A tribute to Khwaja Ahmad Abbas!</li>
  						<li>SWA वार्तालाप with Purnendu Shekhar</li>
  					</ul>
  					<div class="news-preview">

              <div class="news-content top-content">
                <article class="video">
                  <figure>
                    <a class="fancybox fancybox.iframe" href="https://youtu.be/x3yvKgvA6ls"><img class="videoThumb" src="images/vid_th_1.jpg"></a>
                  </figure>
                </article>
              </div>

  						<div class="news-content top-content">
                <article class="video">
                  <figure>
                    <a class="fancybox fancybox.iframe" href="https://youtu.be/oPc1NbbfPSM"><img class="videoThumb" src="images/vid_th_2.jpg"></a>
                  </figure>
                </article>
              </div>

              <div class="news-content top-content">
                <article class="video">
                  <figure>
                    <a class="fancybox fancybox.iframe" href="https://youtu.be/SUCVTaWTaLI"><img class="videoThumb" src="images/vid_th_3.jpg"></a>
                  </figure>
                </article>
              </div>

              <div class="news-content top-content">
                <article class="video">
                  <figure>
                    <a class="fancybox fancybox.iframe" href="https://youtu.be/ygiMJI9UR6c"><img class="videoThumb" src="images/vid_th_4.jpg"></a>
                  </figure>
                </article>
              </div>


  					</div>
  				</div>
  				<div class="cp-posts-style-1">
  					<div class="content-inner">
  						<div class="page-section">
  							<div class="">
  								<h1 class="title bold text-center">

  								</h1>
  							</div>
  						</div>
  					</div>
  					<ul class="cp-posts-list" id="swa_article">
  						<!-- <li class="cp-post">
  							<div class="cp-thumb">
  								<img src="images/option/blog/1.jpg" alt="neo">
  								<div class="cp-post-hover" ><a data-toggle="tooltip" title="Share" data-placement="auto" href="article_dyn.php?q=TWc9PQ"><i class="fa fa-link"></i></a></div>
  							</div>
  							<div class="cp-post-base">
  								<div class="cp-post-content">
  									<h2 onclick="window.location.href = 'article_dyn.php?q=TWc9PQ'">Toilet - Ek Premkatha</h2>
  									<ul class="cp-post-meta">
  										<li><a href="article_dyn.php?q=TWc9PQ">From Film Desk</a></li>
  									</ul>
  									<p>Script Analysis
  									</p>
  									<a href="#" class="leave-comment"><i class="fa fa-comment-o"></i> Leave a Comment</a>
  								</div>
  							</div>
  						</li> -->
  						<li class="cp-post">
  							<div class="cp-thumb">
  								<img src="images/option/slider/2.jpg" alt="neo">
  								<div class="cp-post-hover" ><a data-toggle="tooltip" title="Share" data-placement="auto" href="article_dyn.php?q=TWc9PQ"><i class="fa fa-link"></i></a></div>
  							</div>
  							<div class="cp-post-base">
  								<div class="cp-post-content">
  									<h2 onclick="window.location.href = 'article_dyn.php?q=TXc'">Whose Script Is it, Anyway?</h2>
  									<ul class="cp-post-meta">
  										<li><a href="article_dyn.php?q=TWc9PQ">Film Desk</a></li>
  									</ul>
  									<p>By Dinkar Sharma</p>
  									<!-- <a href="#" class="leave-comment"><i class="fa fa-comment-o"></i> Leave a Comment</a> -->
  								</div>
  							</div>
  						</li>
  						<li class="cp-post">
  							<div class="cp-thumb">
  								<img src="images/option/blog/2.jpg" alt="neo">
  								<div class="cp-post-hover"> <a href="javascript:void(0)" data-toggle="tooltip" title="Share" data-placement="auto"><i class="fa fa-link"></i></a> </div>
  							</div>
  							<div class="cp-post-base">
  								<div class="cp-post-content">
  									<h2 onclick="window.location.href = 'article.php'">SWA HISTORY</h2>
  									<ul class="cp-post-meta">
  										<li><a href="article.php">FILM WRITERS ASSOCIATION: THE DIAMOND YEARS</a></li>
  									</ul>
  									<p>By Kamlesh Pandey</p>
  									<!-- <a href="#" class="leave-comment"><i class="fa fa-comment-o"></i> Leave a Comment</a> -->
  								</div>
  							</div>
  						</li>
  						<li class="cp-post">
  							<div class="cp-thumb">
  								<img src="images/banner/insideedge.jpg" alt="neo">
  								<div class="cp-post-hover"> <a href="javascript:void(0)" data-toggle="tooltip" title="Share" data-placement="auto"><i class="fa fa-link"></i></a> </div>
  							</div>
  							<div class="cp-post-base">
  								<div class="cp-post-content">
  									<h2 onclick="window.location.href = 'article_dyn.php?q=TVRVPQ'">Web-series medium will ensure power and money for writers.</h2>
  									<ul class="cp-post-meta">
  										<li><a href="article_dyn.php?q=TVRVPQ">Q & A with Karan Anshuman, director and creator of Inside Edge</a></li>
  									</ul>
  									<p>By Debashish Irengbam</p>
  									<!-- <a href="#" class="leave-comment"><i class="fa fa-comment-o"></i> Leave a Comment</a> -->
  								</div>
  							</div>
  						</li>
  						<li class="cp-post">
  							<div class="cp-thumb">
  								<img src="images/banner/hindiMedium.jpg" alt="neo">
  								<div class="cp-post-hover"> <a href="javascript:void(0)" data-toggle="tooltip" title="Share" data-placement="auto"><i class="fa fa-link"></i></a> </div>
  							</div>
  							<div class="cp-post-base">
  								<div class="cp-post-content">
  									<h2 onclick="window.location.href = 'article_dyn.php?q=TVRnPQ'">Writer-director of Hindi Medium speaks with SWA</h2>
  									<ul class="cp-post-meta">
  										<li><a href="article_dyn.php?q=TVRnPQ">It takes several bad scripts to reach one decent one that is readable.</a></li>
  									</ul>
  									<p>By Yash Thakur</p>
  									<!-- <a href="#" class="leave-comment"><i class="fa fa-comment-o"></i> Leave a Comment</a> -->
  								</div>
  							</div>
  						</li>
  						<li class="cp-post">
  							<div class="cp-thumb">
  								<img src="images/banner/bl.jpg" alt="neo">
  								<div class="cp-post-hover"> <a href="javascript:void(0)" data-toggle="tooltip" title="Share" data-placement="auto"><i class="fa fa-link"></i></a> </div>
  							</div>
  							<div class="cp-post-base">
  								<div class="cp-post-content">
  									<h2 onclick="window.location.href = 'article_dyn.php?q=TVRZPQ'">The star-writer of Dangal and Bareilly Ki Barfi speaks to SWA</h2>
  									<ul class="cp-post-meta">
  										<li><a href="article_dyn.php?q=TVRZPQ">Bihari Lalla is the new Bambaiyya Badshah!</a></li>
  									</ul>
  									<p>By Punam Mohandas</p>
  									<!-- <a href="#" class="leave-comment"><i class="fa fa-comment-o"></i> Leave a Comment</a> -->
  								</div>
  							</div>
  						</li>
  					</ul>
  				</div>
  				<div class="cp-posts-style-1">
  					<div class="content-inner">
  						<div class="page-section">
  							<div class="">
  								<h1 class="title bold text-center" data-bgtext="NEWS & NOTICES">
  									UPDATES
  								</h1>
  							</div>
  						</div>
  					</div>
  					<div class="owl-carousel" id="news">
  						<div class="item">
  							<figure class="snip0021">
                  <img src="images/news1.jpg" alt="sample22" class="min-ht-220"/>
                  <figcaption>
                    <div><h4><span>Lucknow Girls</span> win First Prize</h4></div>
                    <div>
                      <p>International Short Film Screenwriting Competition</p>
                    </div>
                    <a href="#"></a>
                  </figcaption>
                </figure>

  						</div>
  						<div class="item">
  							<figure class="snip0021 hover">
                  <img src="images/news2.jpg" alt="sample20" class="min-ht-220"/>
                  <figcaption>
                    <div><h4>“A new chapter has started!” - <span>Javed Akhtar leisure</span></h4></div>
                    <div>
                      <p>Shri Javed Akhtar is the new Chairman of revamped IPRS</p>
                    </div>
                    <a href="#"></a>
                  </figcaption>
                </figure>
  						</div>
  						<div class="item">
  							<figure class="snip0021">
                  <img src="images/news3.jpg" alt="sample21" class="min-ht-220"/>
                  <figcaption>
                    <div><h4>Screen writing Workshop by <span>Anjum Rajabali</span></h4></div>
                    <div>
                      <p>Whistling Woods International (WWI), Film workshop</p>
                    </div>
                    <a href="#"></a>
                  </figcaption>
                </figure>
  						</div>
  					</div>
  				</div>
  			</div>
  			<div class="col-md-3 col-xs-12 sidemob"> <!-- removed class pd-top-70 -->
  				<div class="sidebar">
  					<div class="widget featured-posts">
  						<h3>SWA EVENTS</h3>
  						<div class="widget-content reviewwidget">
  							<div class="carousel-wrap">
  								<div class="owl-carousel" id="upcome">
                    <div class="item">
                      <a href="https://www.youtube.com/watch?v=xMISd7xBsCM&t=278s" class="desc_ImageOverlay" target="_blank" style="background-image: url('images/rev/swaeventsnew1.jpg');">
                        <div class="desc">
                          <h4>Jaideep Sahni at SWA</h4>
                          <p>
                            Read More
                          </p>
                        </div>
                      </a>
                    </div>

  									<div class="item">
  										<a href="https://www.facebook.com/population.first/videos/1434028089978781/" class="desc_ImageOverlay" target="_blank" style="background-image: url('images/rev/swaeventsnew.jpg');">
  											<div class="desc">
  												<h4>Lights Camera Patriarchy</h4>
  												<p>
  													Read More
  												</p>
  											</div>
  										</a>
  									</div>
  								</div>
  							</div>
  						</div>
  					</div>
  					<div class="widget archives-widget">
  						<h3>DESI SCRIPTWRITER</h3>
  						<div class="widget-content listwidget" data-toggle="modal" data-target="#sc_modal">
  							<img src="images/sw_th.jpg" style="max-width: 100%;border: 1px solid #c7c7c7;">
  						</div>
  					</div>
  					<div class="widget tags-widget">
  						<h3>CATEGORY</h3>
  						<div class="widget-content widmcontentmob">
  							<ul>
  								<li> <a href="">Film Desk</a> <a href="">Old is Gold</a> <a href="">Television Desk</a> <a href=""> Question & Answers</a> <a href="">Regional Desk</a> <a href="">Discussion Board</a></li>
  							</ul>
  						</div>
  					</div>
  					<!-- <div class="widget latest-stories-widget">
  						<h3>SWA REVIEWS</h3>
              <div class="widget-content reviewwidget">
                <div class="carousel-wrap">
                  <div class="owl-carousel" id="reviews">
                    <div class="item">
                      <a href="#" class="desc_ImageOverlay" style="background-image: url('images/rev/rev1.jpg');">
                        <div class="desc">
                          <h4>बहन होगी तेरी - "दर्शकों को पटाने में असफल!"</h4>
                          <p>
                            Click for full review
                          </p>
                        </div>
                      </a>
                    </div>
                    <div class="item">
                      <a href="#" class="desc_ImageOverlay" style="background-image: url('images/rev/rev2.jpg');">
                        <div class="desc">
                          <h4>राब्ता - "दर्शकों से नहीं बना पाई राब्ता"</h4>
                          <p>
                            Click for full review
                          </p>
                        </div>
                      </a>
                    </div>
                    <div class="item">
                      <a href="#" class="desc_ImageOverlay" style="background-image: url('images/rev/rev3.jpg');">
                        <div class="desc">
                          <h4>Noor - "Leaves audience be-noor"</h4>
                          <p>
                            Click for full review
                          </p>
                        </div>
                      </a>
                    </div>
                    <div class="item">
                      <a href="#" class="desc_ImageOverlay" style="background-image: url('images/rev/rev5.jpg');">
                        <div class="desc">
                          <h4>Wonder Woman - "Wonderful by Comparison; Ordinary on its Own"</h4>
                          <p>
                            Click for full review
                          </p>
                        </div>
                      </a>
                    </div>
                    <div class="item">
                      <a href="#" class="desc_ImageOverlay" style="background-image: url('images/rev/rev4.jpg');">
                        <div class="desc">
                          <h4>Saab Bahadur - "Suspense-thriller? Really?"</h4>
                          <p>
                            Click for full review
                          </p>
                        </div>
                      </a>
                    </div>
                  </div>
                </div>
                </div>
  					</div> -->
  				</div>
  			</div>
  		</div>
  	</div>
  </div>
  <?php include_once('footer.php'); ?>

</div>
<!-- Modal -->
<div id="sc_modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content project-details-popup">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      <div class="modal-header">
        <img class="header-img" src="images/sw.jpg">
      </div>
    </div>
  </div>
</div>
<!-- <div id="notice_modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content project-details-popup" style="padding: 20px;">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      <div class="modal-header">
        <h3>NOTICE</h3>(25-07-2018)
				<p>Dear Members,</p>
				<p>Due to the ongoing Protest and the call of Bandh in the city of Mumbai, we're required to close down our office today. This is being done as a precautionary measure. The office will reopen on 26-07-2018 at 11:00 am. In case there is any change, we will update it on our website www.swaindia.org</p>
				<p>We regret the inconvenience caused to you.</p>
				<p>SWA Management</p>
      </div>
    </div>
  </div>
</div> -->
<script src="js/jquery-1.11.3.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.bxslider.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.3/owl.carousel.min.js"></script>
<script src="js/custom.js"></script>
<script src="js/vertical.news.slider.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.20/jquery.fancybox.min.js"></script>
<script type="text/javascript">
	$('#reviews').owlCarousel({
		loop: true,
		margin: 10,
		nav: true,
		navText: [
		"<i class='fa fa-angle-left' aria-hidden='true'></i>",
		"<i class='fa fa-angle-right' aria-hidden='true'></i>"
		],
		autoplay: true,
		autoplayHoverPause: true,
		items: 1
	});

	$('#upcome').owlCarousel({
    loop: true,
    margin: 10,
    nav: true,
    navText: [
    "<i class='fa fa-angle-left' aria-hidden='true'></i>",
    "<i class='fa fa-angle-right' aria-hidden='true'></i>"
    ],
    autoplay: true,
    autoplayHoverPause: true,
    items: 1
  });

	$('#news').owlCarousel({
		loop: true,
		margin: 50,
		nav: true,
		navText: [
		"<i class='fa fa-angle-left' aria-hidden='true'></i>",
		"<i class='fa fa-angle-right' aria-hidden='true'></i>"
		],
		autoplay: true,
		autoplayHoverPause: true,
		items: 3,
     responsiveClass: true,
      responsive: {
       0: {
         items: 1,
         margin: 50,

       },
       600: {
         items: 1,
         margin: 50,

       },
       1000: {
         items: 3
       }
     }
	});
  $('a').tooltip('show')

  // Fancybox
$(document).ready(function() {
  $('.fancybox').fancybox({
    padding   : 0,
    maxWidth  : '100%',
    maxHeight : '100%',
    width   : 560,
    height    : 315,
    autoSize  : true,
    closeClick  : true,
    openEffect  : 'elastic',
    closeEffect : 'elastic'
  });
  $('.bx-controls').addClass('container');
  $("figure").mouseleave(
    function () {
      $(this).removeClass("hover");
    }
  );

	// $('#notice_modal').modal('show')
});
</script>
<script>
  // (function() {
  //   var cx = '004231188845848405683:nh5utauzsnw';
  //   var gcse = document.createElement('script');
  //   gcse.type = 'text/javascript';
  //   gcse.async = true;
  //   gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
  //   var s = document.getElementsByTagName('script')[0];
  //   s.parentNode.insertBefore(gcse, s);
  // })();
</script>
<gcse:search></gcse:search>
</body>
</html>
