<?php session_start();
?>
<!doctype html>
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="icon" type="image/x-icon" href=""/>
      <title>Sign In</title>
      <link href="css/custom.css" rel="stylesheet" type="text/css">
      <link href="css/color.css" rel="stylesheet" type="text/css">
      <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
      <link href="css/responsive.css" rel="stylesheet" type="text/css">
      <link href="css/jquery.bxslider.css" rel="stylesheet" type="text/css">
      <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
      <link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen"/>
      <link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,300' rel='stylesheet' type='text/css'>
      <link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
      <style>
      .page404, .comming-soon {
          background: url(images/loginbanner.jpg) no-repeat top center;
          background-size: cover;
          padding: 60px 0;
          width: 100%;
          float: left;
          text-align: center;
      }
      .cp-login-box, .cp-reg-box {
    margin: 0 auto;
    background: #fff;
    border-radius: 2px;
    border: 1px solid #eeeeee;
    text-align: left;
    padding: 50px;
}
.rule-list{
   color:#fff;
}
.clearboth{
  clear: both!important;
}
.marg-10{
  margin: 10px 0;
}
.foot-note{
  clear: both;
  padding: 30px 0 0 0;
  font-size: 11px;
}
li#login_message {
    text-align: center;
}
.invalid_msg {
    padding: 15px;
    color: red;
    font-size: 20px;
    margin-bottom: 15px;
}
.success_msg{
    padding: 15px;
    color: green;
    font-size: 20px;
    margin-bottom: 15px;
}
      </style>
   </head>
   <body class="inner-page">
      <div id="wrapper">
         <?php include_once('isc_header.php'); ?>
         <div id="cp-content-wrap" class="page404 cp-login-page">
            <div class="container">
               <div class="row">
                  <div class="col-md-12 main-login-div">
                     <div class="cp-login-box sub-login-div text-center">
                         <?php if(isset($_SESSION['isc_error_msg']))
                              { ?>
                              <div class="col-md-12 invalid_msg">
                                 <?php
                                   echo $_SESSION['isc_error_msg'];
                                   unset($_SESSION['isc_error_msg']);
                                 ?>
                              </div>
                              <?php } ?>

                              <?php if(isset($_SESSION['isc_success_msg']))
                              { ?>
                              <div class="col-md-12 success_msg">
                                 <?php
                                   echo $_SESSION['isc_success_msg'];
                                   unset($_SESSION['isc_success_msg']);
                                 ?>
                              </div>
                              <?php }
                              session_destroy();
                               ?>
                               <a href="http://swaindia.org/5ISCregistrations/#products" type="submit" style="background: #050505;color: #fff;padding: 15px;">Re-Order</a><br>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <?php include_once('footer.php'); ?>
      </div>
      <script src="js/jquery-1.11.3.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="js/jquery.bxslider.min.js"></script>
      <script src="js/owl.carousel.min.js"></script>
      <script src="js/jquery.prettyPhoto.js"></script>
      <script src="js/custom.js"></script>
   </body>
</html>
