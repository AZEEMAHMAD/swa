<?php session_start();
// header('Location: dashboard.php');
// echo "<pre>";
// print_r($_SESSION);
// echo "</pre>";
// die;
date_default_timezone_set('Asia/Calcutta');

if(isset($_SESSION['is_login']) &&  $_SESSION['is_login'] == 'true')
{
  include_once('includes/config.php');
  ?>

  <!doctype html>
  <html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/x-icon" href=""/>
    <title>Film Writers Association | My Creations</title>
    <link href="css/custom.css" rel="stylesheet" type="text/css">
    <link href="css/color.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="css/responsive.css" rel="stylesheet" type="text/css">
    <link href="css/jquery.bxslider.css" rel="stylesheet" type="text/css">
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- <link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen"/> -->
    <link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,300' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
    .input-ed{
      width:100%;
      border: #dddddd 1px solid;
    }
    .input-trns{
      background: transparent;
      border: none;
      padding: 0px 3px;
    }
    .input-ed::-webkit-inner-spin-button,
    .input-ed::-webkit-outer-spin-button {
      -webkit-appearance: none;
      -moz-appearance: none;
      appearance: none;
      margin: 0;
    }
    /**
    * Tabs
    */
    /*.tabs {
    display: flex;
    flex-wrap: wrap; // make sure it wraps
  }
  .tabs label {
  order: 1; // Put the labels first
  display: block;
  padding: 1rem 2rem;
  margin-right: 0.2rem;
  cursor: pointer;
  background: #eff0f2;
  font-weight: normal;
  transition: background ease 0.2s;
  border-bottom: 2px solid #000;
}
.tabs .tab {
order: 99; // Put the tabs last
flex-grow: 1;
width: 100%;
display: none;
padding: 1rem;
background: #fff;
}
.tabs input[type="radio"] {
display: none;
}
.tabs input[type="radio"]:checked + label {
background: #000;
color: #fff;
border-bottom: 2px solid #000;
}
.tabs input[type="radio"]:checked + label + .tab {
display: block;
}
@media (max-width: 45em) {
.tabs .tab,
.tabs label {
order: initial;
}
.tabs label {
width: 100%;
margin-right: 0;
margin-top: 0.2rem;
}
}*/
.mem{margin-left: 35px;}
.nerror{border: 1px solid red!important;}
</style>
</head>
<body class="inner-page">
  <div id="wrapper" class="inside-menu">
    <?php include_once('header.php'); ?>
    <div id="cp-content-wrap" class="page404 cp-login-page">
      <div class="container">
        <div class="row">
          <div class="col-md-12 main-reg-div">
            <div class="cp-reg-box sub-reg-div">
              <!-- <h5 class="sub-hd">WELCOME  <?php //echo $_SESSION['name'];?></h5> -->
              <?php
              $sql = "select fu.*,mem.*,mtyp.type from `fwa_users` fu
              left join `fwa_members` mem on mem.reg_no = fu.reg_no
              left join `mem_type` mtyp on mtyp.type_id = fu.type_id
              where fu.reg_no = '".$_SESSION['username']."'";
              $result =  mysqli_query($db,$sql);
              $row = mysqli_fetch_assoc($result);
              //  echo '<pre>';
              //  var_dump($row);

              /* Membership Type Wise	Renwal Fees */

              $ren_fees_regular_mem = 120;
              $ren_fees_senior_associate_mem = 72;
              $ren_fees_associate_mem = 72;
              $ren_fees_fellow_mem = 72;
              $ren_fees_fellow_foreign_mem = 72;

              /* Late Penalty */
              $late_phase_1 = 0;  // 0 - 6 months
              $late_phase_2 = 500;  // 6 - 18 months
              $late_phase_3 = 1000;  // 18 - 24 months
              $late_phase_4 = 0;  // after 24 (can not renew)

              $renwal_valid = 1;
              $renwal_valid_msg = '';

              $min_renew_period = 1;
              $max_renew_period = 10;

              $late_penelty = 0;
              $ren_fees = 0;

              $monthly_pay = 0; /** 1: for Associate and Fellow and 0: for rest */
              $final_fees = 0; /** only for Associate and Fellow */

              $type_id = $row['type_id'];

              $today_date = strtotime("today midnight"); // unix time stamp
              $exp_date = strtotime($row['membership_expiry_date']);

              if($exp_date < $today_date)
              {
                $fromDate = new DateTime($row['membership_expiry_date']);
                $curDate = new DateTime();

                $diff = $curDate->diff($fromDate);
                $expiry_months = (($diff->format('%y') * 12) + $diff->format('%m') + ($diff->format('%d') / 30 ) );     // time (in months) since membership expired

                $expiry_flag = 1;
                if($expiry_months <= 6)
                {
                  $late_penelty = $late_phase_1;
                }
                elseif($expiry_months > 6 && $expiry_months <= 18){
                  $late_penelty = $late_phase_2;
                  $min_renew_period = ceil($expiry_months/12);
                }
                elseif($expiry_months > 18 && $expiry_months <= 24){
                  $late_penelty = $late_phase_3;
                  $min_renew_period = ceil($expiry_months/12);
                }
                elseif($expiry_months > 24){
                  $late_penelty = $late_phase_4;
                  $renwal_valid = 0;
                  $renwal_valid_msg = 'It has been more than 24 months since your membership has expired, please apply for re admission.';
                }
                else {
                  $renwal_valid = 0;
                  $renwal_valid_msg = 'It has been more than 24 months since your membership has expired, please apply for re admission.';
                }
              }
              else {
                $expiry_flag = 0;
              }

              switch ($type_id) {

                /* Life member */
                case 1:

                $renwal_valid = 0;
                $renwal_valid_msg = "You are a lifetime member, you don't need to renew.";
                break;

                /* Expired Member */
                case 2:
                $renwal_valid = 0;
                $renwal_valid_msg = "Your membership type is: 'Expired member', please contact SWA Office for more details";
                break;

                /* blacklist for lifetime */
                case 3:
                $renwal_valid = 0;
                $renwal_valid_msg = "Your membership type is: 'Blacklist for lifetime', please contact SWA Office for more details";
                break;

                /* 	Senior - R */
                case 4:
                $renwal_valid = 0;
                $renwal_valid_msg = "You are a Senior Regular Member, you don't need to renew.";
                break;

                /* 	Regular */
                case 5:
                $ren_fees = $ren_fees_regular_mem;
                break;

                /* 	Suspend */
                case 6:
                $renwal_valid = 0;
                $renwal_valid_msg = "Your membership type is: 'Suspend', please contact SWA Office for more details";
                break;

                /* 	Senior - A / not valid for renewal valid till only 31 Dec 2019 regardless of expiry date */
                case 7:
                $renwal_valid = 0;
                $renwal_valid_msg = "You are a Senior Associate, you don't need to renew.";
                break;

                /* 	Associate can renew only till 31 Dec 2019 */
                case 8:
                $ren_fees = $ren_fees_associate_mem;
                $monthly_pay = 1;
                $fromDate = new DateTime($row['membership_expiry_date']);
                $toDate = new DateTime('2019-12-31');
                $diff = $toDate->diff($fromDate);
                $diff_months = (($diff->format('%y') * 12) + $diff->format('%m') + ceil($diff->format('%d') / 30 ) );     // time (in months) since membership expired
                $final_fees = (($diff_months * 6) + ($diff->format('%y') * 5));
                $_SESSION['diff_months'] = $diff_months;
                break;

                /* Fellow can renew only till 31 Dec 2019 */
                case 9:
                $ren_fees = $ren_fees_fellow_mem;
                $monthly_pay = 1;
                $fromDate = new DateTime($row['membership_expiry_date']);
                $toDate = new DateTime('2019-12-31');
                $diff = $toDate->diff($fromDate);
                $diff_months = (($diff->format('%y') * 12) + $diff->format('%m') + ceil($diff->format('%d') / 30 ) );     // time (in months) since membership expired
                $final_fees = (($diff_months * 6) + ($diff->format('%y') * 5));
                $_SESSION['diff_months'] = $diff_months;
                break;

                /* 	Cancelled */
                case 10:
                $renwal_valid = 0;
                $renwal_valid_msg = "Your membership type is: 'Cancelled for lifetime', please contact SWA Office for more details";
                break;

                /* 	Fellow-Foreign */
                case 11:
                $ren_fees = $ren_fees_fellow_foreign_mem;
                break;

                /* Debarred */
                case 12:
                $renwal_valid = 0;
                $renwal_valid_msg = "Your membership type is: 'Debarred', please contact SWA Office for more details";
                break;

                /* Dual Membership */
                case 13:
                $renwal_valid = 0;
                $renwal_valid_msg = "Your membership type is: 'Dual Membership', please contact SWA Office for more details";
                break;

                /* External */

                case 14:
                $renwal_valid = 0;
                $renwal_valid_msg = "Your membership type is: 'External', please contact SWA Office for more details";
                break;

                default:
                # code...
                break;
              }

              $_SESSION['mem_renew_charge'] = $ren_fees;         // renew fesss per year
              $_SESSION['current_exp_date'] = date("d-m-Y", strtotime($row['membership_expiry_date']) );         // expiry date (before renew)
              $_SESSION['mem_renew_penelty'] = $late_penelty;    // late fees
              $_SESSION['monthly_pay'] = $monthly_pay;
              $_SESSION['final_fees'] = $final_fees; /** only for monthly pay = 1 */
              ?>
              <h4>Membership Renewal</h4>
                <input type="hidden" value="<?php echo $row['reg_no']; ?>" id="reg_no">
                <form action="renew_confirm.php" method="post" id="renew_form">

                <table class='table table-hover'>
                  <tr>
                    <th>Membership No.</th>
                    <td><input value="<?php echo $row['reg_no']; ?>" class="input-trns" name="reg_no" readonly></td>
                  </tr>
                  <tr>
                    <th>Membership Type</th>
                    <td><input name="mem_type" class="input-trns" name="mem_type" id="mem_type" value="<?php echo $row['type'];?>" readonly /></td>
                  </tr>
                  <tr>
                    <th>Membership expiry date</th>
                    <td><input name="current_exp_date"  class="input-trns" id="current_exp_date" value="<?php echo date("d-m-Y", strtotime($row['membership_expiry_date']) );?>" readonly/>
                    </td>
                  </tr>

                  <?php if($renwal_valid == 1) { ?>

                  <tr>
                    <th>Annual Membership renew charge</th>
                    <td>
                      <label>Rs. </label>&nbsp;<input name="mem_renew_charge"  class="input-trns" id="mem_renew_charge" value="<?php echo $ren_fees; ?>" readonly>
                    </td>
                  </tr>
                  <tr>
                    <th>Renew Membership Time</th>
                    <td>

                      <?php if($monthly_pay == 0) { ?>
                      <select class="form-control duration_select update_pref" name="renew_period" id="duration_select">
                        <option value="">Select duration (years)</option>
                        <?php for ($i = $min_renew_period; $i<=$max_renew_period;$i++ ) { ?>
                          <option value="<?php echo $i; ?> "><?php echo $i; ?></option>
                        <?php } ?>
                      </select>
                      <input type="hidden" name="monthly_pay" value="0">
                    <?php } else { ?>
                    Till 31 December 2019 (in months) <input name="renew_period"  class="input-trns" id="duration_select" value="<?php echo $diff_months; ?>" readonly>
                    <input type="hidden" name="monthly_pay" value="1">
                    <?php } ?>
                    </td>
                  </tr>
                  <tr>
                    <th>Total Membership renew charge</th>
                    <td>
                      <?php if($monthly_pay == 0) { ?>
                      <label>Rs. </label>&nbsp;<input name="total_mem_renew_charge"  id="total_mem_renew_charge"  class="input-trns" value="0" readonly>
                      <?php } else { ?>
                        <label>Rs. </label>&nbsp;<input name="total_mem_renew_charge"  id="total_mem_renew_charge"  class="input-trns" value="<?php echo $final_fees; ?>" readonly>
                        <?php } ?>
                    </td>
                  </tr>
                  <tr>
                    <th>New membership expiry date</th>
                    <?php if($monthly_pay == 0) { ?>
                    <td><input name="new_exp_date" id="new_exp_date" value="-" class="input-trns" readonly>
                        <?php } else { ?>
                    <td><input name="new_exp_date" id="new_exp_date" value="31-12-2019" class="input-trns" readonly>
                      <?php } ?>
                    </td>
                  </tr>
                  <tr>
                    <th>Membership renew late penelty (if any)</th>
                    <td><label>Rs. </label>&nbsp;<input name="mem_renew_penelty" id="mem_renew_penelty" class="input-trns" value="<?php echo $late_penelty; ?>" readonly>
                    </td>
                  </tr>
                  <tr>
                    <th><input id="new_card" name="new_card" class="update_pref opt-check" type="checkbox"> Get new membership card</th>
                    <td><label>Rs. </label>&nbsp;<input name="new_card_charges" id="new_card_charges" class="input-trns" value="0" readonly>
                    </td>
                  </tr>
                  <tr>
                    <th><input id="new_card_post" name="new_card_post" disabled class="update_pref opt-check" type="checkbox"> Get new membership card posted to my address:<br><?php echo $row['add1']; ?> ...</th>
                    <td><label>Rs. </label>&nbsp;<input name="new_card_postage" id="new_card_postage" class="input-trns" value="0" readonly>
                    </td>
                  </tr>
                  <tr>
                    <th>Total Amount Payable</th>
                      <?php if($monthly_pay == 0) {
                        ?>
                    <td><label>Rs. </label>&nbsp;<input name="grand_total" id="grand_total" class="input-trns" value="0" readonly>
                        <?php } else {
                          $grand_total = $final_fees + $late_penelty;
                          ?>
                    <td><label>Rs. </label>&nbsp;<input name="grand_total" id="grand_total" class="input-trns" value="<?php echo $grand_total; ?>" readonly>
                    <?php } ?>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2" class="text-center"><button type="submit" href="javascript:void(0)" class="btn btn-success" role="button">Proceed</button></td>
                  </tr>
                <?php } else{ ?>
                  <tr>
                    <td colspan="2" class="text-center"><strong><?php echo $renwal_valid_msg; ?></strong></td>
                  </tr>

                <?php } ?>
                </table>
                </form>
                <div id="data_msg"></div>

              </div>
            </div>
          </div>
        </div>
      </div>
      <?php include_once('footer.php'); ?>
    </div>
    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.bxslider.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <!-- <script src="js/jquery.prettyPhoto.js"></script> -->
    <script src="js/custom.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){
      var host_url = location.protocol + "//" + location.host;

      $('.update_pref').on('change',function(){
        var duration = parseInt($('#duration_select').val());

        if(!isNaN(duration)) {
        // console.log(duration);return false;
        var new_card_status = 0;
        var card_post_status = 0;

        if($('#new_card').is(':checked')){
          new_card_status = 1;
          $('#new_card_post').prop('disabled',false);
        }
        else {
          $('#new_card_post').prop('checked',false).prop('disabled',true);
          $('#data_msg').html('');
          $('#duration_select').removeClass('br-error');
        }

        if($('#new_card_post').is(':checked')){
          card_post_status = 1;
        }

        var ajax_data =
        {
          duration:duration,
          new_card_status:new_card_status,
          card_post_status:card_post_status
        };
        $.ajax({
            type: "POST",
            url: host_url + '/process/ajax_renew.php',
            data: ajax_data,
            success: function(result) {
              var data = JSON.parse(result);
              if(data.error_flag == 0)
              {
                if(data.monthly_pay == 0) {
                $('#total_mem_renew_charge').val(data.total_mem_renew_charge);
                $('#new_card_charges').val(data.new_card_charge);
                $('#new_card_postage').val(data.card_post_charge);
                $('#grand_total').val(data.grand_total);
                $('#new_exp_date').val(data.new_exp_date);
              }
              else {
                $('#grand_total').val(data.grand_total);
                $('#new_card_charges').val(data.new_card_charge);
                $('#new_card_postage').val(data.card_post_charge);
                }
              }
              else {
                //error occured
              }
            },
            error: function() {
              //error occured
            }
          });
        }
        else
        {
          $('#total_mem_renew_charge,#new_exp_date,#new_card_charges,#new_card_postage,#grand_total').val('0');
          $('#new_exp_date').val('-');
          $('#new_card,#new_card_post').prop('checked', false);
        }

      });
        // if(!isNaN(duration)) {
        //   var mem_renew_charge = parseInt($('#mem_renew_charge').val());     // per year renew charge as per membership
        //   var mem_renew_penelty = parseInt($('#mem_renew_penelty').val());     // late penelty
        //   var current_exp_date = $('#current_exp_date').val();
        //   var total_mem_renew_charge = duration * mem_renew_charge;
        //
        //   var grand_total = total_mem_renew_charge + mem_renew_penelty;
        //
        //   var str = current_exp_date.toString();
        //   var parts = str.split("-");
        //
        //   var day = parts[0] && parseInt( parts[0], 10 );
        //   var month = parts[1] && parseInt( parts[1], 10 );
        //   var year = parts[2] && parseInt( parts[2], 10 );
        //   // console.log('day: '+day+' | month: '+month);
        //   if( day <= 31 && day >= 1 && month <= 12 && month >= 1 ) {
        //
        //     var expiryDate = new Date( year, month - 1, day );
        //     expiryDate.setFullYear( expiryDate.getFullYear() + duration );
        //
        //     var day = ( '0' + expiryDate.getDate() ).slice( -2 );
        //     var month = ( '0' + ( expiryDate.getMonth() + 1 ) ).slice( -2 );
        //     var year = expiryDate.getFullYear();
        //
        //     var exp_date = day + "-" + month + "-" + year;
        //
        //     $('#new_exp_date').val(exp_date);
        //     $('#new_card').prop('disabled',false);
        //
        //     $('#total_mem_renew_charge').val(total_mem_renew_charge);
        //     $('#grand_total').val(grand_total);
        //
        //     // console.log(exp_date);
        //   }
        //   else {
        //     $('#new_exp_date').val('-');
        //     $('#new_card').prop('disabled',true);
        //
        //     $('#total_mem_renew_charge').val('-');
        //     $('#grand_total').val('-');
        //
        //   }
        // }
        // else {
        //   $('#new_exp_date').val('-');
        //   $('#new_card').prop('disabled',true);
        //
        //   $('#total_mem_renew_charge').val('-');
        //   $('#grand_total').val('-');
        // }


      // $('#new_card').on('change', function(){
      //   if($('#new_card').is(':checked')) {
      //     // console.log('asdasd');
      //     var grand_total = parseInt($('#grand_total').val());
      //     grand_total = grand_total + 50;
      //     $('#grand_total').val(grand_total);
      //     $('#new_card_charges').val('50');
      //   }else {
      //     var grand_total = parseInt($('#grand_total').val());
      //     grand_total = grand_total - 50;
      //     $('#grand_total').val(grand_total);
      //     $('#new_card_charges').val('-');
      //   }
      // });

      $('#renew_form').on('submit',function(e){
        // e.preventDefault();
        var flag = 0;
        var duration_select = $('#duration_select').val();
        if(duration_select == '' || duration_select == null)
        {
          $('#data_msg').html('<span class="label label-danger">Please select the duration for which you want to renew the membership</span>');
          $('#duration_select').addClass('br-error');
          return false;
        }
        // else {
        //   $('#renew_form').submit();
        // }

      });
    });
    </script>
  </body>
  </html>
<?php }
else{
  header('Location: login.php');
} ?>
