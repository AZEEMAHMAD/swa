<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                @if(Auth::guard('web')->user()->image)
                    <img src="{{asset('images/profile_image'.'/'.Auth::guard('web')->user()->image)}}"
                         class="img-circle"
                         alt="User Image"/>
                @else
                    <img src="{{ asset("/bower_components/admin-lte/dist/img/user2-160x160.jpg") }}" class="img-circle"
                         alt="User Image"/>
                @endif
            </div>
            <div class="pull-left info">
                <p>{{ ucwords(Auth::guard('web')->user()->name)}}</p>
                <!-- Status -->

            </div>
        </div>

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">Admin-Panel</li>
            <!-- Optionally, you can add icons to the links -->
            <li><a href="{{'/admin/home'}}"><span><i class="fa fa-home"></i> Adminpannel</span></a></li>

                <li><a href="{{'/admin/users'}}"><span><i class="fa fa-user"></i> User</span></a></li>


            {{--<li class="treeview">--}}
            {{--<a href="#"><span><i class="fa fa-flag"></i> Home Page</span> <i--}}
            {{--class="fa fa-angle-left pull-right"></i></a>--}}
            {{--<ul class="treeview-menu">--}}
            {{--<li><a href="#"><i class="fa fa-bookmark"></i> About Funds Map</a>--}}
            {{--</li>--}}
            {{--<li class="treeview">--}}
            {{--<a href="#"><span><i class="fa fa-plus-circle"></i> Funds Map</span> <i--}}
            {{--class="fa fa-angle-left pull-right"></i></a>--}}
            {{--<ul class="treeview-menu">--}}
            {{--<li><a href="#"><i class="fa fa-comment-o"></i> Description</a>--}}
            {{--</li>--}}
            {{--<li><a href="#"><i class="fa fa-film"></i> Funds Map</a></li>--}}
            {{--</ul>--}}

            {{--</li>--}}
            {{--</ul>--}}
            {{--</li>--}}


        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>